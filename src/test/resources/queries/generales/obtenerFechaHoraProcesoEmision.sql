SELECT TOP 1 CONVERT(VARCHAR(5),[FechaHora_ProcesoEmisionInforme], 14) AS Fecha
  FROM [t_iyf_SolicitudProcesoEmisionInforme] SPEI WITH(NOLOCK)
 INNER JOIN [t_iyf_ProcesoEmisionInforme] PEI ON PEI.ID_ProcesoEmisionInforme = SPEI.[ID_ProcesoEmisionInforme]
 WHERE Id_Solicitud = (SELECT ID_solicitud FROM t_sol_solicitud WHERE Numero_Solicitud = 1) ORDER BY 1 DESC;
