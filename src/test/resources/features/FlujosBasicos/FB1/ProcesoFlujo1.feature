Feature: Flujo Basico 1  - trigo  Calidad Comercial  - IA

  @Flujo1 @Completo
  Scenario Outline: Proceso Completo Flujo 1 - Exitoso
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
    And El Usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    #Paso Nro.6
    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    When Se valida el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    #Paso Nro.7
    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    When Se ingresa la porcion a Corte
    And Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    #Paso Nro.8
    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    #Paso Nro.9
    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContPorciones>
    When Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>
    #Paso Nro.10
    Given El usuario busca en el menu por la palabra 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    When Se Ingresan los siguientes componentes <CodAnalista>, <ArdyDanado>, <GranosDanados>, <MatExtranas>, <QuebradosyChuzo>, <PanzaBlanca>, <Picados>, <Pureza>
    #Paso Nro.11
    Given El usuario busca en el menu por la palabra 'Vaciar Contenedor'
    When Se ingresan los siguientes contenedores <ContGeneral>, <ContPorciones>
    #Paso nro.12
    Given El usuario obtiene la fecha de ingreso de la solicitud mediante el <direccion>, <nombreArchivo>
    Then se cierra la aplicacion laboratorio

    Examples:
      | CodPuerto | ContGeneral | ContPorciones | ContDestino | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo | EMA | CORTE | FISICO | AyDM | CodAnalista | CodMesada | PH | HEA | CP | CodEquipo | resultado                               | ArdyDanado | GranosDanados | MatExtranas | QuebradosyChuzo | PanzaBlanca | Picados | Pureza |
      | 24802     | AUCG02      | AUCP01        | QAAG05      | automatizador02 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | BCR1066N      | 403 | 404   | 405    | 407  | 41          | 2         | 73 | 15  | 12 | 482       | Los resultados se encuentran correctos. | 0,03       | 0,03          | 0,00        | 0,00            | 0,00        | 0,00    | 65     |

# BCR --> ESTO ES MAL ACA , debe ir separado
#  @Flujo1 @ImportacionArchivoSolici @Flujo2
#  Scenario Outline: Proceso Flujo 1 - Paso importacion archivo solici - Caso exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    When Se carga un archivo solici <direccion>, <nombreArchivo>, <CodPuerto>
#    Then Se valida el ingreso de muestras
#    @Flujo1
#    Examples:
#
#      | CodPuerto | usuario         | password     | direccion                                     | nombreArchivo |
#      | 24802     | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      |
#    @Flujo2
#    Examples:
#      | CodPuerto | usuario         | password     | direccion                                     | nombreArchivo |
#      | 24802     | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB2\ | Solici01      |
#
#  @AperturaContenedor
#  Scenario Outline: Proceso Flujo 1 - Paso apertura contenedor - Caso exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario busca en el menu por la palabra 'Ingresar Muestras de Solicitudes Automática'
#    When Ingresa la fecha del dia
#    And Ingresa el codigo puerto <CodPuerto>
#    And Ingresa el numero de muestra <direccion>, <nombreArchivo>
#    And Selecciona el boton 'Registrar Solicitud'
#    And Selecciona el boton 'Abrir nuevo contenedor'
#    And Ingresa el nombre del contenedor <ContGeneral>
#    And Desactiva la opcion de imprimir datos de apertura
#    And Selecciona el boton 'Abrir Contenedor'
#    Then Se valida la creacion del contenedor
#    And se cierra la aplicacion laboratorio
#
#    @Flujo1
#    Examples:
#      | CodPuerto | ContGeneral | usuario         | password     | direccion                                     | nombreArchivo |
#      | 24802     | ARCG01      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      |
#    @Flujo2
#    Examples:
#      | CodPuerto | ContGeneral | usuario         | password     | direccion                                     | nombreArchivo |
#      | 24802     | ARCG01      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB2\ | Solici01      |
#
#  @Flujo1 @CierreContenedor
#  Scenario Outline: Proceso Flujo 1 - Paso cierre contenedor exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    When Se obtiene el numero de solicitud del archivo <direccion>, <nombreArchivo>
#    And El usuario busca en el menu por la palabra 'Cerrar Contenedor'
#    And Cierra el contenedor <ContGeneral>
#    And Selecciona el boton 'Cerrar Contenedor'
#    Then Se valida el cierre del contenedor
#    And se cierra la aplicacion laboratorio
#
#    Examples:
#      | CodPuerto | ContGeneral | usuario         | password     | direccion                                     | nombreArchivo |
#      | 24802     | ARCG01      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      |
#
#  @Flujo1 @CambioPuestoDeTrabajo
#  Scenario Outline: Proceso Flujo 1 - Paso cambio puesto de trabajo - Exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    And Se obtiene el numero de solicitud del archivo <nombreArchivo>
#    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
#    When El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <nuevo>
#
#    Examples:
#      | CodPuerto | ContGeneral | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | nuevo |
#      | 24802     | ARCG01      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403   |
#
#  @Flujo1 @IngresoMuestraPorcion
#  Scenario Outline: Proceso Flujo 1 - Paso ingreso muestra/porcion a un sector - Exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    And Se obtiene el numero de solicitud del archivo <nombreArchivo>
#    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <nuevo>
#    When El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector'
#    And Ingresa el codigo del contenedor <ContGeneral>
#    Then se valida el ingreso de la muestra
#
#    Examples:
#      | CodPuerto | ContGeneral | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | nuevo |
#      | 24802     | ARCG01      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403   |
#
#  @Flujo1 @IngresoEMA
#  Scenario Outline: Proceso Flujo 1 - Paso e Ingreso al sector de EMA - Exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    And Se obtiene el numero de solicitud del archivo <direccion>, <nombreArchivo>
#    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
#    And El usuario revisa el ingreso y registra la solicitud a un sector con los parametros <ContGeneral>
#    And El usuario busca en el menu por la palabra 'Ingresar Resultados EMA'
#    Then Se valida el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado>
#    And se cierra la aplicacion laboratorio
#
#    Examples:
#      | CodPuerto | ContGeneral | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | EMA | CodAnalista | PH | HEA | CP | CodEquipo | resultado                               |
#      | 24802     | ARCG01      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403 | 41          | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. |
#
#  @Flujo1 @IngresoCorte
#  Scenario Outline: Proceso Completo Flujo 1 - Paso e Ingreso al sector de Corte- Exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.6
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se valida el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.7
#    When El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector'
#    And Se ingresa la porcion al sector <ContGeneral>, <ContPorciones>
#    And Se Validan la muestras ingresadas <ContPorciones>
#
#    Then se cierra la aplicacion laboratorio
#
#    Examples:
#      | CodPuerto | ContGeneral | ContPorciones | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | EMA | CORTE | CodAnalista | PH | HEA | CP | CodEquipo | resultado                               |
#      | 24802     | ARCG01      | ARCP01        | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403 | 404   | 41          | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. |
#
#
#  @Flujo1 @Archivo
#  Scenario Outline: Proceso Completo Flujo 1- Paso e Ingreso de Muestra al Archivo - Exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.6
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se valida el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.7
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se ingresa la porcion al sector de Corte con el parametro <ContPorciones>
#    And Se Validan la muestras ingresadas <ContPorciones>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.8
#    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
#    When Se ingresa la muestra al archivo <ContDestino>
#
#    Then se cierra la aplicacion laboratorio
#
#    Examples:
#      | CodPuerto | ContGeneral | ContPorciones | ContDestino | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | EMA | CORTE | AyDM | CodAnalista | PH | HEA | CP | CodEquipo | resultado                               |
#      | 24802     | ARCG01      | ARCP01        | QAAG05      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403 | 404   | 407  | 41          | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. |
#
#  @Flujo1 @LabFisoco
#  Scenario Outline: Proceso Completo Flujo 1 - Asignar Analista A Muestras Porciones Dentro De Un Contenedor - Exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.6
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se valida el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.7
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se ingresa la porcion al sector de Corte con el parametro <ContPorciones>
#    And Se Validan la muestras ingresadas <ContPorciones>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.8
#    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
#    When Se ingresa la muestra al archivo <ContDestino>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.9
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContPorciones>
#    When Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>
#    Then se cierra la aplicacion laboratorio
#
#    Examples:
#      | CodPuerto | ContGeneral | ContPorciones | ContDestino | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | EMA | CORTE | FISICO | AyDM | CodAnalista | CodMesada | PH | HEA | CP | CodEquipo | resultado                               |
#      | 24802     | ARCG01      | ARCP01        | QAAG05      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403 | 404   | 405    | 407  | 41          | 2         | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. |
#
#  @Flujo1 @Balanza
#  Scenario Outline: Proceso Completo Flujo 1 - Se ingresan la porcion y datos correctos al sector de Balanza - Exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.6
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se valida el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.7
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se ingresa la porcion al sector de Corte con el parametro <ContPorciones>
#    And Se Validan la muestras ingresadas <ContPorciones>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.8
#    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
#    When Se ingresa la muestra al archivo <ContDestino>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.9
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContPorciones>
#    When Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>
#    #Paso Nro.10
#    Given El usuario busca en el menu por la palabra 'Ingresar Componentes Ensayos Sector Balanza-Físico'
#    When Se ingresan los componentes siguientes <CodAnalista>, <ArdyDanado>, <GranosDanados>, <MatExtranas>, <QuebradosyChuzo>, <PanzaBlanca>, <Picados>, <Pureza>
#    Then se cierra la aplicacion laboratorio
#
#    Examples:
#      | CodPuerto | ContGeneral | ContPorciones | ContDestino | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | EMA | CORTE | FISICO | AyDM | CodAnalista | CodMesada | PH | HEA | CP | CodEquipo | resultado                               | ArdyDanado | GranosDanados | MatExtranas | QuebradosyChuzo | PanzaBlanca | Picados | Pureza |
#      | 24802     | ARCG01      | ARCP01        | QAAG05      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403 | 404   | 405    | 407  | 41          | 2         | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. | 0,03       | 0,03          | 0,00        | 0,00            | 0,00        | 0,00    | 65     |
#
#
#  @Flujo1 @Vaciar_Contenedores
#  Scenario Outline: Proceso Completo Flujo 1 - Se vacian los contenedores correctamente - Exitoso
#    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
#    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
#    And El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
#    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.6
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se valida el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.7
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
#    When Se ingresa la porcion al sector de Corte con el parametro <ContPorciones>
#    And Se Validan la muestras ingresadas <ContPorciones>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.8
#    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
#    When Se ingresa la muestra al archivo <ContDestino>
#    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
#    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
#    #Paso Nro.9
#    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContPorciones>
#    When Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>
#    #Paso Nro.10
#    Given El usuario busca en el menu por la palabra 'Ingresar Componentes Ensayos Sector Balanza-Físico'
#    When Se ingresan los componentes siguientes <CodAnalista>, <ArdyDanado>, <GranosDanados>, <MatExtranas>, <QuebradosyChuzo>, <PanzaBlanca>, <Picados>, <Pureza>
#    #Paso Nro.11
#    Given El usuario busca en el menu por la palabra 'Vaciar Contenedor'
#    When Se ingresan los siguientes contenedores <ContGeneral>, <ContPorciones>
#    Then se cierra la aplicacion laboratorio
#
#    Examples:
#      | CodPuerto | ContGeneral | ContPorciones | ContDestino | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | EMA | CORTE | FISICO | AyDM | CodAnalista | CodMesada | PH | HEA | CP | CodEquipo | resultado                               | ArdyDanado | GranosDanados | MatExtranas | QuebradosyChuzo | PanzaBlanca | Picados | Pureza |
#      | 24802     | ARCG01      | ARCP01        | QAAG05      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403 | 404   | 405    | 407  | 41          | 2         | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. | 0,03       | 0,03          | 0,00        | 0,00            | 0,00        | 0,00    | 65     |