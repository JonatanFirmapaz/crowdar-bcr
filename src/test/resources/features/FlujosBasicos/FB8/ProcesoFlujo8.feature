Feature: Flujo Basico 8

  @Flujo8 @Completo
  Scenario Outline: Proceso Completo Flujo 8 - Exitoso
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
    When El usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <PRODUCTO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    And El usuario ingresa el codigo de la muestra
    When El usuario se dirige a la pantalla 'Leer Muestra/Porción para Ingresar Componentes'
    And Se ingresa la muestra al sector
    And Se ingresan los siguientes datos al sector HUMEDAD: '<CodAnalista>', '<TC1>', '<TC2>', '<PM1>', '<PM2>', '<PCMS1>', '<PCMS2>'
    When El usuario hace click en el boton 'Aceptar Componente'
    And El usuario valida los resultados ingresados en HUMEDAD
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    And El usuario ingresa el codigo de la muestra
    And Se ingresa la porcion a Corte
    When Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContPorciones>'
    Then Se ingresa al sector LabFisico con los siguientes datos <CodAnalista2>, <CodMesada>, <ContPorciones>

    Given El usuario se dirige a la pantalla 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    When Se Ingresan los siguientes datos <CodAnalista2>, <PesoEnsayo>, <Pureza>, <Chamico>, <Tarrito>
    And El usuario busca en el menu por la palabra 'Vaciar Contenedor'
    When Se ingresa el siguiente contenedor <ContPorciones>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <PRODUCTO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el codigo de la porcion al sector
    Then Modificacion de archivo RMN <direccion>, <RMN>

    Given El usuario se dirige a la pantalla 'Consultar Procesos de Archivos RMN ensayo Materia Grasa'
    When El usuario carga el archivo RMN con los siguientes datos <direccion>, <RMN>, <EXTENSION>, <CodAnalista>, <CodEquipo>, <Matriz>

    Given El usuario se dirige a la pantalla 'Leer Muestra/Porción para Ingresar Componentes'
    And Se ingresa la muestra_Porcion al sector
    When El usuario carga ensayo de Acidez con los siguientes datos <CodAnalista2>, <TM>, <PMA>, <V>
    And El usuario hace click en el boton 'Aceptar Componente'
    And El usuario hace click en el boton 'Aceptar'
    Then Se validan los resultados

    Given El usuario se dirige a la pantalla 'Listar Muestras-Porciones con Error de Criterio de Aceptación'
    And El usuario selecciona el checkbox de 'Lab. químico - productos'
    And El usuario selecciona la casilla de 'Todos'
    When El usuario hace click en el boton 'Filtrar'
    And El usuario selecciona el checkbox que contiene la muestra
    When El usuario hace click en el boton 'Aceptar Componentes'
    And El usuario ingresa en el menu por palabra 'Error analitico', 'AU pruebas'
    Then El usuario hace click en el boton 'Aceptar'

    Examples:
      | usuario         | password     | puestoTrabajo   | CodPuerto | direccion                                     | nombreArchivo | RMN         | EXTENSION | CORTE | FISICO | AyDM | PRODUCTO | ContPorciones | ContDestino | CodAnalista | CodAnalista2 | TC1  | TC2  | PM1  | PM2  | PCMS1 | PCMS2  | Pureza | CodMesada | PesoEnsayo | Chamico | Tarrito | CodEquipo | Matriz | TM   | PMA | V |
      | automatizador02 | QA.Prov-2021 | DESKTOP-0BESI4E | 24802     | \src\test\resources\solici\FlujosBasicos\FB8\ | FB8           | Flujo 8 RMN | .dat      | 404   | 405    | 407  | 603      | AUCP01        | QAAG06      | 48          | 41           | 0,35 | 0,37 | 0,38 | 0,37 | 0,70  | 0,7130 | 60     | 3         | 0,01       | 1       | QAT001  | 1434      | 27     | 0,06 | 9,4 | 1 |