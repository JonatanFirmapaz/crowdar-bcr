@Flujo6
Feature: Flujo Basico 6

  @Paso1 @Completo
  Scenario Outline: Proceso Incompleto Flujo 6 - Paso 1
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en '<tipoSolicitud>' en la pantalla Pre Ingresar Solicitud
    And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'

    Examples:
      | usuario         | password     | tipoSolicitud | Solicitante | Matriz | Grupo | Peso |
      | automatizador02 | QA.Prov-2021 | General       | 105         | 21     | 1     | 1200 |

  @Paso2 @Completo
  Scenario Outline: Proceso Incompleto Flujo 6 - Paso 2
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud
    And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'
    And El usuario ingresa el numero de solicitud
    And El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Ingresar solicitud' en la pantalla de Administrar Solicitudes
    And El usuario completa los campos de Informacion General con los siguientes datos: '<Vendedor>', '<Solicitante>', '<MuestraDeclarada>'
    And El usuario completa los campos de Otros Datos Generales con los siguientes datos: '<Pagador>', '<FormaPago>'

    Examples:
      | usuario         | password     | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada | Pagador | FormaPago |
      | automatizador02 | QA.Prov-2021 | 105         | 21     | 1     | 1200 | 105      | SOJA             | 1       | 3         |

  @FlujoCompleto6
  Scenario Outline: Proceso Flujo 6 - Completo
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    # STEP 1
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud
    And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'
    And El usuario guarda los cambios
    And El usuario decide no abrir el contenedor
    Then El usuario visualiza el popup de exito
    # STEP 2
    When El usuario ingresa el numero de solicitud
    And El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Ingresar solicitud' en la pantalla de Administrar Solicitudes
    And El usuario completa los campos de Informacion General con los siguientes datos: '<Vendedor>', '<Solicitante>', '<MuestraDeclarada>'
    And El usuario completa los campos de Otros Datos Generales con los siguientes datos: '<Pagador>', '<FormaPago>'
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito
    # STEP 3
    When El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Verificar' en la pantalla de Administrar Solicitudes
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito

    Given El usuario se dirige a la pantalla 'Administrar Facturas de Contado'
    When El usuario hace click en 'Agregar'
    And El usuario completa los campos de Administrar Facturas de Contado con el código de cliente '<Solicitante>'
    And El usuario hace click en '_Agregar'
    And El usuario ingresa el concepto '<concepto>' e importe total '<importeTotal>'
    And El usuario hace click en 'Aceptar'
    And El usuario selecciona checkbox de la solicitud
    And El usuario selecciona la forma de pago '<descFormaPago>'
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'muestra' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros ''
    # Then validar resultado OK

    Given El usuario se dirige a la pantalla 'Leer Muestra/Porción para Ingresar Componentes'
    When El usuario ingresa el numero de muestra ya obtenido y da enter
    And El usuario completa el formulario en la pantalla Leer Muestra o Porción para ingresar componentes con los siguientes datos: '<CodAnalista>', '10', '90', '900'
    And El usuario hace click en el boton 'Aceptar Componente'
    Then El usuario verifica el popup 'Resultados con error de topes'
    And El usuario hace click en el boton 'Cancelar'
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    And Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'muestra' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros ''
    And Se ingresa la porcion a Corte
    And Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContPorciones>'
    And Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>

    Given El usuario se dirige a la pantalla 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    When Se Ingresan los siguientes Componentes <CodAnalista>, <GranosDanados>, <GranosVerdes>, <TierraIncluidaEnMatExtranas>, <MatExtranas>, <GranosQuebrados>, <Pureza>

    Given El usuario se dirige a la pantalla 'Consultar Resultados de Ensayos de una Solicitud'
    When El usuario ingresa el numero de solicitud ya obtenido y da enter
    # then visualiza estados OK

    Given El usuario se dirige a la pantalla 'Listar Muestras-Porciones con Error de Tope'
    When Se genera listado de tope en 'Lab. físico - análisis físico', 'Aceptar Componentes'
    And El usuario ingresa en el menu por palabra 'Error analitico', 'AU pruebas'
    And El usuario regresa a la pantalla 'Consultar Resultados de Ensayos de una Solicitud'
    When El usuario hace click en 'Número de Solicitud: ', e ingresa la solicitud en la pantalla de Consultar Resultados de Ensayos de una Solicitud
    And El usuario busca en el menu por la palabra 'Vaciar Contenedor'
    Then Se ingresa el siguiente contenedor <ContPorciones>

    Examples:
      | usuario         | password     | puestoTrabajo   | CORTE | FISICO | AyDM | ContPorciones | ContDestino | CodAnalista | GranosDanados | MatExtranas | GranosVerdes | TierraIncluidaEnMatExtranas | GranosQuebrados | Pureza | CodMesada | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada | Pagador | FormaPago | concepto                     | importeTotal | descFormaPago |
      | automatizador02 | QA.Prov-2021 | DESKTOP-0BESI4E | 404   | 405    | 407  | AUCP01        | QAAG06      | 41          | 0,01          | 0,02        | 0,01         | 0,01                        | 0,00            | 45     | 3         | 32003       | 21     | 1     | 1200 | 30771    | SOJA             | 1       | 3         | Honorarios por Análisis Soja | 1495         | PAGO DIFERIDO |