@Flujo11

  Feature: Flujo Básico 11

    Scenario Outline: Proceso Flujo 6 - Completo
      Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    # STEP 1
      When El usuario se dirige a la pantalla 'Administrar Solicitudes'
      And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
      And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud
      And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'
      And El usuario guarda los cambios
      And El usuario decide no abrir el contenedor
      Then El usuario visualiza el popup de exito

      Examples:
        | usuario         | password     | puestoTrabajo   | CORTE | FISICO | AyDM | ContPorciones | ContDestino | CodAnalista | GranosDanados | MatExtranas | GranosVerdes | TierraIncluidaEnMatExtranas | GranosQuebrados | Pureza | CodMesada | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada | Pagador | FormaPago | concepto                     | importeTotal | descFormaPago |
        | automatizador02 | QA.Prov-2021 | DESKTOP-0BESI4E | 404   | 405    | 407  | AUCP01        | QAAG06      | 41          | 0,01          | 0,02        | 0,01         | 0,01                        | 0,00            | 45     | 3         | 32003       | 21     | 1     | 1200 | 30771    | SOJA             | 1       | 3         | Honorarios por Análisis Soja | 1495         | PAGO DIFERIDO |