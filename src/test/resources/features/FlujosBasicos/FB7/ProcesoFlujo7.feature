Feature: Flujo Basico 7  - Lacrado

  @Flujo7 @Completo7
  Scenario Outline: Proceso Completo Flujo 2 - Exitoso
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
    And El usuario ingresa una solicitud lacrado con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <MensajeSistema>, <ContZorra>
    And El usuario selecciona empleado firmante <codFirmante>
    And El usuario ingresa por la pantalla Cuartear Muestra de Lacrado
    And El usuario abre el contenedor: <ContGeneral>
    And El usuario cierra el contenedor: <ContGeneral>

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    When Se ingresa la porcion a Corte
    And Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContPorciones>
    And El usuario cierra el contenedor: <ContPorciones>
    When Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>

    Given El usuario busca en el menu por la palabra 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    When Se ingresan los componentes siguientes <CodAnalista>, <PesoEnsayo>, <Pureza>, <ContTarrito>
    And El usuario busca en el menu por la palabra 'Vaciar Contenedor'
    When Se ingresa el siguiente contenedor <ContPorciones>

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <PRODUCTOS>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector'
    When Se ingresa la porcion a Productos
    And Se modifica archivo RMN <direccion>, <RMN>

    Given El usuario se dirige a la pantalla 'Consultar Procesos de Archivos RMN ensayo Materia Grasa'
    When El usuario carga el archivo RMN con los siguientes datos <direccion>, <RMN>, <EXTENSION>, <CodAnalista2>, <CodEquipo2>, <Matriz>
    Then El usuario busca en el menu por la palabra 'Listar Solicitudes con Error Procesadas por RMN'
    And El usuario selecciona RMN con error de Curva

    Given El usuario busca en el menu por la palabra 'Consultar Resultados de Ensayos de una Solicitud'
    When El usuario ingresa el numero de solicitud
    Then se cierra la aplicacion laboratorio


    @Flujo2 @Completo7
    Examples:
      | CodPuerto | ContGeneral | ContPorciones | ContDestino | ContZorra | ContTarrito | usuario         | password     | nombreArchivo | RMN         | EXTENSION | puestoTrabajo   | codFirmante | PRODUCTOS | CORTE | FISICO | AyDM | CodAnalista | CodAnalista2 | CodMesada | PH | HEA | CodEquipo | CodEquipo2 | PesoEnsayo | Matriz | MatExtranas | QuebradosyChuzo | PanzaBlanca | OtroTipo | Picados | Pureza | direccion                                     | resultado                     | MensajeSistema |
      | 24802     | AUCG07      | ARCP07        | QAAG07      | QAZ001    | QAT001      | automatizador03 | QA.Prov-2021 | FB7           | Flujo_7_RMN | .mdt      | LAPTOP-8VNHLSCQ | 58          | 603       | 404   | 405    | 407  | 41          | 48            | 2         | 70 | 12  | 482       | 1434        | 0,01       | 20     | 0,01        | 0,00            |             | 0,00     | 0,00    | 65     | \src\test\resources\solici\FlujosBasicos\FB7\ | Resultados con error de topes | Correcta.       |
