Feature: Flujo Basico 5  - soja  Calidad Comercial  - IA

  @Flujo5 @Completo
  Scenario Outline: Proceso Completo Flujo 5 - Exitoso
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
    And El Usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
    When El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    And El usuario busca en el menu por la palabra 'Leer Muestra/Porción para Ingresar Componentes'
    When Se ingresa al sector con los siguientes datos <CodAnalista>, '2', '100', '200'
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    And Se ingresa la porcion a Corte
    When Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContPorciones>
    When Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>
    And El usuario busca en el menu por la palabra 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    Then Se Ingresan los siguientes Componentes <CodAnalista>, <GranosDanados>, <GranosVerdes>, <MatExtranas>, <QuebradosyChuzo>, <TierraMatExtranas>, <Pureza>

    Given El usuario se dirige a la pantalla 'Listar Muestras-Porciones con Error de Tope'
    When Se genera listado de tope en 'Lab. físico - análisis físico', 'Aceptar Componentes'
    Then El usuario ingresa en el menu por palabra 'Error analitico', 'AU pruebas'

    Given El usuario busca en el menu por la palabra 'Modificar Componentes'
    When El usuario modifica los componentes siguientes '90', '900'
    Then El usuario ingresa detalle de la modificacion 'Error analitico', 'AU pruebas'

    Given El usuario busca en el menu por la palabra 'Vaciar Contenedor'
    When Se ingresan los siguientes contenedores <ContPorciones>, <ContGeneral>

    Examples:
      | CodPuerto | ContGeneral | ContPorciones | ContDestino | usuario         | password     | direccion                                     | nombreArchivo | puestoTrabajo   | EMA | CORTE | FISICO | AyDM | CodAnalista | CodMesada | PH | HEA | CP | CodEquipo | resultado                               | GranosVerdes | GranosDanados | MatExtranas | QuebradosyChuzo | TierraMatExtranas | Pureza |
      #| 24802     | ARCG01      | ARCP01        | QAAG05      | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      | DESKTOP-0BESI4E | 403 | 404   | 405    | 407  | 41          | 2         | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. | 0,03       | 0,03          | 0,00        | 0,00            | 0,00        | 65     |
      | 24802     | AUCG05      | AUCP05        | QAAG05      | automatizador02 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB5\ | FB5           | DESKTOP-0BESI4E | 403 | 404   | 405    | 407  | 41          | 3         | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. | 0,01         | 0,01          | 0,01        | 0,01            | 0,01              | 45     |
