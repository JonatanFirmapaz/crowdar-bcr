Feature: Flujo Basico 9 - GIRASOL ALTO OLEICO
@Flujo9Completo
Scenario Outline: Proceso Flujo 9 - Completo
Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    # STEP 1
When El usuario se dirige a la pantalla 'Administrar Solicitudes'
And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
  And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud

And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'
And El usuario guarda los cambios
And El usuario decide no abrir el contenedor
Then El usuario visualiza el popup de exito
    # STEP 2
When El usuario ingresa el numero de solicitud
And El usuario selecciona la celda que contiene la muestra
And El usuario hace click en 'Ingresar solicitud' en la pantalla de Administrar Solicitudes
And El usuario completa los campos de Informacion General con los siguientes datos: '<Vendedor>', '<Solicitante>', '<MuestraDeclarada>'
And El usuario completa los campos de Otros Datos Generales con los siguientes datos: '<Pagador>', '<FormaPago>'
And El usuario guarda los cambios
Then El usuario visualiza el popup de exito
    # STEP 5
When El usuario selecciona la celda que contiene la muestra
And El usuario hace click en 'Verificar' en la pantalla de Administrar Solicitudes
And El usuario guarda los cambios
Then El usuario visualiza el popup de exito

    #Given El usuario se dirige a la pantalla 'Administrar Facturas de Contado'
    #When El usuario hace click en 'Agregar'
    #And El usuario completa los campos de Administrar Facturas de Contado con el código de cliente '<Solicitante>'
    #And El usuario hace click en '_Agregar'
    #And El usuario ingresa el concepto '<concepto>' e importe total '<importeTotal>'
    #And El usuario hace click en 'Aceptar'
    #And El usuario selecciona la forma de pago '<descFormaPago>'
    #And El usuario selecciona la celda de la solicitud
    #And El usuario guarda los cambios
    #Then El usuario visualiza el popup de exito
#STEP 6
  And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
  Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
  Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    ## ContGeneral
  When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContGeneral>'
  When Se ingresa la porcion a Corte
  And Se abre el contenedor: '<ContPorciones>'
  And Se Validan la muestras ingresadas <ContPorciones>
  And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
  Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

Given El usuario se dirige a la pantalla 'Ingresar Muestra o Contenedor a Archivo'
When Se ingresa la muestra al archivo <ContDestino>
And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

  #STEP 9
Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContPorciones>'
And Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <NroPorcion>, '<ContPorciones>' #Es correcto?

Given El usuario se dirige a la pantalla 'Ingresar Componentes Ensayos Sector Balanza-Físico'
When Se ingresan los siguientes componentes '<CodAnalista>', '<MatExtranas>', '<Chamico>', '<Pureza>','<NroTarrito>'

	#Falta steps de jhona (flujo 8) rmn y acidez

	#Step 12
Given El usuario se dirige a la pantalla 'Listar Muestras-Porciones con Error de Tope'
When Se genera listado de tope en 'Lab. químico - Productos', 'Aceptar Componentes'
And El usuario ingresa en el menu por palabra 'Error analitico', 'AU pruebas'
And El usuario regresa a la pantalla 'Consultar Resultados de Ensayos de una Solicitud'
And El usuario hace click en 'Número de Solicitud: ', e ingresa la solicitud en la pantalla de Consultar Resultados de Ensayos de una Solicitud
    # then valida resultados OK

And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CROMA>
Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContPorciones>'
And se ingresan los siguientes componentes <CodAnalistaCroma>, <Flujo9croma.txt> y aceptamos #Modificar archivo con N de porcion para utilizar

Given El usuario se dirige a la pantalla 'Consultar Resultados de Ensayos de una Solicitud'
When El usuario ingresa el numero de solicitud ya obtenido y da enter
    # then visualiza estados OK

When El usuario se dirige a la pantalla 'Administrar Solicitudes'
And El usuario hace click en 'Aprobar Emisión' en la pantalla de Administrar Solicitudes
And El usuario hace click en 'Motivo' y guarda el dato

Examples:
  | usuario  | password   | puestoTrabajo | NumeroMuestra | CORTE | FISICO | AyDM | ContPorciones | ContDestino | CodAnalista | Esclerotos | MatExtranas | NroTarrito | Chamico | Pureza | CodMesada | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada    | Pagador | FormaPago | concepto                    | importeTotal | descFormaPago | CodAnalistaCroma |
  | evazquez | 358595Evk! | BCR1066N      | AU3302FB33001 | 404   | 405    | 407  | AUCP01        | QAAG06      | 41          | 0          | 0,01        | QAT001     | 1       | 60     | 3         | 32003       | 27     | 14    | 1200 |          | Girasol Alto Oleico | 1       | 3         | Honorarios por Análisis GAO | 3629         | PAGO DIFERIDO | 173              |