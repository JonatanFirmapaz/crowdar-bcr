Feature: Flujo Basico 2  - Maiz Calidad Comercial Sin Chamico - IA

  @Flujo2Completo
  Scenario Outline: Proceso Completo Flujo 2 - Exitoso
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
    When El Usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    When Se confirma el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CodEquipo>, <resultado>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    And Se ingresa la porcion a Corte
    When Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContPorciones>
    When Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>

    Given El usuario busca en el menu por la palabra 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    When Se Ingresan los siguientes Componentes <CodAnalista>, <GranosDanados>, <MatExtranas>, <QuebradosyChuzo>, <Picados>, <OtroTipo>, <Pureza>

    Given El usuario busca en el menu por la palabra 'Listar Muestras-Porciones con Error de Tope'
    When Se genera listado de tope en 'Lab. físico - EMA', 'Re-Analizar por Mismo Método'
    Then El usuario ingresa en el menu por palabra 'Error analitico', 'AU pruebas'

    Given El usuario se dirige a la pantalla 'Pedido de Extracción'
    And El usuario hace click en el boton 'Agregar'
    And El usuario completa los detalles en la pantalla de Pedido de Extraccion con los siguientes parametros: 'Laboratorio Físico Comercial', 'Baja', 'Técnico', 'Pendiente', '41'
    When El usuario hace click en el boton 'Listar Solicitudes'
    And El usuario selecciona la celda que contiene la muestra
    And El usuario guarda los cambios
    When El usuario visualiza el popup de exito
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Administrar Archivo'
    And El usuario hace click en el boton 'Extraer'
    When El usuario hace click en el boton 'Extraer' en la pantalla Extraer Muestras
    And El usuario extrae la muestra del archivo '<ContGeneral>'
    And El usuario hace click en el boton 'Aceptar'
    When El usuario cierra el contenedor: <ContGeneral>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    When Se confirma el ingreso al sector de EMA con los siguientes datos <CodAnalista>, <PH>, <HEA>, <CodEquipo>, <resultado>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Muestra o Contenedor a Archivo'
    Then Se ingresa la muestra al archivo <ContDestino>

    Given El usuario busca en el menu por la palabra 'Listar Solicitudes Pendientes de Decisión de Resultado'
    And El usuario seleccina el sector 'Lab. físico - EMA'
    And El usuario hace click en el boton 'Filtrar'
    When El usuario hace click en la muestra
    And El usuario hace click en el boton 'Aceptar Solicitud'
    When El usuario seleccina  el ensayo
    And El usuario selecciona la opcion de 'Aceptar Resultado.'
    And El usuario preciona el boton 'Si'
    Then El usuario selecciona el boton Aceptar

    Given El usuario busca en el menu por la palabra 'Consultar Resultados de Ensayos de una Solicitud'
    Then El usuario ingresa el numero de la solicitud a verificar

    Given El usuario busca en el menu por la palabra 'Vaciar Contenedor'
    Then Se ingresan los siguientes contenedores <ContGeneral>, <ContPorciones>

    Examples:
      | CodPuerto | ContGeneral | ContPorciones | ContDestino | usuario         | password     | nombreArchivo | puestoTrabajo   | EMA | CORTE | FISICO | AyDM | CodAnalista | CodMesada | PH | HEA | CodEquipo | GranosDanados | MatExtranas | QuebradosyChuzo | OtroTipo | Picados | Pureza | direccion                                     | resultado                     |
      | 24802     | AUCG02      | ARCP02        | QAAG05      | automatizador02 | QA.Prov-2021 | Solici01      | DESKTOP-0BESI4E | 403 | 404   | 405    | 407  | 41          | 2         | 70 | 12  | 482       | 0,02          | 0,01        | 0,00            | 0,00     | 0,00    | 48     | \src\test\resources\solici\FlujosBasicos\FB2\ | Resultados con error de topes |
