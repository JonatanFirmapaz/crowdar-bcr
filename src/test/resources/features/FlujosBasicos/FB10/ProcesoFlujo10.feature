Feature: Flujo Basico 10

  @Flujo10 @Completo
  Scenario Outline: Proceso Completo Flujo 10 - Exitoso
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
    And El Usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <PRODUCTO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    Then El usuario busca en el menu por la palabra 'Leer Muestra/Porcion para Ingresar Componentes'
    And Se ingresan los siguientes datos al sector HUMEDAD: '<CodAnalista>', '<TC1>', '<TC2>', '<PM1>', '<PM2>', '<PCMS1>', '<PCMS2>'
    When El usuario hace click en el boton 'Aceptar Componente'
    And El usuario valida los resultados ingresados en HUMEDAD

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario busca en el menu por la palabra 'Ingresar Contenedor o Muestra/Porción a un sector' con el parametro <ContGeneral>
    When Se ingresa la porcion a Corte
    And Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContPorciones>'
    And Se ingresa al sector LabFisico con los siguientes datos <CodAnalista2>, <CodMesada>, <ContPorciones>

    Given El usuario se dirige a la pantalla 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    When Se Ingresan los siguientes datos <CodAnalista2>, <PesoEnsayo>, <Pureza>, <Chamico>, <Tarrito>
    And El usuario busca en el menu por la palabra 'Vaciar Contenedor'
    When Se ingresa el siguiente contenedor <ContPorciones>

    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <PRODUCTO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el codigo de la porcion al sector
    And El usuario busca en el menu por la palabra 'Leer Muestra/Porcion para Ingresar Componentes'
    And El usuario ingresa el codigo de la porcion al sector



    Examples:
      | usuario         | password     | puestoTrabajo   | CodPuerto | direccion                                      | nombreArchivo | CORTE | FISICO | AyDM | PRODUCTO | ContGeneral | ContGeneralDos | ContPorciones | ContDestino | CodAnalista | CodAnalista2 | TC1  | TC2  | PM1  | PM2  | PCMS1 | PCMS2  | Pureza | CodMesada | PesoEnsayo | Chamico | Tarrito | CodEquipo | Matriz |
      | automatizador03 | QA.Prov-2021 | LAPTOP-8VNHLSCQ | 24802     | \src\test\resources\solici\FlujosBasicos\FB10\ | FB10          | 404   | 405    | 407  | 603      | AUCG02      | AUCG07         | AUCP01        | QAAG06      | 48          | 41           | 0,35 | 0,37 | 0,38 | 0,37 | 0,70  | 0,7130 | 60     | 3         | 0,01       | 1       | QAT001  | 1434      | 27     |