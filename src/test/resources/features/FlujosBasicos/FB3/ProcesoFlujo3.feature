@Flujo3
Feature: Flujo Basico 3

  @Paso1 @Completo
  Scenario Outline: Proceso Incompleto Flujo 3 - Paso 1
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en '<tipoSolicitud>' en la pantalla Pre Ingresar Solicitud
    And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<NumeroMuestra>', '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'

    Examples:
      | usuario         | password     | tipoSolicitud | NumeroMuestra  | Solicitante | Matriz | Grupo | Peso |
      | automatizador03 | QA.Prov-2021 | General       | AU180322FB3201 | 32003       | 02     | 01    | 1200 |

  @Paso2 @Completo
  Scenario Outline:  Proceso Incompleto Flujo 3 - Paso 2
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud
    And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'
    And El usuario decide no abrir el contenedor
    And El usuario visualiza el popup de exito
    And El usuario ingresa el numero de solicitud
    And El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Ingresar solicitud' en la pantalla de Administrar Solicitudes
    And El usuario completa los campos de Informacion General con los siguientes datos: '<Vendedor>', '<Solicitante>', '<MuestraDeclarada>'
    And El usuario completa los campos de Otros Datos Generales con los siguientes datos: '<Pagador>', '<FormaPago>'

    Examples:
      | usuario         | password     | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada | Pagador | FormaPago |
      | automatizador03 | QA.Prov-2021 | 32003       | 02     | 01    | 1200 | 30771    | MAIZ             | 1       | 3         |

  @Paso3 @Completo
  Scenario Outline: Proceso Incompleto Flujo 3 - Paso 3
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    # STEP 1
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud
    And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'
    And El usuario guarda los cambios
    And El usuario decide no abrir el contenedor
    Then El usuario visualiza el popup de exito
    # STEP 2
    When El usuario ingresa el numero de solicitud
    And El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Ingresar solicitud' en la pantalla de Administrar Solicitudes
    And El usuario completa los campos de Informacion General con los siguientes datos: '<Vendedor>', '<Solicitante>', '<MuestraDeclarada>'
    And El usuario completa los campos de Otros Datos Generales con los siguientes datos: '<Pagador>', '<FormaPago>'
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito
    # STEP 3
    When El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Verificar' en la pantalla de Administrar Solicitudes
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito

    Examples:
      | usuario         | password     | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada | Pagador | FormaPago |
      | automatizador03 | QA.Prov-2021 | 32003       | 02     | 01    | 1200 | 30771    | MAIZ             | 1       | 3         |

  @Paso4 @Completo
  Scenario Outline: Proceso Incompleto Flujo 3 - Paso 4
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    # STEP 1
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud
    And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'
    And El usuario guarda los cambios
    And El usuario decide no abrir el contenedor
    Then El usuario visualiza el popup de exito
    # STEP 2
    When El usuario ingresa el numero de solicitud
    And El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Ingresar solicitud' en la pantalla de Administrar Solicitudes
    And El usuario completa los campos de Informacion General con los siguientes datos: '<Vendedor>', '<Solicitante>', '<MuestraDeclarada>'
    And El usuario completa los campos de Otros Datos Generales con los siguientes datos: '<Pagador>', '<FormaPago>'
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito
    # STEP 3
    When El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Verificar' en la pantalla de Administrar Solicitudes
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito
    # STEP 4
    When El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Examples:
      | usuario         | password     | puestoTrabajo   | EMA | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada | Pagador | FormaPago |
      | automatizador03 | QA.Prov-2021 | LAPTOP-8VNHLSCQ | 403 | 32003       | 02     | 01    | 1200 | 30771    | MAIZ             | 1       | 3         |

  @Paso55 @Completo
  Scenario Outline: Proceso Incompleto Flujo 3 - Completo
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    # STEP 1
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud
    And El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>', '<Peso>'
    And El usuario guarda los cambios
    And El usuario decide no abrir el contenedor
    Then El usuario visualiza el popup de exito
    # STEP 2
    When El usuario ingresa el numero de solicitud
    And El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Ingresar solicitud' en la pantalla de Administrar Solicitudes
    And El usuario completa los campos de Informacion General con los siguientes datos: '<Vendedor>', '<Solicitante>', '<MuestraDeclarada>'
    And El usuario completa los campos de Otros Datos Generales con los siguientes datos: '<Pagador>', '<FormaPago>'
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito
    # STEP 3
    When El usuario selecciona la celda que contiene la muestra
    And El usuario hace click en 'Verificar' en la pantalla de Administrar Solicitudes
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito
    # STEP 4
    When El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    # STEP 5
    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'muestra' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros ''
    And Se ingresan los datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado> en EMA
    And El usuario abre el contenedor: <ContGeneral>
    And El usuario valida los resultados ingresados en EMA
    And El usuario cierra el contenedor: <ContGeneral>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    # STEP 6
    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    ## ContGeneral
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContGeneral>'
    When Se ingresa la porcion a Corte
    And Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    # STEP 7
    Given El usuario se dirige a la pantalla 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>
    # STEP 8
    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContPorciones>'
    And Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>
    # STEP 9
    Given El usuario se dirige a la pantalla 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    When Se Ingresan los siguientes Componentes '<CodAnalista>', '<GranosDanados>', '<MatExtranas>', '<QuebradosyChuzo>', '<Picados>', '<OtroTipo>', '<Pureza>'
    # STEP 10
    Given El usuario se dirige a la pantalla 'Consultar Resultados de Ensayos de una Solicitud'
    When El usuario ingresa el numero de solicitud en la pantalla Consultar Resultados de Ensayos de una Solicitud
    # STEP 11
    Given El usuario se dirige a la pantalla 'Administrar Facturas de Contado'
    When El usuario hace click en 'Agregar'
    And El usuario completa los campos de Administrar Facturas de Contado con el código de cliente '<Solicitante>'
    And El usuario hace click en '_Agregar'
    And El usuario ingresa el concepto '<concepto>' e importe total '<importeTotal>'
    And El usuario hace click en 'Aceptar'
    And El usuario selecciona la forma de pago '<descFormaPago>'
    And El usuario selecciona la celda de la solicitud
    And El usuario guarda los cambios
    Then El usuario visualiza el popup de exito

    Examples:
      | usuario         | password     | puestoTrabajo   | EMA | CORTE | FISICO | AyDM | ContGeneral | ContPorciones | ContDestino | CodAnalista | GranosDanados | MatExtranas | QuebradosyChuzo | Picados | OtroTipo | Pureza | CodMesada | PH | HEA | CP | CodEquipo | resultado                               | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada | Pagador | FormaPago | concepto                     | importeTotal | descFormaPago |
      | automatizador03 | QA.Prov-2021 | LAPTOP-8VNHLSCQ | 403 | 404   | 405    | 407  | AUCG06      | AUCP01        | QAAG06      | 41          | 0,01          | 0,02        | 0,00            | 0,00    | 0,00     | 0,00   | 3         | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos. | 32003       | 02     | 01    | 1200 | 30771    | MAIZ             | 1       | 3         | Honorarios por Análisis Maíz | 752          | PAGO DIFERIDO |

