Feature: Flujo Basico 4

  @Flujo4 @Completo
  Scenario Outline: Proceso Completo Flujo 4 - Exitoso
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    And El usuario carga un archivo solici y valida el ingreso de muestras con los parametros <direccion>, <nombreArchivo>, <CodPuerto>
    When El Usuario ingresa una solicitud automatica con los parametros <CodPuerto>, <direccion>, <nombreArchivo>, <ContGeneral>
    And El usuario cierra el contenedor con los parametros <ContGeneral>, <direccion>, <nombreArchivo>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContGeneral>'
    # then validar resultados
    
    Given El usuario se dirige a la pantalla 'Ingresar Resultados EMA'
    And Se ingresa la muestra en EMA
    And Se ingresan los siguientes datos al sector EMA: '<CodAnalista>', '<PH>', '<HEA>', '<CP>', '<CodEquipo>'
    When El usuario hace click en el boton 'Aceptar Componente'
    And Se abre el contenedor: '<ContGeneral>'
    And El usuario valida los resultados ingresados en EMA
    When El usuario cierra el contenedor: <ContGeneral>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <CORTE>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContGeneral>'
    When Se ingresa la porcion a Corte
    And Se abre el contenedor: '<ContPorciones>'
    And Se Validan la muestras ingresadas <ContPorciones>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Muestra o Contenedor a Archivo'
    When Se ingresa la muestra al archivo <ContDestino>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <FISICO>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContPorciones>'
    And Se ingresa al sector LabFisico con los siguientes datos <CodAnalista>, <CodMesada>, <ContPorciones>

    Given El usuario se dirige a la pantalla 'Ingresar Componentes Ensayos Sector Balanza-Físico'
    When Se Ingresan los siguientes componentes <CodAnalista>, <ArdyDanado>, <GranosDanados>, <MatExtranas>, <QuebradosyChuzo>, <PanzaBlanca>, <Picados>, <Pureza>

    Given El usuario se dirige a la pantalla 'Re-analizar Ensayo con Resultado Correcto'
    When El usuario ingresa el numero de solicitud ya obtenido
    And El usuario selecciona el checkbox de 'Peso hectolítrico'
    And El usuario hace click en 'Re-análisis del ensayo por el mismo método' en la pantalla de Re-analizar Ensayo con Resultado Correcto
    And El usuario ingresa 'Error ana' en el input de tipo motivo
    And El usuario ingresa 'QA PRUEBAS' en el input de observaciones
    And El usuario hace click en el boton 'Aceptar Re-análisis' y da enter
    Then El usuario visualiza el popup de exito

    Given El usuario se dirige a la pantalla 'Consultar Resultados de Ensayos de una Solicitud'
    When El usuario ingresa el numero de solicitud ya obtenido y da enter

    Given El usuario se dirige a la pantalla 'Pedido de Extracción'
    When El usuario hace click en el boton 'Agregar'
    And El usuario completa los detalles en la pantalla de Pedido de Extraccion con los siguientes parametros: 'Laboratorio Físico Comercial', 'Baja', 'Técnico', 'Pendiente', '41'
    And El usuario hace click en el boton 'Listar Solicitudes'
    When El usuario selecciona la celda que contiene la muestra
    And El usuario guarda los cambios
    And El usuario visualiza el popup de exito
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <AyDM>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Administrar Archivo'
    When El usuario hace click en el boton 'Extraer'
    And El usuario hace click en el boton 'Extraer'
    When El usuario extrae la muestra del archivo '<ContGeneral>'
    And El usuario hace click en el boton 'Aceptar'
    And El usuario cierra el contenedor: <ContGeneral>
    And El usuario realiza el cambio de puesto de trabajo <puestoTrabajo> a <EMA>
    Then Se reinicia el aplicativo y se inicia sesion con las credenciales <usuario>, <password>

    Given El usuario se dirige a la pantalla 'Ingresar Contenedor o Muestra/Porción a un sector'
    When El usuario ingresa el código de 'contenedor' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '<ContGeneral>'
    And Se ingresan los datos <CodAnalista>, <PH>, <HEA>, <CP>, <CodEquipo>, <resultado> en EMA
    And El usuario decide no abrir el contenedor
    Then El usuario valida los resultados ingresados en EMA

    Given El usuario se dirige a la pantalla 'Consultar Resultados de Ensayos de una Solicitud'
    When El usuario ingresa el numero de solicitud en la pantalla Consultar Resultados de Ensayos de una Solicitud

    Given El usuario se dirige a la pantalla 'Listar Solicitudes Pendientes de Decisión de Resultado'
    And El usuario selecciona el checkbox de 'Lab. físico - EMA'
    When El usuario hace click en el boton 'Filtrar'
    And El usuario selecciona la celda que contiene la muestra
    Then El usuario hace click en el boton 'Aceptar Solicitud'

    Given El usuario busca en el menu por la palabra 'Vaciar Contenedor'
    Then Se ingresan los siguientes contenedores <ContGeneral>, <ContPorciones>

    Examples:
      | usuario         | password     | puestoTrabajo   | CodPuerto | direccion                                     | nombreArchivo | EMA | CORTE | FISICO | AyDM | ContGeneral | ContGeneralDos | ContPorciones | ContDestino | CodAnalista | ArdyDanado | GranosDanados | MatExtranas | QuebradosyChuzo | PanzaBlanca | Picados | OtroTipo | Pureza | CodMesada | PH | HEA | CP | CodEquipo | resultado                                | Solicitante | Matriz | Grupo | Peso | Vendedor | MuestraDeclarada | Pagador | FormaPago | concepto                     | importeTotal | descFormaPago |
      | automatizador02 | QA.Prov-2021 | DESKTOP-TMHRKRA | 24802     | \src\test\resources\solici\FlujosBasicos\FB4\ | FB4           | 403 | 404   | 405    | 407  | AUCG07      | AUCG07         | AUCP01        | QAAG06      | 41          | 0,01       | 0,01          | 0,00        | 0,00            | 0,00        | 0,00    | 0,00     | 45     | 3         | 72 | 15  | 12 | 482       | Los resultados se encuentran correctos.  | 32003       | 02     | 01    | 1200 | 30771    | MAIZ             | 1       | 3         | Honorarios por Análisis Maíz | 752          | PAGO DIFERIDO |