Feature: Ingreso de Solici
@ImportacionArchivoSolici
Scenario Outline: Proceso importacion archivo solici - Caso exitoso
Given El usuario inicia sesion con sus credenciales <usuario>, <password>
When Se carga un archivo solici <direccion>, <nombreArchivo>, <CodPuerto>
Then Se valida el ingreso de muestras
 Examples:
  | CodPuerto | usuario         | password     | direccion                                     | nombreArchivo |
  | 24802     | automatizador01 | QA.Prov-2021 | \src\test\resources\solici\FlujosBasicos\FB1\ | Solici01      |
