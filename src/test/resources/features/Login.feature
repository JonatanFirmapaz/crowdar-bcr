Feature: Login

  @LoginExitoso
  Scenario Outline: Logueo de usuario - El usuario ingresa las credenciales correctas
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    Then Se verifica que sea correcto el ingreso

    Examples:
      | usuario         | password     |
      | automatizador03 | QA.Prov-2021 |


  @LoginFallido
  Scenario Outline: Logueo de usuario - El usuario ingresa las credenciales Incorrectas
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    Then Se verifica que el usuario y password son incorectas

    Examples:
      | usuario         | password     |
      | automatizador00 | QA.Prov-2021 |


  @LoginEnBlanco
  Scenario Outline: Logueo de usuario - El usuario no ingresa las credenciales
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    Then Se verifica la falta de credenciales

    Examples:
      | usuario | password |
      |         |          |
