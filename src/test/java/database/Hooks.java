package database;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

import org.apache.log4j.Logger;

import java.io.IOException;

public class Hooks {

    @Before()
    public void setScenario(Scenario scenario) {
        Logger.getRootLogger().info("-----------" + scenario.getName() + "-----------");
    }

    @After()
    public void dismissAll(Scenario scenario) {
        Logger.getRootLogger().info(" ending -----------" + scenario.getName() + "-----------");
        try {
            Runtime.getRuntime().exec( "taskkill /F /IM BCR.UI.Launcher.exe" );
            Runtime.getRuntime().exec( "taskkill /F /IM BCR.Lab.UI.Desktop.Shell.exe" );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}