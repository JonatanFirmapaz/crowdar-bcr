package com.crowdar.examples.utils;

import com.crowdar.core.Constants;
import com.crowdar.core.LocatorManager;
import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class ActionsManager {
    public static Robot r;

    static {
        try {
            r = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public static void clickWithoutWait(String locatorElement) {
        By elementToBeClicked = LocatorManager.getLocator(locatorElement);
        DriverManager.getDriverInstance().findElement(elementToBeClicked).click();
    }

    public static void clickWithoutWait(String locatorElement, String... locatorReplacementValues) {
        WebElement elementToBeClicked = DriverManager.getDriverInstance().findElement(By.xpath(String.format(locatorElement, locatorReplacementValues)));
        elementToBeClicked.click();
    }

    public static boolean waitVisibilityFromTargetDynamicTimeout(String locator, int duration)
    {
        By target = LocatorManager.getLocator(locator);

        getFluentWaitFromTargetDynamicTimeOut(duration).until(ExpectedConditions.visibilityOfElementLocated(target));
        return true;
    }

    public static Wait<EventFiringWebDriver> getFluentWaitFromTargetDynamicTimeOut(int duration)
    {
        FluentWait fluentWait = new FluentWait(DriverManager.getDriverInstance());
        return fluentWait.withTimeout(Duration.ofSeconds(duration))
                .pollingEvery(Duration.ofMillis(Constants.getFluentWaitRequestFrequencyInMillis()))
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class);
    }

    public static void customSendKeys(long waitIntervaloInMillis, int tabsToArriveField, String text, boolean enter) {
        if (waitIntervaloInMillis != 0) {
            for (int i = 0; i < tabsToArriveField; i++) {
                pressKey(KeyEvent.VK_TAB, waitIntervaloInMillis);
            }
        } else {
            for (int i = 0; i < tabsToArriveField; i++) {
                pressKey(KeyEvent.VK_TAB);
            }
        }

        customSendKeys(text, enter);
    }

    public static void customSendKeys(String text, boolean enter) {
        if (!StringUtils.isBlank(text)) {
            byte[] chars = text.getBytes(StandardCharsets.UTF_8);
            for (byte c: chars) {
                r.keyPress(c);
                r.keyRelease(c);
            }
        }

        if (enter) {
            r.keyPress(KeyEvent.VK_ENTER);
            r.keyRelease(KeyEvent.VK_ENTER);
        }
    }

    public static void customSendKeysWithTimeout( long timeout, String text, boolean enter ) {
        try {
            Thread.sleep( timeout );
            customSendKeys( text, enter );
        } catch ( InterruptedException e ) {
            e.printStackTrace();
        }
    }

    public static void customPressKey(long waitInMillis, int tabs, int keyEvent) {
        if ( tabs > 0 ) {
            for ( int i = 0; i < tabs; i++ ) {
                if ( waitInMillis > 0 ) {
                    try {
                        Thread.sleep( waitInMillis );
                    } catch ( InterruptedException e ) {
                        e.printStackTrace();
                    }
                }
                pressKey( KeyEvent.VK_TAB );
            }
        }
        pressKey( keyEvent, waitInMillis );
    }

    public static void pressKey(int k, int count, long waitIntervaloInMillis) {
        try {
            for (int i = 0; i < count; i++) {
                if (waitIntervaloInMillis != 0)
                    Thread.sleep(waitIntervaloInMillis);
                r.keyPress(k);
                r.keyRelease(k);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void pressKey(int k) {
        r.keyPress(k);
        r.keyRelease(k);
    }

    public static void pressKey(int k, long waitInMillis) {
        try {
            if (waitInMillis != 0)
                Thread.sleep(waitInMillis);
            r.keyPress(k);
            r.keyRelease(k);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String incrementarNumeroMuestra(String muestra) {
        String[] lastChars = muestra.split("[^0-9]+");
        String lastIncrementedChars = String.valueOf(Integer.parseInt(lastChars[lastChars.length - 1]) + 1);
        return muestra.replaceFirst("[0-9]+", lastIncrementedChars);
    }

    public static String cambiarMuestra(String direccion, String nombreArchivo) {
        try {
            String path = System.getProperty("user.dir").concat(direccion).concat(nombreArchivo).concat(".txt");
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            List<Object> listadoLineasSolici;
            listadoLineasSolici = Arrays.asList(bufferedReader.lines().toArray());
            StringBuilder nuevasLineas = new StringBuilder();
            String numMuestra = listadoLineasSolici.get(0).toString();
            String numeroNuevoMuestra = numMuestra.substring(0, 2);
            String fechaMuestra = numMuestra.substring(2, 8);
            String fechaDelDia = new SimpleDateFormat("ddMMYY").format(new java.util.Date());
            String flujo = numMuestra.substring(8, 12);

            Integer intento = Integer.valueOf(numMuestra.substring(12, 14));
            if (fechaMuestra.equals(fechaDelDia)) {
                intento++;
                numeroNuevoMuestra = numeroNuevoMuestra + fechaMuestra + flujo + String.format("%02d", intento);
            } else {
                numeroNuevoMuestra = numeroNuevoMuestra + fechaDelDia + flujo + "00";
            }
            numMuestra = numMuestra.replace(numMuestra, numeroNuevoMuestra + " ");
            nuevasLineas.append(numMuestra).append("\r\n");

            FileWriter fileWriter = new FileWriter(path, false);
            fileWriter.write(String.valueOf(nuevasLineas));

            fileWriter.close();
            fileReader.close();
            bufferedReader.close();

            return numMuestra;
        }catch(Exception e){
            System.out.println("Falló " + e);
        }
        return null;
    }

    public static boolean isVisibilityNotRunTimeException(String locatorName, String... locatorReplacementValues) {
        try {
            return DriverManager.getDriverInstance().findElement(By.xpath(String.format(locatorName, locatorReplacementValues))).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean waitForElement(String locator, String... replacementValues) {
        DriverManager.getDriverInstance().manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
        boolean result;
        try {
            long tryings = 0;
            while (!isVisibilityNotRunTimeException(locator, replacementValues)) {
                Thread.sleep(200);
                tryings++;
                if (tryings == 30) throw new RuntimeException();
            }
            result = true;
        } catch (RuntimeException | InterruptedException e) {
            Logger.getLogger(ActionsManager.class).info(String.format("Element %s not found !", String.format(locator, replacementValues)));
            result = false;
        } finally {
            DriverManager.getDriverInstance().manage().timeouts().implicitlyWait(Constants.getWaitImlicitTimeout(), TimeUnit.SECONDS);
        }
        return result;
    }

    public static void checkAlertInformation(String text) {
        try {
            if (waitForElement("//*[contains(@Name, '%s')]", text)) {
                pressKey(KeyEvent.VK_ENTER);
            } else {
                throw new RuntimeException(String.format("¡Error, element with TEXT [%s] NOT found!", text));
            }

        } catch (RuntimeException e) {
            Logger.getLogger(ActionsManager.class).error(e.getMessage());
        }
    }

    public static boolean theFollowingAlertIsPresent(String text) {
        try {
            if (waitForElement("//*[contains(@Name, '%s')]", text)) {
                return true;
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean theFollowingButtonIsPresent(String xpath) {
        try {
            if (waitForElement(xpath)) {
                return true;
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void scrollWithoutReference( String containerLocator, String target ) {
        try {
            WebElement element = DriverManager.getDriverInstance().findElement( By.xpath( containerLocator ) );
            int xPos = element.getLocation().x;
            int yPos = element.getLocation().y;
            int xSiz = element.getSize().width;
            int ySiz = element.getSize().height;
            int xOrig = xPos + ( xSiz / 2 );
            int yOrig = yPos + ( ySiz / 2 );
            r.mouseMove( xOrig, yOrig );
            while ( !ActionsManager.isVisibilityNotRunTimeException( target, "" ) ) {
                r.mouseWheel( 50 );
            }
            DriverManager.getDriverInstance().findElement( By.xpath( target ) ).click();
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}