package com.crowdar.examples.utils;

import com.crowdar.driver.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class KeyboardManager {
    private static final String COMMON_INPUT = "//*[@AutomationId='xamComboControl']|//*[@AutomationId='checkBoxToolControl']|//*[@ClassName='Button']/*[@Name='...']";
    private static final String PARAMETRIZED_INPUT = "//*[@AutomationId='labelToolControl'][@Name='%s']/../*[2]";

    public static List<WebElement> getCommonInput() {
        return DriverManager.getDriverInstance().findElements(By.xpath(COMMON_INPUT));
    }

    private static List<WebElement> getEnabledComponents(List<WebElement> components) {
        List<WebElement> enabledComponents = new ArrayList<>();
        for (WebElement component : components) {
            if (component.getAttribute("HasKeyboardFocus").equalsIgnoreCase("true")) {
                System.out.println("OK");
            }
            if (component.isEnabled()) enabledComponents.add(component);
        }
        return enabledComponents;
    }

    public static void setDynamicInput(String label, String text, boolean enter) {
        Point location = getInputLocation(label);
        List<WebElement> inputs = getEnabledComponents(getCommonInput());
        if (inputs.isEmpty()) throw new RuntimeException();
        List<Point> locations = inputs.stream().map(WebElement::getLocation).collect(Collectors.toList());
        int tabs = processNeededTabs(locations, location);
        ActionsManager.customSendKeys(500, tabs, text, enter);
    }

    public static int processNeededTabs(List<Point> locations, Point searchedLocation ) {
        int tabs = -1;
        for (Point location : locations) {
            if (isSameLocation(location, searchedLocation)) break;
            else tabs++;
        }
        return tabs;
    }

    public static boolean isSameLocation(Point currentInputLocation, Point inputSearchedLocation) {
        return inputSearchedLocation.equals(currentInputLocation);
    }

    public static Point getInputLocation(String label) {
        return DriverManager.getDriverInstance().findElement(By.xpath(String.format(PARAMETRIZED_INPUT, label))).getLocation();
    }
}