package com.crowdar.examples.utils;

public class UserManager {

    private static ThreadLocal<UserManager> INSTANCE = new ThreadLocal<>();

    private static String username;
    private static String password;
    private static String puesto;

    // Datos para flujos ~~~~~
    private String numeroSolicitud;
    private String muestraModificada;
    private String solicitante;
    private String contenedor;

    private UserManager() {}

    public static UserManager getInstance() {
        if (INSTANCE.get() == null) {
            INSTANCE.set(new UserManager());
        }
        setMandatoryData();
        return INSTANCE.get();
    }

    private static void setMandatoryData() {
        setPuesto( getEnvProperty( "CLIENTNAME" ) );
        setUsername( getEnvProperty( "USERNAME" ) );
    }

    public static void setPuesto(String pst) {
        puesto = pst;
    }

    public String getPuesto(){
        return puesto;
    }

    public static void setUsername(String usr) {
        username = usr;
    }

    public static String getEnvProperty(String property) {
        String propertyValue = System.getenv( property );
        if ( propertyValue == null )
            throw new RuntimeException();
        return propertyValue;
    }

    public String getUsername() {
        return username;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setMuestra(String muestraModificada) {this.muestraModificada = muestraModificada;}

    public String getMuestra() {
        return muestraModificada;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setContenedor(String contenedor) {
        this.contenedor = contenedor;
    }

    public String getContenedor() {
        return contenedor;
    }
}
