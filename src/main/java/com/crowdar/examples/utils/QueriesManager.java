package com.crowdar.examples.utils;

import com.crowdar.examples.database.util.Database;
import com.crowdar.examples.database.util.SqlFileReader;

public class QueriesManager {

    private static ThreadLocal<QueriesManager> INSTANCE = new ThreadLocal<>();

    private String numeroSolicitud;

    private QueriesManager() {}

    public static QueriesManager getInstance() {
        if (INSTANCE.get() == null) {
            INSTANCE.set(new QueriesManager());
        }

        return INSTANCE.get();
    }

    public String getNumeroSolicitud() {
        return getInstance().numeroSolicitud;
    }

    public String consultarNumeroSolicitud(String muestra) {
        try {
            String query = SqlFileReader.getQueryString("generales/obtenerNumeroSolicitud.sql");
            String updateQuery = query.replaceFirst("\\?", "'"+muestra+"'");
            return (String)Database.executeQuery(updateQuery).get(0).get("Numero_Solicitud");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
