package com.crowdar.examples.utils;

import com.crowdar.examples.services.BusquedaAvanzadaService;

public enum FlowActions {

    FECHAINGRESO {
        @Override
        public void performBehavior() {
            BusquedaAvanzadaService.setDate();
        }
    };

    public abstract void performBehavior();
}
