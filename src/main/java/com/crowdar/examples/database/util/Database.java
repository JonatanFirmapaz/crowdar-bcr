package com.crowdar.examples.database.util;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.crowdar.core.PropertyManager;

public class Database {

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(PropertyManager.getProperty("crowdar.base.connection"));
    }

    public static void closeConnection() throws SQLException {
        getConnection().close();
    }

    public static List<Map<String, Object>> executeQuery(String queryString) throws SQLException {
        List<Map<String, Object>> records = null;
        ResultSet rs;
        try {
            try (Statement st = getConnection().createStatement()) {
                rs = st.executeQuery(queryString);
                if (rs != null) {
                    ResultSetMetaData md = rs.getMetaData();
                    int columns = md.getColumnCount();
                    // list to have all rows
                    records = new ArrayList<>(columns);
                    // load data for each column / record
                    while (rs.next()) {
                        HashMap<String, Object> row = new HashMap<>(columns);
                        // add column/value for a record
                        for (int i = 1; i <= columns; ++i) {
                            row.put(md.getColumnName(i), rs.getObject(i));
                        }
                        records.add(row);
                    }
                }
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                getConnection().close();
            } catch (SQLException e) {
                throw e;
            }
        }

        return records;
    }

    public static boolean executeUpdate(String ddlString) throws SQLException {
        int recAffected = 0;
        try (Statement st = getConnection().createStatement()) {
            recAffected = st.executeUpdate(ddlString);
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                getConnection().close();
            } catch (SQLException e) {
                throw e;
            }
        }
        return recAffected >= 0;
    }

    public static  Object executeQueryWithParameters(String sqlString, Map<Integer, Object> parameters) throws SQLException {
        /*
         * establish connection here
         */
        PreparedStatement preparedStatement = getConnection().prepareStatement(sqlString);

        if(parameters != null){
            /*
             * Iterate over the map to set parameters
             */
            for(Integer key : parameters.keySet()){
                preparedStatement.setObject(key, parameters.get(key));
            }
        }

        if(sqlString.startsWith("UPDATE") || sqlString.startsWith("DELETE") || sqlString.startsWith("INSERT")|| sqlString.startsWith("SET")){
            int recAffected = preparedStatement.executeUpdate();
            return recAffected > 0;
        }else{
            ResultSet rs = preparedStatement.executeQuery();
            return rs;
        }
    }

    public static  Object executeMixQuery(String sqlString, Map<Integer, Object> parameters) throws SQLException {
        /*
         * establish connection here
         */
        PreparedStatement preparedStatement = getConnection().prepareStatement(sqlString);

        if(parameters != null){
            /*
             * Iterate over the map to set parameters
             */
            for(Integer key : parameters.keySet()){
                preparedStatement.setObject(key, parameters.get(key));
            }
        }
        ResultSet rs = null;
        if(sqlString.startsWith("SET")){
            rs = preparedStatement.executeQuery();
            return rs;
        }
        return rs;
    }

}
