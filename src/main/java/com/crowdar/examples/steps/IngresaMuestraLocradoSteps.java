package com.crowdar.examples.steps;

import com.crowdar.examples.services.ImportacionArchivoService;
import com.crowdar.examples.services.IngresarMuestraSoliLacradoService;
import io.cucumber.java.en.And;

public class IngresaMuestraLocradoSteps {
    @And("El usuario selecciona empleado firmante (.*)")
    public void elUsuarioSeleccionaEmpleadoFirmanteCodFirmante(String codFirmante) {
        ImportacionArchivoService.buscarInputMenu("Seleccionar Empleado Firmante de Lacrado");
        IngresarMuestraSoliLacradoService.seleccionaFirmante(codFirmante);
        IngresarMuestraSoliLacradoService.clickGuardar();
        IngresarMuestraSoliLacradoService.popUpInfo();


    }
    @And ("El usuario ingresa por la pantalla Cuartear Muestra de Lacrado")
    public void elUsuarioIngresaPantallaCuartearMuestraLacrado(){
        ImportacionArchivoService.buscarInputMenu("Cuartear Muestra de Lacrado");
        IngresarMuestraSoliLacradoService.ingresaNroMuestra();


    }
}
