package com.crowdar.examples.steps;

import com.crowdar.examples.services.ServiceInCommon;
import io.cucumber.java.en.*;

public class ExtraerMuestrasSteps {

    @And("El usuario hace click en el boton '(.*)' en la pantalla Extraer Muestras")
    public void elUsuarioHaceClickEnElBoton( String name ) {
        ServiceInCommon.clickButon(name);
    }
    }

