package com.crowdar.examples.steps;

import com.crowdar.examples.services.VaciarContenedoresService;
import io.cucumber.java.en.When;

public class VaciarContenedoresSteps {

    @When("Se ingresan los siguientes contenedores (.*), (.*)")
    public void seIngresanLosSiguientesContenedores(String contGeneral, String contPorciones) {
        VaciarContenedoresService.inputCodContenedor(contGeneral);
        VaciarContenedoresService.botonAceptar();
        VaciarContenedoresService.inputCodContenedor(contPorciones);
        VaciarContenedoresService.botonAceptar();
    }

    @When("Se ingresa el siguiente contenedor (.*)")
    public void seIngresaElSiguienteContenedor(String contPorciones) {
        VaciarContenedoresService.inputCodContenedor(contPorciones);
        VaciarContenedoresService.botonAceptar();
    }
}
