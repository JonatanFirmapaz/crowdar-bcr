package com.crowdar.examples.steps;

import com.crowdar.examples.services.FechaYestadoSolicutudService;
import io.cucumber.java.en.Given;

import java.io.IOException;
import java.sql.SQLException;

public class FechaYestadoSolicitudSteps {

    @Given("El usuario obtiene la fecha de ingreso de la solicitud mediante el (.*), (.*)")
    public void ElUsuarioObtieneLaFechaDeIngresoDeLaSolicitudMedianteEl(String direccion, String nombreArchivo) throws SQLException, IOException {
        FechaYestadoSolicutudService.fechaingresoSolicitud(direccion, nombreArchivo);
        FechaYestadoSolicutudService.fechaSolicitud(direccion, nombreArchivo);
        FechaYestadoSolicutudService.crearTxt(direccion, nombreArchivo);
    }
}
