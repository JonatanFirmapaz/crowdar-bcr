package com.crowdar.examples.steps.LeerMuestraPorcionParaIngresarComponentes;

import com.crowdar.core.PageSteps;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.utils.ActionsManager;
import io.cucumber.java.en.And;
import org.openqa.selenium.By;

import java.awt.event.KeyEvent;

public class LeerMuestraPorcionParaIngresarComponentesSteps extends PageSteps {

    @And( "^El usuario completa el formulario en la pantalla Leer Muestra o Porción para ingresar componentes con los siguientes datos: '(.*)', '(.*)', '(.*)', '(.*)'$" )
    public void elUsuarioCompletaElFormulario( String codAnalista, String semillas, String pesoSobreZaranda, String pesoBajoZaranda ) {
        ActionsManager.waitForElement( "//*[@Name='Código Analista a Replicar: ']/../*[2]" );
        //DriverManager.getDriverInstance().findElement( By.xpath( "//*[@Name='Código Analista a Replicar: ']/../*[2]" ) ).sendKeys( codAnalista );
        ActionsManager.customSendKeys( codAnalista, true );
        ActionsManager.pressKey(KeyEvent.VK_ENTER);
        ActionsManager.customPressKey( 500, 3, KeyEvent.VK_DOWN ); // fecha
        ActionsManager.customSendKeys( 100, 1, null, true ); // pre-cargar analista
        ActionsManager.customSendKeys( 2000, 1, null, true ); // pre-cargar fecha
        ActionsManager.customSendKeys( 100, 4, semillas, true );
        ActionsManager.customSendKeys( 100, 1, pesoSobreZaranda, true );
        ActionsManager.customSendKeys( 100, 1, pesoBajoZaranda, true );
    }
}
