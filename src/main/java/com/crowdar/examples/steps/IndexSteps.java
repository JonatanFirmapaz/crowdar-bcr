package com.crowdar.examples.steps;

import com.crowdar.core.Injector;
import com.crowdar.core.PageSteps;
import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.drivers.DesktopDriver;
import com.crowdar.examples.drivers.LaboratorioDriver;
import com.crowdar.examples.drivers.LauncherDriver;
import com.crowdar.examples.services.ImportacionArchivoService;

import java.sql.Driver;
import java.util.Locale;
import com.crowdar.examples.services.IndexService;

import com.crowdar.examples.services.LoginService;
import io.cucumber.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.Map;

public class IndexSteps extends PageSteps {

    @Given("Hago click en About y completo ip")
    public void about() {

    //Inicializo el driver de Launcher
        LauncherDriver.open();

    //Seteo User y Pass en Login
        LoginService.setUsername("automatizador01");
        LoginService.setPassword("QA.Prov/2021");
        LoginService.login();
        LoginService.goToLaboratorio();
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    //Cierro Driver Launcher
        LauncherDriver.close();
    //Inicializo Driver Desktop (Para obtener appTopLevelWindow)
        DesktopDriver.open();
    //Obtengo el appTopLevelWindow de laboratorio
        String laboratorioApp = DesktopDriver.getLaboratorioWindow();

    //Cierro Driver de Desktop
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    //Inicializo Driver Laboratorio
        LaboratorioDriver.open(laboratorioApp);
    //Clickeo boton cerrar de Laboratorio
        ImportacionArchivoService.close();

    //Cierro Driver Laboratorio
        LaboratorioDriver.close();

    }

}
