package com.crowdar.examples.steps;

import com.crowdar.examples.services.IngresoResultadosEmaService;
import com.crowdar.examples.services.IngresoSectorHumedadService;
import io.cucumber.java.en.And;

public class IngresoSectorHumedadSteps {

    @And("^Se ingresa la muestra al sector$")
    public void seIngresaLaMuestraEnHUMEDAD() throws InterruptedException {
        IngresoResultadosEmaService.ingresarMuestra();
    }

    @And("^Se ingresan los siguientes datos al sector HUMEDAD: (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void seIngresanLosSiguientesDatosAlSectorHUMEDADCodAnalista(String codAnalista, String TC1, String TC2, String PM1, String PM2, String PCMS1, String PCMS2) {
        IngresoSectorHumedadService.verificarPantalla();
        IngresoSectorHumedadService.ingresarCodAnalista(codAnalista);
        IngresoSectorHumedadService.ingresaFecha();
        IngresoSectorHumedadService.ingresarValorTC1(TC1);
        IngresoSectorHumedadService.ingresarValorTC2(TC2);
        IngresoSectorHumedadService.ingresarValorPM1(PM1);
        IngresoSectorHumedadService.ingresarValorPM2(PM2);
        IngresoSectorHumedadService.ingresarValorPCMS1(PCMS1);
        IngresoSectorHumedadService.ingresarValorPCMS2(PCMS2);
    }

    @And("El usuario valida los resultados ingresados en HUMEDAD")
    public void elUsuarioValidaLosResultadosIngresadosEnHUMEDAD() {
        IngresoSectorHumedadService.validarResultados("Resultados con error de criterio de aceptación, se debe realizar");
        IngresoResultadosEmaService.botonCancelar();
    }

    @And("Se ingresa la muestra_Porcion al sector")
    public void seIngresaLaMuestraPorcionAlSector() {
        IngresoSectorHumedadService.ingresarMuestraPorcion();
    }
}
