package com.crowdar.examples.steps;

import com.crowdar.examples.services.ModificarArchivoRMNService;
import io.cucumber.java.en.When;

import java.io.IOException;

public class ModificarArchivoRMNSteps {

    @When("Modificacion de archivo RMN (.*), (.*)")
    public void modificacionDeArchivoRMN(String direccion, String rmn) throws IOException {
        ModificarArchivoRMNService.ArchivoDat(direccion, rmn);
    }
    @When("Se modifica archivo RMN (.*), (.*)")
    public void modificaArchivoRMN(String direccion, String rmn) throws IOException {
        ModificarArchivoRMNService.ArchivoMdt(direccion, rmn);
    }
}
