package com.crowdar.examples.steps;

import com.crowdar.examples.services.IngresarComponentesService;
import com.crowdar.examples.services.IngresoResultadosEmaService;
import io.cucumber.java.en.*;
import org.openqa.selenium.support.ui.Wait;

public class IngresarComponentesSteps {

    @When("Se ingresa al sector con los siguientes datos (.*), (.*), (.*), (.*)")
    public void seIngresaAlSectorConLosSiguientesDatos(String codAnalista, String CSch, String PSZ, String PBZ){
        IngresarComponentesService.ingresarMuestra();
        IngresarComponentesService.inputCodAnalista(codAnalista);
        IngresarComponentesService.selectDate();
        IngresarComponentesService.inputCantSemilla(CSch);
        IngresarComponentesService.inputPesoSobreZaranda(PSZ);
        IngresarComponentesService.inputPesoBajoZaranda(PBZ);
        IngresarComponentesService.clickAceptarComp();
        IngresarComponentesService.clickOnAceptar();
        IngresarComponentesService.clickOnCancelar();

    }


    @When("El usuario modifica los componentes siguientes '(.*)', '(.*)'")
    public void elUsuarioModificaLosComponentesSiguientes(String PSZ, String PBZ) {
        IngresarComponentesService.ingresarMuestra();
        IngresarComponentesService.clickAceptarSolictud();
        IngresarComponentesService.selectComponeteAmodificar();
        IngresarComponentesService.buttonAceptar();
        IngresarComponentesService.inputPSZ_modificado(PSZ);
        IngresarComponentesService.inputPBZ_modificado(PBZ);
        IngresarComponentesService.clickAceptarComp();
    }

    @And("El usuario ingresa detalle de la modificacion '(.*)', '(.*)'")
    public void elUsuarioIngresaDetalleDeLaModificacion(String palabra, String observacion) {
        IngresarComponentesService.inputMotivo(palabra);
        IngresarComponentesService.inputObservacion(observacion);
        IngresarComponentesService.clickOnAceptar();
        IngresarComponentesService.clickOnAceptar();
        IngresarComponentesService.clickOnCancelar();

    }
}