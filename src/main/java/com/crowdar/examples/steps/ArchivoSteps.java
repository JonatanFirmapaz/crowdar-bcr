package com.crowdar.examples.steps;

import com.crowdar.examples.services.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;

public class ArchivoSteps {

    @When("Se ingresa la muestra al archivo (.*)")
    public void seIngresaLaMuestraAlArchivo(String contDestino) {
        ArchivoService.clickAgregar();
        ArchivoService.ingresarMuestra();
        ArchivoService.clickBotonAceptar();
        ArchivoService.validarEstadoMuestra();
        ImportacionArchivoService.buscarInputMenu("Administrar Archivo");
        ArchivoService.clickBotonIngresar();
        ArchivoService.clickBotonAgregar();
        ArchivoService.inputContDestino(contDestino);
        ArchivoService.botonAceptar();
        ArchivoService.validarIngreso();
    }

    @And( "^El usuario extrae la muestra del archivo '(.*)'$" )
    public void seExtraeLaMuestraDelArchivo( String container ) {
        ArchivoService.clickBotonAgregar();
        ArchivoService.inputContDestino(container);
        ArchivoService.botonAceptar();
    }
}
