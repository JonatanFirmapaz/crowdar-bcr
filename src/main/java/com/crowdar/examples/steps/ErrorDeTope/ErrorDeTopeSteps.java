package com.crowdar.examples.steps.ErrorDeTope;

import com.crowdar.examples.services.*;
import io.cucumber.java.en.*;

public class ErrorDeTopeSteps {
    @When("^Se genera listado de tope en '(.*)', '(.*)'$")
    public void seGeneraListadoDeTopeEnSectorTopeAccion(String sector, String buton) {
        ErrorDeTopeService.selectSector(sector);
        ErrorDeTopeService.clickTodos();
        ErrorDeTopeService.clickFiltrar();
        ErrorDeTopeService.clickNroMuestra();
        ErrorDeTopeService.clickAccion(buton);
    }

    @And("^El usuario ingresa en el menu por palabra '(.*)', '(.*)'$")
    public void elUsuarioIngresaEnElMenuPorLaPalabra(String palabra, String observacion){
        ErrorDeTopeService.reAnalizarPorMismoMetodo(palabra);
        ErrorDeTopeService.inputObservacion(observacion);
        ErrorDeTopeService.clickonAceptarOpciones();
        ErrorDeTopeService.clickConfirmarReanalizar();
    }

    @And("El usuario selecciona el checkbox que contiene la muestra")
    public void elUsuarioSeleccionaElCheckboxQueContieneLaMuestra() {
        ErrorDeTopeService.clickCheckNroMuestra();
    }
}
