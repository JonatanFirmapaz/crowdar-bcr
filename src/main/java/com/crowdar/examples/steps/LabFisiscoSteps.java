package com.crowdar.examples.steps;

import com.crowdar.examples.services.*;
import io.cucumber.java.en.When;

import java.awt.*;

public class LabFisiscoSteps {

    @When("Se ingresa al sector LabFisico con los siguientes datos (.*), (.*), (.*)")
    public void seIngresaAlSectorLabFisicoConLosSiguientesDatos(String codAnalista, String codMesada, String codContenedor) {
        ImportacionArchivoService.buscarInputMenu("Asignar Analista A Muestras Porciones Dentro De Un Contenedor");
        LabFisicoService.asignarAnalista(codAnalista);
        LabFisicoService.asignarMesada(codMesada);
        LabFisicoService.asignarContenedor(codContenedor);
    }
}
