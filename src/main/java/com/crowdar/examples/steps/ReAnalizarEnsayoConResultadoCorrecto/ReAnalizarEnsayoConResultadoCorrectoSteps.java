package com.crowdar.examples.steps.ReAnalizarEnsayoConResultadoCorrecto;

import com.crowdar.core.PageSteps;
import com.crowdar.driver.DriverManager;

import com.crowdar.examples.utils.ActionsManager;
import io.cucumber.java.en.And;

import org.openqa.selenium.By;

public class ReAnalizarEnsayoConResultadoCorrectoSteps extends PageSteps {

    @And( "^El usuario hace click en '(.*)' en la pantalla de Re-analizar Ensayo con Resultado Correcto$" )
    public void elUsuarioHaceClickEn( String boton ) {
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "//*[@Name='%s']", boton ) ) ).click();
    }

    @And( "^El usuario ingresa '(.*)' en el input de tipo motivo$" )
    public void elUsuarioIngresaComoTipoMotivo( String text ) {
        ActionsManager.customSendKeys( 0, 2, text.toUpperCase(), true );
    }

    @And( "^El usuario ingresa '(.*)' en el input de observaciones$" )
    public void elUsuarioIngresaComoObservaciones( String text ) {
        ActionsManager.customSendKeys( 0, 1, text.toUpperCase(), true );
    }

}
