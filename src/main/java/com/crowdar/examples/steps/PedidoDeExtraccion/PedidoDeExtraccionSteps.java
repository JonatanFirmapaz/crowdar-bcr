package com.crowdar.examples.steps.PedidoDeExtraccion;

import com.crowdar.core.PageSteps;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.utils.ActionsManager;

import io.cucumber.java.en.And;

import org.openqa.selenium.By;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PedidoDeExtraccionSteps extends PageSteps {

    @And( "^El usuario hace click en '(.*)', e ingresa '(.*)' en la pantalla de Pedido de Extraccion$" )
    public void setInputInPedidoExtraccion( String name, String text ) {
        try {
            Thread.sleep( 5000 );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        DriverManager.getDriverInstance().findElement(By.xpath( String.format( "(//*[@Name='%s']/../*)[2]/*/*", name ) ) ).sendKeys( text );
        ActionsManager.pressKey(KeyEvent.VK_ENTER );
    }

    @And( "^El usuario completa los detalles en la pantalla de Pedido de Extraccion con los siguientes parametros: '(.*)', '(.*)', '(.*)', '(.*)', '(.*)'$" )
    public void completarCampos( String area, String prioridad, String tipoMotivo, String motivo, String codigoResponsable ) {
        try {
            DriverManager.getDriverInstance().findElement( By.xpath( "(//*[@AutomationId=\"PART_FocusSite\"]/Edit[@AutomationId=\"PART_EditableTextBox\"])[1]" ) ).sendKeys( area );
            ActionsManager.pressKey( KeyEvent.VK_ENTER );

            ActionsManager.pressKey( KeyEvent.VK_TAB, 3, 100 );

            DriverManager.getDriverInstance().findElement( By.xpath( "(//*[@AutomationId=\"PART_FocusSite\"]/Edit[@AutomationId=\"PART_EditableTextBox\"])[1]" ) ).sendKeys( prioridad );
            ActionsManager.pressKey( KeyEvent.VK_ENTER );
            ActionsManager.pressKey( KeyEvent.VK_TAB );

            List<String> fields = new ArrayList<>( Arrays.asList( tipoMotivo, motivo ) );
            for ( String field : fields ) {
                ActionsManager.pressKey( KeyEvent.VK_TAB );
                DriverManager.getDriverInstance().findElement( By.xpath( "(//*[@AutomationId=\"PART_FocusSite\"]/Edit[@AutomationId=\"PART_EditableTextBox\"])[1]" ) ).sendKeys( field );
                ActionsManager.pressKey( KeyEvent.VK_ENTER );
            }
            ActionsManager.pressKey( KeyEvent.VK_TAB );
            ActionsManager.pressKey( KeyEvent.VK_DOWN );
            ActionsManager.customSendKeys( 100, 2, codigoResponsable, true );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

}
