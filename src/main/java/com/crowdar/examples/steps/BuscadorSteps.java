package com.crowdar.examples.steps;

import com.crowdar.core.PageSteps;

import com.crowdar.examples.services.ImportacionArchivoService;

import io.cucumber.java.en.When;

public class BuscadorSteps extends PageSteps {

    @When("^El usuario se dirige a la pantalla '(.*)'$")
    public void elUsuarioSeDirigeAlaPantalla(String name) {
        ImportacionArchivoService.buscarInputMenu(name);
    }

}
