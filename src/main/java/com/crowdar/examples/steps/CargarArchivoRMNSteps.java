package com.crowdar.examples.steps;

import com.crowdar.examples.services.CargaAcidezService;
import com.crowdar.examples.services.CargarArchivoRMNService;
import com.crowdar.examples.services.IngresoResultadosEmaService;
import com.crowdar.examples.services.IngresoSectorHumedadService;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;

public class CargarArchivoRMNSteps {

    @When("El usuario carga el archivo RMN con los siguientes datos (.*), (.*), (.*), (.*), (.*), (.*)")
    public void elUsuarioCargaElArchivoRMNConLosSiguientesDatos(String direccion, String nombreArchivo, String extencion, String codAnalista, String codEquipo, String matriz) throws IOException {
        CargarArchivoRMNService.verificarPantalla();
        CargarArchivoRMNService.clickButtonProcesarArchivo();
        CargarArchivoRMNService.inputCodAnalista(codAnalista);
        CargarArchivoRMNService.inputCodEquipo(codEquipo);
        CargarArchivoRMNService.inputMatriz(matriz);
        CargarArchivoRMNService.clickButonSubir();
        CargarArchivoRMNService.importarArchivo(direccion, nombreArchivo, extencion);
        CargarArchivoRMNService.clickProcesarArchivo();
        CargarArchivoRMNService.botonAceptar();
    }


    @When("El usuario carga ensayo de Acidez con los siguientes datos (.*), (.*), (.*), (.*)")
    public void elUsuarioCargaEnsayoDeAcidezConLosSiguientesDatos(String codAnalista2, String TM, String PMA, String V) {
        IngresoSectorHumedadService.ingresarCodAnalista(codAnalista2);
        IngresoSectorHumedadService.ingresaFecha();
        CargaAcidezService.inputTM(TM);
        CargaAcidezService.inputPMA(PMA);
        CargaAcidezService.input_V(V);
    }

    @Then("Se validan los resultados")
    public void seValidanLosResultados() {
        IngresoResultadosEmaService.botonCancelar();
    }
}
