package com.crowdar.examples.steps.ConsultarResultadosDeEnsayosDeUnaSolicitud;

import com.crowdar.core.PageSteps;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;

import java.awt.event.KeyEvent;

public class ConsultarResultadosDeEnsayosDeUnaSolicitud extends PageSteps {

    @When( "^El usuario ingresa el numero de solicitud en la pantalla Consultar Resultados de Ensayos de una Solicitud$" )
    public void elUsuarioIngresaElNumeroDeSolicitud() {
        ActionsManager.customSendKeysWithTimeout( 3000, UserManager.getInstance().getNumeroSolicitud(), true );
    }

    @When( "^El usuario hace click en '(.*)', e ingresa la solicitud en la pantalla de Consultar Resultados de Ensayos de una Solicitud$" )
    public void elUsuarioIngresaLSolicitudYDaEnter( String name ) {
        DriverManager.getDriverInstance().findElement(By.xpath( String.format( "//*[@Name='%s']/../*[2]", name ) ) ).sendKeys( UserManager.getInstance().getNumeroSolicitud() );
        ActionsManager.pressKey(KeyEvent.VK_ENTER );
    }

}
