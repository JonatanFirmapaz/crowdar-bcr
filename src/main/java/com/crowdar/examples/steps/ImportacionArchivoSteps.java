package com.crowdar.examples.steps;

import com.crowdar.core.PageSteps;
import com.crowdar.examples.services.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.Locale;

public class ImportacionArchivoSteps extends PageSteps {
    @Given("El usuario busca en el menu por la palabra '(.*)'")
    public void elUsuarioBuscaEnElMenuPorLaPalabra(String palabraClave) {
        ImportacionArchivoService.buscarInputMenu(palabraClave);
    }

    @When("Clickea el boton '(.*)'")
    public void clickeaElBoton(String nombreBoton) {
        switch (nombreBoton){
            case "Ingresar Solicitud Automática": ImportacionArchivoService.clickBotonIngresarSolicitud();
                break;
            case "Subir": ImportacionArchivoService.clickBotonSubirArchivo();
                break;
            case "Procesar": ImportacionArchivoService.clickBotonProcesar();
        }
    }

    /*@When("Ingresa en el sistema un archivo (.*)")
    public void ingresaEnElSistemaUnArchivo(String nombreArchivo) throws IOException {
        ImportacionArchivoService.importarArchivo(nombreArchivo);
    }*/

    @When("Ingrese el codigo puerto (.*)")
    public void ingresaElCodigoDePuerto(String codigoPuerto) {
        ImportacionArchivoService.ingresaCodigoPuerto(codigoPuerto);
    }

    @Then("Se valida el ingreso de las muestras a la tabla solici")
    public void seValidaElIngresoDeLasMuestrasALaTablaSolici() {
        ImportacionArchivoService.validaIngresoMuestras();
    }

    @Given("El usuario carga un archivo solici y valida el ingreso de muestras con los parametros (.*), (.*), (.*)")
    public void elUsuarioCargaUnArchivoSoliciYValidaElIngresoDeMuestras(String direccion, String nombreArchivo, String codigoPuerto) throws IOException{
        ImportacionArchivoService.buscarInputMenu("Administrar Solicitudes");
        ImportacionArchivoService.clickBotonIngresarSolicitud();
        ImportacionArchivoService.clickBotonSubirArchivo();
        ImportacionArchivoService.importarArchivo(direccion, nombreArchivo);
        ImportacionArchivoService.ingresaCodigoPuerto(codigoPuerto);
        ImportacionArchivoService.clickBotonProcesar();
        //ImportacionArchivoService.validaIngresoMuestras();
    }


    @Then("Se obtiene el numero de solicitud del archivo (.*)")
    public void seObtieneElNumeroDeSolicitudDelArchivo(String direccion, String nombreArchivo) throws SQLException, IOException {
       String respuesta = ImportacionArchivoService.selectNumeroSolicitud(direccion, nombreArchivo);
       ImportacionArchivoService.numeroSolicitud(direccion, nombreArchivo);
    }

    @And("El Usuario ingresa una solicitud automatica con los parametros (.*), (.*), (.*), (.*)")
    public void elUsuarioIngresaUnaSolicitudAutomaticaConLosParametros(String codigoPuerto, String direccion, String nombreArchivo, String codigoContenedor) throws SQLException, IOException, InterruptedException, AWTException {
        ImportacionArchivoService.buscarInputMenu("Ingresar Muestras de Solicitudes Automática");
        IngresoMuestraService.ingresarFechaDelDia();
        IngresoMuestraService.ingresarcodigoPuerto(codigoPuerto);
        IngresoMuestraService.ingresarNumeroDeMuestra(direccion, nombreArchivo);
        IngresoMuestraService.clickEnElBotonAbrirNuevoContenedor();
        IngresoMuestraService.ingresoNombreContenedor(codigoContenedor);
        IngresoMuestraService.clickEnElBotonAbrirContenedor();
        IngresoMuestraService.getModalCreacionContenedor();
    }

    @When("Se carga un archivo solici (.*), (.*), (.*)")
    public void elUsuarioCargaUnArchivoSolici(String direccion, String nombreArchivo, String codigoPuerto) throws IOException {
        ImportacionArchivoService.buscarInputMenu("Administrar Solicitudes");
        ImportacionArchivoService.clickBotonIngresarSolicitud();
        ImportacionArchivoService.clickBotonSubirArchivo();
        ImportacionArchivoService.importarArchivo(direccion, nombreArchivo);
        ImportacionArchivoService.ingresaCodigoPuerto(codigoPuerto);
        ImportacionArchivoService.clickBotonProcesar();
    }

    @Then("Se valida el ingreso de muestras")
    public void seValidaElIngresoDeMuestras(){
        ImportacionArchivoService.validaIngresoMuestras();
    }

    @And("El usuario cierra el contenedor con los parametros (.*), (.*), (.*)")
    public void elUsuarioCierraElContenedorConLosParametros(String codigoContenedor, String direccion, String nombreArchivo) throws SQLException, IOException {
        ImportacionArchivoService.selectNumeroSolicitud(direccion, nombreArchivo);
        ImportacionArchivoService.numeroSolicitud(direccion, nombreArchivo);
        ImportacionArchivoService.buscarInputMenu("Cerrar Contenedor");
        IngresoMuestraService.ingresoNombreContenedorParaCerrarlo(codigoContenedor);
        IngresoMuestraService.clickEnElBotonCerrarContenedor();
        IngresoMuestraService.getModalCierreContenedor();
    }

    @Given("El usuario busca en el menu por la palabra '(.*)' con el parametro (.*)")
    public void elUsuarioBuscaEnElMenuPorLaPalabraClaveYElParametroCodigoContenedor(String palabraClave, String codigoContenedor) {
        ImportacionArchivoService.buscarInputMenu(palabraClave);
        IngresoMuestraService.ingresoNombreContenedorMasEnter(codigoContenedor);
    }

//    @Given("El usuario busca en el menu por la palabra '(.*)'")
//    public void elUsuarioBuscaEnElMenuPorLaPalabraClaveYElParametroCodigoContenedor(String palabraClave) {
//        ImportacionArchivoService.buscarInputMenu(palabraClave);
//        IngresarComponentesService.ingresarMuestra();

//    }

    @And("Ingresa el codigo del contenedor (.*)")
    public void ingresaElCodigoDelContenedorCodigoContenedor(String codigoContenedor) {
        IngresoMuestraService.ingresoNombreContenedorMasEnter(codigoContenedor);
    }

    @And("El usuario ingresa una solicitud lacrado con los parametros (.*), (.*), (.*), (.*), (.*)")
    public void elUsuarioIngresaUnaSolicitudLacradoConLosParametros(String codigoPuerto, String direccion, String nombreArchivo, String mensaje, String codigoContenedor) throws SQLException, IOException, InterruptedException, AWTException {
        ImportacionArchivoService.buscarInputMenu("Ingresar Muestras Solicitud de Lacrado");
        IngresoMuestraService.ingresarFechaDelDia();
        IngresoMuestraService.ingresarcodigoPuerto(codigoPuerto);
        IngresoMuestraService.ingresarNumeroDeMuestra(direccion, nombreArchivo);
        IngresoMuestraService.validarMensajeSist(mensaje);//revisar locator
        IngresoMuestraService.ingresoNombreContZorra(codigoContenedor);

//        IngresoMuestraService.clickEnElBotonAbrirNuevoContenedor();
//        IngresoMuestraService.ingresoNombreContenedor(codigoContenedor);
//        IngresoMuestraService.clickEnElBotonAbrirContenedor();
//        IngresoMuestraService.getModalCreacionContenedor();
    }

    @And("El usuario ingresa una solicitud automatica con los parametros (.*), (.*), (.*)")
    public void elUsuarioIngresaUnaSolicitudAutomatica(String codigoPuerto, String direccion, String nombreArchivo) throws InterruptedException, IOException {
        ImportacionArchivoService.buscarInputMenu("Ingresar Muestras de Solicitudes Automática");
        IngresoMuestraService.ingresarFechaDelDia();
        IngresoMuestraService.ingresarcodigoPuerto(codigoPuerto);
        IngresoMuestraService.ingresarNumeroDeMuestra(direccion, nombreArchivo);
        StepsInCommon.elUsuarioDecideNoAbrirElContenedor();
    }
}
