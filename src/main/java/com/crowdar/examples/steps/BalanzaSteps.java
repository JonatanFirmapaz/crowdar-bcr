package com.crowdar.examples.steps;

import com.crowdar.examples.services.BalanzaService;

import io.cucumber.java.en.When;

public class BalanzaSteps {

    @When("^Se Ingresan los siguientes componentes (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void seIngresanLosComponentesSiguientes (String codAnalista, String ArdyDanado, String GranosDanados, String MatExtranas, String QuebradosyChuzo, String PanzaBlanca, String Picados, String Pureza) {
        BalanzaService.inputNroPorcion();
        BalanzaService.inputCodAnalista(codAnalista);
        BalanzaService.inputGranosArdidos(ArdyDanado);
        BalanzaService.inputGranosDanadosSec(GranosDanados);
        BalanzaService.inputMateriaExtranas(MatExtranas);
        BalanzaService.inputQuebradosYchusos(QuebradosyChuzo);
        BalanzaService.inputGranosPanzaBlanca(PanzaBlanca);
        BalanzaService.inputGranosPicados(Picados);
        BalanzaService.inputPureza(Pureza);
        BalanzaService.botonAceptar();
        BalanzaService.botonCancelar();
    }

    @When("^Se Ingresan los siguientes Componentes (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void seIngresanLosSiguientesComponentes(String codAnalista, String GranosDanados, String MatExtranas, String QuebradosyChuzo, String Picados, String OtroTipo, String Pureza) {
        BalanzaService.inputNroPorcion();
        BalanzaService.inputCodAnalista(codAnalista);
        BalanzaService.inputGranosDanados(GranosDanados);
        BalanzaService.inputMateriaExtranas(MatExtranas);
        BalanzaService.inputQuebradosYchusos(QuebradosyChuzo);
        BalanzaService.inputGranosPicados(Picados);
        BalanzaService.inputOtroTipo(OtroTipo);
        BalanzaService.inputPureza(Pureza);
        BalanzaService.botonAceptar();
        BalanzaService.botonCancelar();
    }

    @When("^Se ingresan los siguientes componentes (.*), (.*), (.*),(.*),(.*)$")
    public void seIngresanLosSiguientesComponentes(String codAnalista, String chamico, String MatExtranas, String Pureza, String NroTarrito) {
        BalanzaService.inputNroPorcion();
        BalanzaService.inputCodAnalista(codAnalista);
        BalanzaService.inputMateriaExtranas(MatExtranas);
        BalanzaService.inputChamico(chamico);
        BalanzaService.inputPureza(Pureza);
        BalanzaService.inputTarrito(NroTarrito);
        BalanzaService.botonAceptar();
        BalanzaService.botonCancelar();
    }

    @When("Se Ingresan los siguientes datos (.*), (.*), (.*), (.*), (.*)")
    public void seIngresanLosSiguientesDatos(String codAnalista2, String pesoEnsayo, String pureza, String chamico, String tarrito) throws InterruptedException {
        BalanzaService.inputNroPorcion();
        BalanzaService.inputCodAnalista(codAnalista2);
        BalanzaService.inputPeseDelEnsayo(pesoEnsayo);
        BalanzaService.inputPureza(pureza);
        BalanzaService.inputChamico(chamico);
        BalanzaService.inputTarrito(tarrito);
        BalanzaService.botonAceptar();
        StepsInCommon.elUsuarioDecideNoAbrirElContenedor();
        BalanzaService.botonCancelar();
    }

    @When("Se ingresan los componentes siguientes (.*), (.*), (.*), (.*)")
    public void seIngresanLosSiguientesDatos(String codAnalista2, String pesoEnsayo, String pureza, String tarrito) throws InterruptedException {
        BalanzaService.inputNroPorcion();
        BalanzaService.inputCodAnalista(codAnalista2);
        BalanzaService.inputPeseDelEnsayo(pesoEnsayo);
        BalanzaService.inputPureza(pureza);
        BalanzaService.inputTarrito(tarrito);
        BalanzaService.botonAceptar();
        StepsInCommon.elUsuarioDecideNoAbrirElContenedor();
        BalanzaService.botonCancelar();
    }


}
