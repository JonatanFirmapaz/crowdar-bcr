package com.crowdar.examples.steps;

import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.ErrorDeTopeConstants;
import com.crowdar.examples.services.CorteService;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import io.cucumber.java.en.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SolicitudesPendientesDecisiónResultadoSteps {

    @Then("El usuario seleccina el sector '(.*)'")
    public void seleccionaSector(String name){
        ActionsManager.scrollWithoutReference("//*[@Name='Records']", String.format("(//*[@Name='%s'][@ClassName='Cell']/../*)[1]", name));
    }

    @And("El usuario hace click en la muestra")
    public void elUsuarioHaceClickEnLaMuestra() {
        try {
            Thread.sleep(100);
            WebElement butonFecha = DriverManager.getDriverInstance().findElement(By.xpath("//Text[@ClassName='TextBlock'][@Name='Fecha de Solicitud']"));
            butonFecha.click();
            butonFecha.click();
            ActionsManager.clickWithoutWait("//Custom[@ClassName='Cell'][contains(@Name,'%s')]", UserManager.getInstance().getNumeroSolicitud());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @And ("El usuario seleccina  el ensayo")
    public void elUsuarioSeleccionaElEnsayo() {
        try{
            Thread.sleep(200);
            DriverManager.getDriverInstance().findElement(By.xpath("//Custom[@ClassName='Cell'][@Name='Peso hectolítrico']")).click();
            DriverManager.getDriverInstance().findElement(By.xpath("//RadioButton[@Name='Re-Análisis 1'][@AutomationId='radioButton']")).click();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
