package com.crowdar.examples.steps;

import com.crowdar.examples.services.ActualizarPuestoDeTrabajoService;
import com.crowdar.examples.services.ImportacionArchivoService;
import com.crowdar.examples.services.IngresoMuestraService;
import com.crowdar.examples.services.LoginService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.io.IOException;
import java.sql.SQLException;

public class CambioDePuestoSteps {

    @Given("El usuario realiza el cambio de puesto de trabajo (.*) a (.*)")
    public void elUsuarioRealizaElCambioDePuestoDeTrabajoActualANuevo(String puestoActual, int puestoNuevo) throws SQLException, IOException {
        ActualizarPuestoDeTrabajoService.actualizarPuesto(puestoActual, puestoNuevo);
    }
}
