package com.crowdar.examples.steps;

import com.crowdar.examples.services.ErrorDeCurvaService;
import com.crowdar.examples.services.ServiceInCommon;
import io.cucumber.java.en.And;

public class ErrorDeCurvaSteps {
    @And("El usuario selecciona RMN con error de Curva")
    public void seleccionaRmnErrorCurva(){

        ErrorDeCurvaService.inputNroMuestra();
        ErrorDeCurvaService.clickFiltrar();
        ErrorDeCurvaService.selectNroMuestra();
        ServiceInCommon.clickButon("Aceptar Resultado");////Button[@ClassName=\"Button\"][@Name=\"Aceptar Resultado.\"]/Text[@ClassName=\"TextBlock\"][@Name=\"Aceptar Resultado.\"]"


    }
}
