package com.crowdar.examples.steps.AdministrarSolicitudes;

import com.crowdar.core.PageSteps;
import com.crowdar.examples.services.BusquedaAvanzadaService;
import io.cucumber.java.en.And;

public class BusquedaAvanzadaSteps extends PageSteps {

    @And("^El usuario completa '(.*)' de Busqueda Avanzada$")
    public void completarCamposBusquedaAvanzada(String behaviors) {
        BusquedaAvanzadaService.completeFieldsForBehaviors(behaviors);
    }

    @And("^El usuario hace click en '(.*)' en la pantalla de Busqueda Avanzada$")
    public void clickOn(String component) {
        BusquedaAvanzadaService.clickOn(component);
    }

    @And("^El usuario selecciona la celda que contiene la muestra$")
    public void elUsuarioSeleccionaLaCeldaQueContieneLaMuestra() {
        BusquedaAvanzadaService.selectCell();
    }
}
