package com.crowdar.examples.steps.AdministrarSolicitudes;

import com.crowdar.core.PageSteps;
import com.crowdar.examples.services.PreIngresoSolicitudService;

import com.crowdar.examples.utils.KeyboardManager;
import io.cucumber.java.en.And;
import org.apache.commons.lang.StringUtils;

public class PreIngresoSolicitudSteps extends PageSteps {

    @And("^El usuario hace click en '(.*)' en la pantalla Pre Ingresar Solicitud$")
    public void clickOn(String boton) {
        PreIngresoSolicitudService.clickOn(boton);
    }

    @And("^El usuario completa los campos de Pre Ingresar Solicitud con los siguientes datos: '(.*)', '(.*)', '(.*)', '(.*)'$")
    public void CompleteFieldsWith(String Solicitante, String matriz, String grupo, String peso) {
        PreIngresoSolicitudService.completeFieldsWith(Solicitante, matriz, grupo, peso);
    }

    @And("^El usuario completa el input '(.*)' con '(.*)'( y da enter)?$")
    public void completeField(String label, String text, String enter) {
        boolean etr = false;
        if (StringUtils.isNotBlank( enter ) ) etr = true;
        KeyboardManager.setDynamicInput(label, text, etr);
    }

}