package com.crowdar.examples.steps.AdministrarSolicitudes;

import com.crowdar.core.PageSteps;

import com.crowdar.examples.services.InformacionGeneralService;
import io.cucumber.java.en.And;

public class InformacionGeneralSteps extends PageSteps {

    @And("^El usuario completa los campos de Informacion General con los siguientes datos: '(.*)', '(.*)', '(.*)'$")
    public void elUsuarioCompletaLosCamposDeInformacionGeneral(String vendedor, String solicitante, String muestraDeclarada) {
        InformacionGeneralService.completeFieldsWith(vendedor, solicitante, muestraDeclarada);
    }

}
