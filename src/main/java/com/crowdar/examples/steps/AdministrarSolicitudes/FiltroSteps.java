package com.crowdar.examples.steps.AdministrarSolicitudes;

import com.crowdar.core.PageSteps;
import com.crowdar.core.actions.ActionManager;
import com.crowdar.examples.constants.AdministrarSolicitudes.AdministrarSolicitudesConstants;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.QueriesManager;
import com.crowdar.examples.utils.UserManager;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FiltroSteps extends PageSteps {
    @And("^El usuario ingresa el numero de solicitud$")
    public void elUsuarioIngresaElNumeroDeSolicitud() {
        String numeroSolicitud = QueriesManager.getInstance().consultarNumeroSolicitud(UserManager.getInstance().getMuestra());
        UserManager.getInstance().setNumeroSolicitud(numeroSolicitud);
        ActionsManager.clickWithoutWait(AdministrarSolicitudesConstants.NUMERO_SOLICITUD_INPUT);
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud(), false);
        ActionsManager.customSendKeys(200, 1, null, true); // FILTRAR
    }

    @When( "^El usuario ingresa el numero de solicitud ya obtenido$" )
    public void elUsuarioIngresaElNumeroDeSolicitudYaObtenido() {
        //UserManager.getInstance().getNumeroSolicitud()
        ActionsManager.customSendKeysWithTimeout(3000, UserManager.getInstance().getNumeroSolicitud(), false);
        ActionsManager.customSendKeys(100, 1, null, true); // tab enter
    }

    @When( "^El usuario ingresa el numero de solicitud ya obtenido y da enter$" )
    public void elUsuarioIngresaElNumeroDeSolicitudYaObtenidoYDaEnter() {
        //UserManager.getInstance().getNumeroSolicitud()
        ActionsManager.customSendKeysWithTimeout(3000, UserManager.getInstance().getNumeroSolicitud(), true);
    }

    @When( "^El usuario ingresa el numero de muestra ya obtenido y da enter$" )
    public void elUsuarioIngresaElNumeroDeMuestraYaObtenido() {
        ActionsManager.customSendKeysWithTimeout( 3000, UserManager.getInstance().getNumeroSolicitud()+"-M", true );
    }

    @Then("El usuario ingresa el numero de la solicitud a verificar")
    public void elUsuarioIngresaElNumeroDeLaSolicitudAVerificar() {
        String numeroSolicitud = QueriesManager.getInstance().consultarNumeroSolicitud(UserManager.getInstance().getMuestra());
        UserManager.getInstance().setNumeroSolicitud(numeroSolicitud);
        ActionsManager.clickWithoutWait(AdministrarSolicitudesConstants.INPUT_NUMERO_SOLICITUD);
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud(), true);
        ActionManager.waitVisibility(AdministrarSolicitudesConstants.VERIFICACION_PAGE);
    }
}
