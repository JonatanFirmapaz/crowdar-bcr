package com.crowdar.examples.steps.AdministrarSolicitudes;

import com.crowdar.core.PageSteps;
import com.crowdar.examples.services.AdministrarSolicitudesService;

import io.cucumber.java.en.And;

public class AdministrarSolicitudesSteps extends PageSteps {

    @And("^El usuario hace click en '(.*)' en la pantalla de Administrar Solicitudes$")
    public void preIngresoDeSolicitudClick(String tipo) {
        AdministrarSolicitudesService.clickOn(tipo);
    }

    @And("^El usuario filtra por '(.*)'$")
    public void elUsuarioFiltraPor(String opcion) {
        AdministrarSolicitudesService.filtrarPor(opcion);
    }
}
