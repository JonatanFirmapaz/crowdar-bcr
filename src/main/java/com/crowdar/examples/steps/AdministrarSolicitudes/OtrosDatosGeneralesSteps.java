package com.crowdar.examples.steps.AdministrarSolicitudes;

import com.crowdar.core.PageSteps;
import com.crowdar.examples.services.OtrosDatosGeneralesService;

import io.cucumber.java.en.And;

public class OtrosDatosGeneralesSteps extends PageSteps {

    @And("^El usuario completa los campos de Otros Datos Generales con los siguientes datos: '(.*)', '(.*)'$")
    public void elUsuarioCompletaLosDatosCon(String pagador, String formaPago) {
        OtrosDatosGeneralesService.completeFieldsWith(pagador, formaPago);
    }
}
