package com.crowdar.examples.steps.IngresarMuestraOContenedorAUnSector;

import com.crowdar.core.PageSteps;
import com.crowdar.core.actions.ActionManager;
import com.crowdar.examples.constants.GenericConstants;
import com.crowdar.examples.constants.IngresarContenedorOMuestraPorcionAUnSector.IngresarContenedorOMuestraPorcionAUnSectorConstants;
import com.crowdar.examples.constants.IngresoMuestraConstans;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;

import io.cucumber.java.en.When;

import org.apache.commons.lang.StringUtils;
import org.testng.Assert;

import java.util.HashMap;

public class IngresarMuestraOContenedorAUnSectorSteps extends PageSteps {
    @When("^El usuario ingresa el código de '(.*)' en la pantalla Ingresar Contenedor o Muestra/Porcion a un sector con los parametros '(.*)'$")
    public void elUsuarioIngresaElCodigoDe(String arg0, String optValue) {
        if (!StringUtils.isBlank(optValue))
            UserManager.getInstance().setContenedor(optValue);
        HashMap<String, Runnable> action = new HashMap<>();

        action.put("MUESTRA", this::ingresarCodigoMuestra);
        action.put("CONTENEDOR", this::ingresarCodigoContenedor);

        action.get(arg0.toUpperCase()).run();
    }

    public void ingresarCodigoMuestra() {
        ActionsManager.clickWithoutWait(IngresarContenedorOMuestraPorcionAUnSectorConstants.BOTON_AGREGAR);
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M", false);
        ActionsManager.clickWithoutWait(GenericConstants.BOTON_ACEPTAR);
    }

    public void ingresarCodigoContenedor() {
        Assert.assertTrue(ActionManager.waitVisibility(IngresoMuestraConstans.INPUT_CODIGO_CONTENEDOR).isDisplayed());
        ActionsManager.customSendKeys(UserManager.getInstance().getContenedor(), true);
    }
}
