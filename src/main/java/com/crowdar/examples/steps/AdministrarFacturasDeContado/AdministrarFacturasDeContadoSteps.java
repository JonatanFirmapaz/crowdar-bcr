package com.crowdar.examples.steps.AdministrarFacturasDeContado;

import com.crowdar.core.PageSteps;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.examples.constants.AdministrarFacturasDeContado.AdministrarFacturasDeContadoConstants;
import com.crowdar.examples.services.AdministrarFacturasDeContadoService;
import com.crowdar.examples.utils.ActionsManager;
import io.cucumber.java.en.And;

import java.util.Locale;

public class AdministrarFacturasDeContadoSteps extends PageSteps {

    @And( "^El usuario completa los campos de Administrar Facturas de Contado con el código de cliente '(.*)'$" )
    public void elUsuarioIngresa( String codigoCliente ) {
        AdministrarFacturasDeContadoService.completeFieldsWith( codigoCliente );
    }

    @And( "^El usuario ingresa el concepto '(.*)' e importe total '(.*)'$" )
    public void elUsuarioIngresa( String concepto, String importeTotal ) {
        AdministrarFacturasDeContadoService.completeFieldsWith( concepto, importeTotal );
    }

    @And( "^El usuario selecciona la forma de pago '(.*)'$" )
    public void elUsuarioSelecciona( String formaPago ) {
        AdministrarFacturasDeContadoService.completeFormaPago( formaPago );
    }

    @And( "^El usuario selecciona la celda de la solicitud$" )
    public void elUsuarioSeleccionaLaCeldaDeLaSolicitud() {
        AdministrarFacturasDeContadoService.checkSolicitud();
    }

    @And("El usuario selecciona checkbox de la solicitud")
    public void elUsuarioSeleccionaCheckboxDeLaSolicitud() {
        AdministrarFacturasDeContadoService.clickCheckboxNroMuestra();
    }
}
