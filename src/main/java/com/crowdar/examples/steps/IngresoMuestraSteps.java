package com.crowdar.examples.steps;

import com.crowdar.examples.services.IngresoMuestraService;
import com.crowdar.examples.services.LoginService;
import com.crowdar.examples.steps.IngresarMuestraOContenedorAUnSector.IngresarMuestraOContenedorAUnSectorSteps;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en.And;

import java.awt.*;
import java.io.IOException;
import java.util.Locale;
public class IngresoMuestraSteps {

    @When("Ingresa la fecha del dia")
    public void ingresaLaFechaDelDia() {
        IngresoMuestraService.ingresarFechaDelDia();
    }

    @When("Ingresa el codigo puerto (.*)")
    public void ingresaElCodigoPuerto(String codigoPuerto) {
        IngresoMuestraService.ingresarcodigoPuerto(codigoPuerto);
    }


    @When("Ingresa el numero de muestra (.*)")
    public void ingresaElNumeroDeMuestra(String direccion, String nombreArchivo) throws IOException {
        IngresoMuestraService.ingresarNumeroDeMuestra(direccion, nombreArchivo);
    }

    @When("Selecciona el boton '(.*)'")
    public void seleccionaElBoton(String nombreBoton) throws InterruptedException, AWTException {
        switch (nombreBoton.toUpperCase(Locale.ROOT)){
            case "REGISTRAR SOLICITUD": IngresoMuestraService.clickEnElBotonRegistrarSolicitud();
                break;
            case "ABRIR NUEVO CONTENEDOR": IngresoMuestraService.clickEnElBotonAbrirNuevoContenedor();
                break;
            case "ABRIR CONTENEDOR": IngresoMuestraService.clickEnElBotonAbrirContenedor();
                break;
            case "CERRAR CONTENEDOR": IngresoMuestraService.clickEnElBotonCerrarContenedor();
                break;
        }
    }

    @Then("Se valida la existencia del modal contenedores")
    public void seValidaLaExistenciaDelModalContenedores() {
        IngresoMuestraService.getModalContenedor();
    }

    @When("Ingresa el nombre del contenedor (.*)")
    public void ingresaElNombreDelContenedor(String codigoContenedor) {
        IngresoMuestraService.ingresoNombreContenedor(codigoContenedor);
    }

    @When("Cierra el contenedor (.*)")
    public void ingresaElNombreDelContenedorParaCerrarlo(String codigoContenedor) {
        IngresoMuestraService.ingresoNombreContenedorParaCerrarlo(codigoContenedor);
    }

    @When("Desactiva la opcion de imprimir datos de apertura")
    public void desactivaLaOpcionDeImprimirDatosDeApertura() {
        IngresoMuestraService.desactivarCheckBox();
    }

    @Then("Se valida la creacion del contenedor")
    public void seValidaLaCreacionDelContenedor() {
        IngresoMuestraService.getModalCreacionContenedor();
    }

    @Then("Se valida el cierre del contenedor")
    public void seValidaElCierreDelContenedor() {
        IngresoMuestraService.getModalCierreContenedor();
    }

    @Then("se cierra la aplicacion laboratorio")
    public void seCierraLaplicacionLaboratorio() {
        IngresoMuestraService.cierreAplicacionLaboratorio();
        LoginService.cambiarLenguaje();
        LoginService.cambiarLenguaje();
    }

    @Then("se valida el ingreso de la muestra")
    public void seValidaElIngresoDeLaMuestra() {
        //TODO realizar validacion del ingreso de la muestra
    }

    @When("El usuario ingresa el codigo de la muestra")
    public void elUsuarioIngresaElCodigoDeLaMuestra() {
        IngresoMuestraService.ingresarCodigoMuestra();
    }

    @When("El usuario ingresa el codigo de la porcion al sector")
    public void elUsuarioIngresaElCodigoDeLaPorcionAlSector() {
        IngresoMuestraService.ingresarCodigoMuestraPorcion();
    }
}
