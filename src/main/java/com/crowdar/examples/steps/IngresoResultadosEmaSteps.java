package com.crowdar.examples.steps;

import com.crowdar.examples.services.ImportacionArchivoService;
import com.crowdar.examples.services.IngresoMuestraService;
import com.crowdar.examples.services.IngresoResultadosEmaService;

import io.cucumber.java.en.And;

public class IngresoResultadosEmaSteps {

    @And("Se valida el ingreso al sector de EMA con los siguientes datos (.*), (.*), (.*), (.*), (.*), (.*)")
    public void seValidaElIngresoAlSectorDeEMAConLosSiguientesDatos(String codAnalista, String PH, String HEA, String CP, String codEqipo, String resultado) {
        ImportacionArchivoService.buscarInputMenu("Ingresar Resultados EMA");
        IngresoResultadosEmaService.ingresarMuestra();
        IngresoResultadosEmaService.verificarEma();
        IngresoResultadosEmaService.inputCodigoAnalista(codAnalista);
        IngresoResultadosEmaService.inputPH(PH);
        IngresoResultadosEmaService.inputHEA(HEA);
        IngresoResultadosEmaService.inputCodigoEquipo(codEqipo);
        IngresoResultadosEmaService.inputAnalistaCP(codAnalista);
        IngresoResultadosEmaService.inputCP(CP);
        IngresoResultadosEmaService.inputCodigoEquipoCP(codEqipo);
        IngresoResultadosEmaService.botonAceptar();
        IngresoResultadosEmaService.botonNoAbrirContenedor();
        IngresoResultadosEmaService.validarResultados(resultado);
        IngresoResultadosEmaService.botonCancelar();
    }

    @And( "^Se ingresan los siguientes datos al sector EMA: '(.*)', '(.*)', '(.*)', '(.*)', '(.*)'$" )
    public void seValidaElIngresoAlSectorEMAConLosSiguientesDatos(String codAnalista, String PH, String HEA, String CP, String codEqipo) {
        IngresoResultadosEmaService.verificarEma();
        IngresoResultadosEmaService.inputCodigoAnalista(codAnalista);
        IngresoResultadosEmaService.inputPH(PH);
        IngresoResultadosEmaService.inputHEA(HEA);
        IngresoResultadosEmaService.inputCodigoEquipo(codEqipo);
        IngresoResultadosEmaService.inputAnalistaCP(codAnalista);
        IngresoResultadosEmaService.inputCP(CP);
        IngresoResultadosEmaService.inputCodigoEquipoCP(codEqipo);
        IngresoResultadosEmaService.botonAceptar();
    }

    @And( "^Se ingresa la muestra en EMA$" )
    public void seIngresaLaMuestraEnEma() {
        IngresoResultadosEmaService.ingresarMuestra();
    }

    @And("^Se ingresan los datos (.*), (.*), (.*), (.*), (.*), (.*) en el sector EMA$")
    public void seIngresanLosDatosEnElSectorEMA(String codAnalista, String PH, String HEA, String CP, String codEqipo, String resultado) {
        ImportacionArchivoService.buscarInputMenu("Ingresar Resultados EMA");
        IngresoResultadosEmaService.ingresarMuestra();
        IngresoResultadosEmaService.verificarEma();
        IngresoResultadosEmaService.inputCodigoAnalista(codAnalista);
        IngresoResultadosEmaService.inputPH(PH);
        IngresoResultadosEmaService.inputHEA(HEA);
        IngresoResultadosEmaService.inputCodigoEquipo(codEqipo);
        IngresoResultadosEmaService.botonAceptar();
        IngresoResultadosEmaService.botonNoAbrirContenedor();
        IngresoResultadosEmaService.validarResultados(resultado);
        IngresoResultadosEmaService.botonCancelar();
    }

    @And("^Se ingresan los datos (.*), (.*), (.*), (.*), (.*), (.*) en EMA$")
    public void seIngresanLosDatosEnEma(String codAnalista, String PH, String HEA, String CP, String codEqipo, String resultado) {
        ImportacionArchivoService.buscarInputMenu("Ingresar Resultados EMA");
        IngresoResultadosEmaService.ingresarMuestra();
        IngresoResultadosEmaService.verificarEma();
        IngresoResultadosEmaService.inputCodigoAnalista(codAnalista);
        IngresoResultadosEmaService.inputPH(PH);
        IngresoResultadosEmaService.inputHEA(HEA);
        IngresoResultadosEmaService.inputCodigoEquipo(codEqipo);
        IngresoResultadosEmaService.botonAceptar();
    }

    @And("^El usuario abre el contenedor: (.*)$")
    public void elUsuarioAbreElcontenedor(String codigoContenedor) {
        IngresoMuestraService.clickEnElBotonAbrirNuevoContenedor();
        IngresoMuestraService.ingresoNombreContenedor(codigoContenedor);
        IngresoMuestraService.clickEnElBotonAbrirContenedor();
        IngresoMuestraService.getModalCreacionContenedor();
    }

    @And("^El usuario valida los resultados ingresados en EMA$")
    public void elUsuarioValidaLosResultadosIngresadosEnEMA() {
        IngresoResultadosEmaService.validarResultados("Los resultados se encuentran correctos.");
        IngresoResultadosEmaService.botonCancelar();
    }

    @And("^El usuario cierra el contenedor: (.*)$")
    public void elUsuarioCierraElcontenedor(String codigoContenedor) {
        ImportacionArchivoService.buscarInputMenu("Cerrar Contenedor");
        IngresoMuestraService.ingresoNombreContenedorParaCerrarlo(codigoContenedor);
        IngresoMuestraService.clickEnElBotonCerrarContenedor();
        IngresoMuestraService.getModalCierreContenedor();
    }

    @And("El usuario revisa el ingreso y registra la solicitud a un sector con los parametros (.*)")
    public void elUsuarioRevisaElIngresoYRegistraLaSolicitudAUnSector(String codigoContenedor) {
        ImportacionArchivoService.buscarInputMenu("Ingresar Contenedor o Muestra/Porción a un sector");
        IngresoMuestraService.ingresoNombreContenedorMasEnter(codigoContenedor);
    }

    @And("Se confirma el ingreso al sector de EMA con los siguientes datos (.*), (.*), (.*), (.*), (.*)")
    public void seConfirmaElIngresoAlSectorDeEMAConLosSiguientesDatos(String codAnalista, String PH, String HEA, String codEquipo, String resultado) {
        ImportacionArchivoService.buscarInputMenu("Ingresar Resultados EMA");
        IngresoResultadosEmaService.ingresarMuestra();
        IngresoResultadosEmaService.verificarEma();
        IngresoResultadosEmaService.inputCodigoAnalista(codAnalista);
        IngresoResultadosEmaService.inputPH(PH);
        IngresoResultadosEmaService.inputHEA(HEA);
        IngresoResultadosEmaService.inputCodigoEquipo(codEquipo);
        IngresoResultadosEmaService.botonAceptar();
        IngresoResultadosEmaService.botonNoAbrirContenedor();
        IngresoResultadosEmaService.validarResultados(resultado);
        IngresoResultadosEmaService.botonCancelar();
    }

}