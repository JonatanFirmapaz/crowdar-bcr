package com.crowdar.examples.steps;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.AdministrarSolicitudes.AdministrarSolicitudesConstants;
import com.crowdar.examples.constants.GenericConstants;
import com.crowdar.examples.constants.IngresoResultadosEmaConstans;
import com.crowdar.examples.services.IngresoResultadosEmaService;
import com.crowdar.examples.services.LoginService;
import com.crowdar.examples.services.ServiceInCommon;
import com.crowdar.examples.utils.ActionsManager;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.awt.*;
import java.awt.event.KeyEvent;

import java.io.IOException;

import java.util.HashMap;

public class StepsInCommon {

    public static void elUsuarioVisualizaElBotonNoAbrirContenedor() {
        if(ActionsManager.theFollowingButtonIsPresent(IngresoResultadosEmaConstans.BOTON_NO_ABRIR_CONTENEDOR)) {
            ActionManager.click(IngresoResultadosEmaConstans.BOTON_NO_ABRIR_CONTENEDOR);
            ActionsManager.pressKey( KeyEvent.VK_ENTER );
        }
    }

    public static void elUsuarioVisualizaElBotonCancelar() {
        if(ActionsManager.theFollowingButtonIsPresent(IngresoResultadosEmaConstans.BOTON_CANCELAR)) {
            ActionManager.click(IngresoResultadosEmaConstans.BOTON_CANCELAR);
        }
    }

    public static void closePopUps(String... xpaths) {
        boolean componentsExists = false;
        int maxAttemps = 10;
        for (int i = 0; i < maxAttemps; i++) {
            for (String xpath : xpaths) {
                if (isComponentVisible(xpath)) {
                    componentsExists = true;
                    ActionManager.click(xpath);
                    break;
                }
            }
            if (!componentsExists) break;
        }
    }

    public static boolean isComponentVisible(String xpath) {
        try {
            if (DriverManager.getDriverInstance().findElement(By.xpath(xpath)).isDisplayed()){
                return true;
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void elUsuarioDecideNoAbrirElContenedor() throws InterruptedException {
        Thread.sleep(5000);
        if (isComponentVisible("//*[@AutomationId='noRealizarRadioButtonControl2']/*[@Name='No abrir contenedor']")) {
            ActionManager.click(IngresoResultadosEmaConstans.BOTON_NO_ABRIR_CONTENEDOR);
            ActionsManager.pressKey(KeyEvent.VK_ENTER, 500);
        }
    }

    @When("La pantalla de Login aparece")
    public void openLogin() {
        LoginService.openLogin();
    }

    @Then("El usuario ingresa a la aplicacion laboratorio")
    public void openLaboratorio() {
        LoginService.openLaboratorio();
        // ImportacionArchivoService.close();
    }

    @Then("Se reinicia el aplicativo y se inicia sesion con las credenciales (.*), (.*)")
    public void seReiniciaElAplicativoYSeIniciaSesionConLasCredencialesUsuarioPassword(String userName, String password) {
        try {
            Runtime.getRuntime().exec( "taskkill /F /IM BCR.UI.Launcher.exe" );
            Runtime.getRuntime().exec( "taskkill /F /IM BCR.Lab.UI.Desktop.Shell.exe" );
        } catch ( IOException e ) {
            e.printStackTrace();
        } finally {
            LoginService.reinicioLab();
            LoginService.openLogin();
            LoginService.setUsername(userName);
            LoginService.setPassword(password);
            LoginService.login();
            LoginService.goToLaboratorio();
            LoginService.openLaboratorio();
        }
    }

    @Then("^El usuario visualiza el popup de exito$")
    public static void elUsuarioVisualizaElPopUpDeExito() {
        ActionsManager.checkAlertInformation("Se han guardado los cambios");
        if (isComponentVisible(String.format("//*[contains(@Name, '%s')]", "El contenedor asignado es"))) {
            ActionsManager.pressKey(KeyEvent.VK_ENTER, 500);
        }
    }

    @And("^El usuario guarda los cambios$")
    public void guardarCambios() {
        ServiceInCommon.guardarDatos();
    }

    @And("^El usuario decide no abrir el contenedor$")
    public void noAbrirContenedor() {
        ServiceInCommon.cerrarContenedor();
    }

    @And( "^El usuario hace click en '(.*)'$" )
    public void elUsuarioHaceClickEn( String button ) {
        HashMap<String, String> buttons = new HashMap<>();

        buttons.put( "AGREGAR", GenericConstants.BOTON_AGREGAR );
        buttons.put( "_AGREGAR", GenericConstants.BOTON__AGREGAR );
        buttons.put( "ACEPTAR", GenericConstants.BOTON_ACEPTAR );

        ActionsManager.clickWithoutWait( buttons.get( button.toUpperCase() ) );
    }

    @And( "^El usuario hace click en el boton '(.*)'$" )
    public void elUsuarioHaceClickEnElBoton( String name ) {
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "//*[@Name='%s'][@ClassName='Button']", name ) ) ).click();
    }

    @And( "^El usuario hace click en el boton '(.*)' y da enter$" )
    public void elUsuarioHaceClickEnElBotonYdaEnter( String name ) {
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "//*[@Name='%s'][@ClassName='Button']", name ) ) ).click();
        ActionsManager.pressKey( KeyEvent.VK_ENTER );
    }

    @Then( "^El usuario verifica el popup '(.*)'$" )
    public void elUsuarioVerificaElPopup( String resultText ) {
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "//*[@ClassName='TextBlock'][contains(@Name, '%s')]", resultText ) ) );
        ActionsManager.pressKey( KeyEvent.VK_ENTER );
    }

    @And( "^El usuario selecciona el checkbox de '(.*)'$" )
    public void elUsuarioSelecionaLaOpcion( String option ) {
        try {
            Thread.sleep(5000);
            scrollWithoutReference( "//*[@Name='Records']", String.format( "(//*[@Name='%s'][@ClassName='Cell']/../*)[1]/*", option ) );
        } catch (Exception e) {
            e.printStackTrace();
        }

        //DriverManager.getDriverInstance().findElement( By.xpath( String.format( "(//*[@Name='%s'][@ClassName='Cell']/../*)[1]", option) ) ).click();
    }

    public void scrollWithoutReference( String containerLocator, String target ) {
        try {
            Robot r = new Robot();
            WebElement element = DriverManager.getDriverInstance().findElement( By.xpath( containerLocator ) );
            int xPos = element.getLocation().x;
            int yPos = element.getLocation().y;
            int xSiz = element.getSize().width;
            int ySiz = element.getSize().height;
            int xOrig = xPos + ( xSiz / 2 );
            int yOrig = yPos + ( ySiz / 2 );
            r.mouseMove( xOrig, yOrig );
            while ( !ActionsManager.isVisibilityNotRunTimeException( target, "" ) ) {
                r.mouseWheel( 50 );
            }
            DriverManager.getDriverInstance().findElement( By.xpath( target ) ).click();
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    @And( "^El usuario regresa a la pantalla '(.*)'$" )
    public void elUsuarioRegresaAlaPantalla( String pantalla ) {
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "//*[@ClassName='TextBlock'][@Name='%s']", pantalla ) ) ).click();
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "//*[@ClassName='TextBlock'][@Name='%s']", pantalla ) ) ).click();
        ActionsManager.pressKey( KeyEvent.VK_ENTER );
    }


    @And("^El usuario selecciona la casilla de '(.*)'$")
    public void elUsuarioSeleccionaLaCasillaDeTodos(String name) {
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "//*[@ClassName='TextBlock'][@Name='%s']", name ) ) ).click();
    }

    @And("^El usuario preciona el boton '(.*)'$")
    public void elUsuarioPrecionaElBotonSi(String name) {
        DriverManager.getDriverInstance().findElement(By.xpath(String.format( "//*[@Name='%s'][@AutomationId='OKButton']", name) ) ).click();
    }

    @And("^El usuario selecciona la opcion de '(.*)'$")
    public void elUsuarioSeleccionaLaOpcionDeAceptarResultado(String name) {
        DriverManager.getDriverInstance().findElement(By.xpath(String.format( "//*[@Name='%s'][@AutomationId='saveButton']", name) ) ).click();
    }

    @Then("^El usuario selecciona el boton Aceptar$")
    public void elUsuarioSeleccionaElBotonAceptar() {
        ActionManager.click(AdministrarSolicitudesConstants.BUTTON_ACEPTAR);
    }
}
