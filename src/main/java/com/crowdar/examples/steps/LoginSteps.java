package com.crowdar.examples.steps;

import com.crowdar.core.PageSteps;
import com.crowdar.examples.services.FechaYestadoSolicutudService;
import com.crowdar.examples.services.LoginService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.io.IOException;

public class LoginSteps extends PageSteps {

    @When("Ingresa el usuario (.*)")
    public void setUsername(String username) {
        LoginService.setUsername(username);
    }

    @When("Ingresa la contraseña (.*)")
    public void setPassword(String password) {
        LoginService.setPassword(password);
    }

    @When("Clickea en la pantalla de inicio el boton '(.*)'")
    public void clickeaEnLaPantallaDeInicioElBoton(String nombreBoton) {
        switch (nombreBoton){
            case "Login":
                LoginService.login();
                break;
            case "Laboratorio":
                LoginService.goToLaboratorio();
                break;
        }
    }

    @Given("El usuario inicia sesion con sus credenciales (.*), (.*)")
    public void elUsuarioIniciaSesionConSusCredenciales(String username, String password) {
        LoginService.cambiarLenguaje();
        LoginService.openLogin();
        LoginService.setUsername(username);
        LoginService.setPassword(password);
        LoginService.login();
        LoginService.goToLaboratorio();
        LoginService.openLaboratorio();
    }

    @When("El boton de Laboratorio aparece")
    public void verificarLaboratorio() {
        Assert.assertTrue(LoginService.verifyLaboratorio(), "El boton Laboratorio no aparecio correctamente");
    }

    @And("se reinicia la aplicacion laboratorio")
    public void seReiniciaLaAplicacionLaboratorio() {
        LoginService.reinicioLab();
    }

    @Then("Se verifica que sea correcto el ingreso")
    public void seVerificaQueSeaCorrectoElIngreso() {
        LoginService.loginCorrecto();
        LoginService.cambiarLenguaje();
    }

    @Then("Se verifica que el usuario y password son incorectas")
    public void seVerificaQueElUsuarioYPasswordSonIncorectas() {
        LoginService.loginFallido();
        LoginService.cambiarLenguaje();
    }

    @Then("Se verifica la falta de credenciales")
    public void seVerificaLaFaltaDeCredenciales() {
        LoginService.usuarioObligatorio();
        LoginService.cambiarLenguaje();
    }
}
