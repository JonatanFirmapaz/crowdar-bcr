package com.crowdar.examples.steps;

import com.crowdar.examples.constants.IngresoResultadosEmaConstans;
import com.crowdar.examples.services.CorteService;
import com.crowdar.examples.services.ImportacionArchivoService;
import com.crowdar.examples.services.IngresoMuestraService;
import com.crowdar.examples.services.IngresoResultadosEmaService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;

import java.awt.*;

public class CorteSteps {

    @And("Se ingresa la porcion al sector (.*), (.*)")
    public void seIngresaLaPorcionAlSector(String ContGeneral, String ContPorciones) {
        IngresoMuestraService.ingresoNombreContenedorMasEnter(ContGeneral);
        ImportacionArchivoService.buscarInputMenu("Leer Muestra/Porción para Corte");
        CorteService.ingresarMuestraCorte();
        CorteService.clickConfirmarPorciones();
        IngresoMuestraService.clickEnElBotonAbrirNuevoContenedor();
        IngresoMuestraService.ingresoNombreContenedor(ContPorciones);
        IngresoMuestraService.clickEnElBotonAbrirContenedor();
        IngresoResultadosEmaService.botonNoAbrirContenedor();
    }

    @And("Se Validan la muestras ingresadas (.*)")
    public void seValidanLaMuestrasIngresadas(String ContPorciones) {
        CorteService.validarPorcion();
        CorteService.clickCancelar();
        ImportacionArchivoService.buscarInputMenu("Cerrar Contenedor");
        IngresoMuestraService.ingresoNombreContenedorParaCerrarlo(ContPorciones);
        IngresoMuestraService.clickEnElBotonCerrarContenedor();
        IngresoMuestraService.getModalCierreContenedor();
    }

    @When("^Se ingresa la porcion a Corte$")
    public void seIngresaLaPorcionAcorte() {
        ImportacionArchivoService.buscarInputMenu("Leer Muestra/Porción para Corte");
        CorteService.ingresarMuestraCorte();
        CorteService.clickConfirmarPorciones();
    }

    @When("^Se abre el contenedor: '(.*)'$")
    public void seAbreElContenedor(String contenedor) throws InterruptedException {
        IngresoMuestraService.clickEnElBotonAbrirNuevoContenedor();
        IngresoMuestraService.ingresoNombreContenedor(contenedor);
        IngresoMuestraService.clickEnElBotonAbrirContenedor();
        StepsInCommon.elUsuarioVisualizaElPopUpDeExito();
        StepsInCommon.closePopUps(IngresoResultadosEmaConstans.BOTON_CANCELAR);
        StepsInCommon.elUsuarioDecideNoAbrirElContenedor();
    }
}