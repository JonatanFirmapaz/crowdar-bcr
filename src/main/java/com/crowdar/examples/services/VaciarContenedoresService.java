package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.VaciarContenedoresConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class VaciarContenedoresService {

    public static void inputCodContenedor(String codContenedor){
        ActionManager.setInput(VaciarContenedoresConstants.INPUT_CODIGO_CONTENEDOR, codContenedor, true);
         ServiceInCommon.pressKey(Keys.ENTER, ActionManager.getElement(VaciarContenedoresConstants.INPUT_CODIGO_CONTENEDOR));
    }

    public static void botonAceptar(){
        WebElement aceptar = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Aceptar'][@AutomationId='OKButton']"));
        aceptar.click();
    }
}
