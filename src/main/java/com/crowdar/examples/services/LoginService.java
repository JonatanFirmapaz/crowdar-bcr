package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.examples.constants.LoginConstants;
import com.crowdar.examples.drivers.DesktopDriver;
import com.crowdar.examples.drivers.LaboratorioDriver;
import com.crowdar.examples.drivers.LauncherDriver;

import com.crowdar.examples.utils.UserManager;
import org.testng.Assert;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.im.InputContext;
import java.util.Locale;

public class LoginService {

    public static void cambiarLenguaje(){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                InputContext context = InputContext.getInstance();
                if (!context.getLocale().getLanguage().equals(Locale.ENGLISH.getLanguage())) {
                    try {
                        execute();
                    } catch (AWTException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private static void execute() throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_SHIFT);
        robot.delay(10);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_SHIFT);
    }

    public static void openLogin(){
        LauncherDriver.open();
    }

    public static void setUsername(String username) {
        ActionManager.setInput(LoginConstants.USERNAME_INPUT, UserManager.getInstance().getUsername(), true, false);
    }

    public static void setPassword(String password){
        ActionManager.setInput(LoginConstants.PASSWORD_INPUT, password,true);
    }

    public static void login(){
        ActionManager.click(LoginConstants.LOGIN_BUTTON);
    }

    public static boolean verifyLaboratorio(){
        return ActionManager.isPresent(LoginConstants.LABORATORIO_BUTTON);
    }

    public static boolean verifyUsuario(){
        return ActionManager.isPresent(LoginConstants.USUARIO_INCORRECTO);
    }

    public static void goToLaboratorio(){
        if(verifyLaboratorio()){
            ActionManager.waitVisibility(LoginConstants.LABORATORIO_BUTTON);
            ActionManager.waitClickable(LoginConstants.LABORATORIO_BUTTON);
            ActionManager.click(LoginConstants.LABORATORIO_BUTTON);
            System.out.println("El Login fue Exitoso.");
        }else if (verifyUsuario()){
            System.out.println("El usuario/contraseña son incorrectos.");
        }else{
            System.out.println("El campo Usuario y password son obligatorios.");
        }
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void openLaboratorio(){
        LauncherDriver.close();
        //Inicializo Driver Desktop (Para obtener appTopLevelWindow)
        DesktopDriver.open();
        //Obtengo el appTopLevelWindow de laboratorio
        String laboratorioApp = DesktopDriver.getLaboratorioWindow();

        //Cierro Driver de Desktop
        DesktopDriver.close();

        //Inicializo Driver Laboratorio
        LaboratorioDriver.open(laboratorioApp);
    }


    public static void reinicioLab(){
        LaboratorioDriver.close();
        LauncherDriver.open();
    }

    public static void loginCorrecto(){
        ActionManager.waitVisibility(LoginConstants.LABORATORIO_BUTTON);
        ActionManager.isPresent(LoginConstants.LABORATORIO_BUTTON);
        System.out.println("El Login fue exitoso");
    }

    public static void loginFallido(){
        ActionManager.isPresent(LoginConstants.USUARIO_INCORRECTO);
        String incorrecto = ActionManager.getText(LoginConstants.USUARIO_INCORRECTO);
        Assert.assertEquals(incorrecto, "El usuario o la clave ingresada es incorrecta.");
    }

    public static void usuarioObligatorio(){
        ActionManager.waitVisibility(LoginConstants.USUARIO_OBLIGATORIO);
        ActionManager.waitVisibility(LoginConstants.PASS_OBLIGATORIA);
        ActionManager.isPresent(LoginConstants.USUARIO_OBLIGATORIO);
        ActionManager.isPresent(LoginConstants.PASS_OBLIGATORIA);
        String usuario = ActionManager.getText(LoginConstants.USUARIO_OBLIGATORIO);
        String pass = ActionManager.getText(LoginConstants.PASS_OBLIGATORIA);
        Assert.assertEquals(usuario, "El campo Usuario es obligatorio.");
        Assert.assertEquals(pass, "El campo Password es obligatorio.");
    }
}