package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.LabFisicoConstants;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.openqa.selenium.Keys.ENTER;

public class LabFisicoService {

    public static void asignarAnalista(String codAnalista){
        ActionManager.click(LabFisicoConstants.CLICK_ANALISTA);
        ActionManager.setInput(LabFisicoConstants.ASIGNAR_ANALISTA, codAnalista, true);
        ServiceInCommon.pressKey(ENTER, ActionManager.getElement(LabFisicoConstants.ASIGNAR_ANALISTA));
    }

    public static void asignarMesada(String codMesada){
        ActionManager.click(LabFisicoConstants.CLICK_MESADA);
        ActionManager.setInput(LabFisicoConstants.ASIGNAR_MESADA, codMesada, true);
         ServiceInCommon.pressKey(ENTER, ActionManager.getElement(LabFisicoConstants.ASIGNAR_MESADA));
    }

    public static void asignarContenedor(String codContenedor){
        //Se realiza de esta forma ya que lippia no toma por valido el XPATH.
        //TODO:validar esto JAQA
        WebElement contenedor = DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='codigoMuestraText']"));
        contenedor.click();
        contenedor.sendKeys(codContenedor);
        contenedor.sendKeys(ENTER);

    }
}
