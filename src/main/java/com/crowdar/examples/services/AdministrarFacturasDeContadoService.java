package com.crowdar.examples.services;

import com.crowdar.core.LocatorManager;
import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.AdministrarFacturasDeContado.AdministrarFacturasDeContadoConstants;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.crowdar.examples.utils.ActionsManager.*;

public class AdministrarFacturasDeContadoService {

    public static void completeFieldsWith( String codigoCliente ) {
        ActionsManager.customSendKeysWithTimeout( 3000, codigoCliente, true );
    }

    public static void completeFieldsWith(String  concepto, String importeTotal ) {
        ActionManager.setInput( AdministrarFacturasDeContadoConstants.CONCEPTO_INPUT, concepto );
        ActionsManager.customSendKeys( 200, 1, importeTotal, true );
    }

    public static void checkSolicitud() {
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "(//*[@Name='%s']/..//*)[1]", UserManager.getInstance().getNumeroSolicitud()) ) ).click();
    }

    public static void completeFormaPago( String formaPago ) {
        ActionManager.click( AdministrarFacturasDeContadoConstants.FORMA_PAGO_INPUT );
        customSendKeysWithTimeout( 2000, formaPago, true );
    }

    public static void clickCheckboxNroMuestra(){
        List<WebElement> elements = DriverManager.getDriverInstance().findElements( By.xpath( String.format( "//*[@ClassName='Cell'][contains(@Name,'%s')]/../*[1]", UserManager.getInstance().getNumeroSolicitud() ) ) );
        elements.forEach( WebElement::click );
    }
}
