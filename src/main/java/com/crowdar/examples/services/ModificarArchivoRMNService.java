package com.crowdar.examples.services;

import com.crowdar.examples.utils.QueriesManager;
import com.crowdar.examples.utils.UserManager;
import org.joda.time.LocalTime;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class ModificarArchivoRMNService {

    public static void ArchivoDat(String direccion, String nombreArchivo) throws IOException{
        int i = 0;
        String path = System.getProperty("user.dir").concat(direccion).concat(nombreArchivo).concat(".dat");
        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String numeroSolicitud = QueriesManager.getInstance().consultarNumeroSolicitud(UserManager.getInstance().getMuestra());
        UserManager.getInstance().setNumeroSolicitud(numeroSolicitud);
        String numeroMuestra = UserManager.getInstance().getNumeroSolicitud();
        List<Object> listadoLineasRMN;
        try {
            listadoLineasRMN = Arrays.asList(bufferedReader.lines().toArray());
            StringBuilder nuevasLineas = new StringBuilder();
            nuevasLineas.append(listadoLineasRMN.get(0)).append("\r\n");
            nuevasLineas.append(listadoLineasRMN.get(1)).append("\r\n");
            for (i=2; i<4; i++){
                String viejoNumero = listadoLineasRMN.get(i).toString().substring(18, 26);
                System.out.println(viejoNumero);
                String lineaModificada = listadoLineasRMN.get(i).toString().replace(viejoNumero, numeroMuestra);
                nuevasLineas.append(lineaModificada).append("\r\n");
            }

            FileWriter fileWriter = new FileWriter(path, false);
            fileWriter.write(String.valueOf(nuevasLineas));

            fileWriter.close();
            fileReader.close();
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ArchivoMdt(String direccion, String nombreArchivo) throws IOException{
        String path = System.getProperty("user.dir").concat(direccion).concat(nombreArchivo).concat(".mdt");
        FileWriter fileWriter = new FileWriter(path, false);
        String numeroSolicitud = QueriesManager.getInstance().consultarNumeroSolicitud(UserManager.getInstance().getMuestra());
        UserManager.getInstance().setNumeroSolicitud(numeroSolicitud);
        String numeroMuestra = UserManager.getInstance().getNumeroSolicitud()+"-m"+"-p1";
        String fecha = "Fecha: ";
        String fechaMuestra = "29.11.2020";
        String fechaDelDia = new SimpleDateFormat("dd.MM.yyyy").format(new java.util.Date());
        String hora = new SimpleDateFormat("HH:mm:ss\n").format(new java.util.Date());
        String curDateTime = fechaMuestra + " " + hora;
        String currentDateTime = fechaDelDia + " " + hora;
        String texto1= "------------------------\n\n";
        String texto2= "Moisture & Oil Analyses";
        String texto3= "------------------------\n";
        String texto4= "Moisture/Oil Calibration\n" +
                "Mo_Slope      :  0.16950\n" +
                "Mo_Intercept  :  0.42954\n" +
                "Mo_Correlation:  0.94819\n" +
                "Mo_StdDeviat  :  0.06450\n" +
                "O_Slope       :  0.12605\n" +
                "O_Intercept   :  0.09816\n" +
                "O_Correlation :  0.99549\n" +
                "O_StdDeviat   :  0.05088\n";
        String texto5= "\n\n\n\nSampleno: 26\n" +
                "SampleId: "+numeroMuestra+"_26\n" +
                "Weight/g: 11.82\n" +
                "Moist./%: 4.75\n" +
                "   Oil/%: 53.05\n\n\n";
        String texto6= "Sampleno: 27\n" +
                "SampleId: "+numeroMuestra+"_27\n" +
                "Weight/g: 11.86\n" +
                "Moist./%: 4.69\n" +
                "   Oil/%: 52.05\n\n\n";
        String texto7= "Sampleno: 28\n" +
                "SampleId: "+numeroMuestra+"_28\n" +
                "Weight/g: 11.61\n" +
                "Moist./%: 4.62\n" +
                "   Oil/%: 53.04\n";

        try {
            if (fechaMuestra.equals(fechaDelDia)){
                fileWriter.write(texto1 + texto2 + curDateTime+ texto3 + texto1 +texto3+texto4+texto3+texto5+texto6+texto7);
            }
            else {
                fileWriter.write(texto1 + texto2 + currentDateTime+ texto3 + texto1 +texto3+texto4+texto3+texto5+texto6+texto7);
            }

            fileWriter.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
