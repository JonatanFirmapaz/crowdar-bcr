package com.crowdar.examples.services;

import com.crowdar.driver.DriverManager;

import com.crowdar.examples.constants.ErrorDeTope.ErrorDeTopeConstants;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ErrorDeTopeService {

    public static void selectSector (String option){
        try {
            Thread.sleep(5000);
            ActionsManager.scrollWithoutReference(ErrorDeTopeConstants.CONTAINER_LOC,  String.format(ErrorDeTopeConstants.CHECKBOX_SECTOR, option ));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void clickTodos(){
        try {
            DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.CHECK_BOX_TODOS)).click();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void clickFiltrar(){

        DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.BUTON_FILTRAR)).click();

    }

    public static void clickFecha(){

        WebElement butonFecha = DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.BUTON_FECHA));
        butonFecha.click();
        butonFecha.click();

    }

    public static void clickNroMuestra(){
        try {
            Thread.sleep(100);
            WebElement butonFecha = DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.BUTON_FECHA));
            butonFecha.click();
            butonFecha.click();

            List<WebElement> elements = DriverManager.getDriverInstance().findElements( By.xpath( String.format( "//*[@ClassName='Cell'][contains(@Name,'%s')]/../*[1]", UserManager.getInstance().getNumeroSolicitud() ) ) );
            elements.forEach( WebElement::click );

            if (DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.PAGE_UP)).isDisplayed()) {
                DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.PAGE_UP)).click();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void seleccionaRegistro(String ensayo){

        try {
            Thread.sleep(100);

            //ActionsManager.waitForElement(ErrorDeTopeConstants.NRO_MUESTRA, UserManager.getInstance().getNumeroSolicitud());
            //ActionsManager.clickWithoutWait(ErrorDeTopeConstants.CHECKBOX_ENSAYO, ensayo);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void clickAccion(String button){
            if ( ActionsManager.isVisibilityNotRunTimeException( ErrorDeTopeConstants.PAGE_UP )) {
                DriverManager.getDriverInstance().findElement( By.xpath( ErrorDeTopeConstants.PAGE_UP ) ).click();
            }
        ActionsManager.clickWithoutWait(ErrorDeTopeConstants.BUTON_ACCION, button);
    }

    public static void reAnalizarPorMismoMetodo(String palabra){

        DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.MENU_TIPO_MOTIVO)).click();
        if ("Error analitico".equals(palabra)) {

//            ActionsManager.clickWithoutWait(ErrorDeTopeConstants.MENU_TIPO_MOTIVO);
            ActionsManager.customSendKeys("ERROR ANA", true);
        }

    }

    public static void inputObservacion(String observacion){

        DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.OBSERVACION)).click();
        ActionsManager.customSendKeys(observacion.toUpperCase(), false);
    }

    public static void clickonAceptarOpciones(){
        DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.BUTON_ACEPTAR_OPCION)).click();

    }

    public static void clickConfirmarReanalizar(){
        ActionsManager.waitForElement(ErrorDeTopeConstants.BUTON_CONFIRMACION);
        DriverManager.getDriverInstance().findElement(By.xpath(ErrorDeTopeConstants.BUTON_CONFIRMACION)).click();

    }

    public static void clickCheckNroMuestra(){
        List<WebElement> elements = DriverManager.getDriverInstance().findElements( By.xpath( String.format( "//*[@ClassName='Cell'][contains(@Name,'%s')]/../*[1]", UserManager.getInstance().getNumeroSolicitud() ) ) );
        elements.forEach( WebElement::click );
    }

}