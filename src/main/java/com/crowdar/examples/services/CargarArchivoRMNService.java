package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.CargarArchivoRMNConstants;

import com.crowdar.examples.constants.IngresoResultadosEmaConstans;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.QueriesManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Locale;

public class CargarArchivoRMNService {

    public static void verificarPantalla() {
        Assert.assertTrue(ActionManager.waitVisibility(CargarArchivoRMNConstants.PAGE_VERIFICATION).isDisplayed());
    }

    public static void clickButtonProcesarArchivo(){
        ActionManager.click(CargarArchivoRMNConstants.BUTTON_PROCESAR_ARCHIVO);
    }

    public static void inputCodAnalista(String codAnalista) {
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@ClassName='AnalistaEmpleadoCodeSelectionControl']/*[@AutomationId='this']/*[@AutomationId='xamTextControl']/*[@AutomationId='PART_FocusSite']"));
        ActionsManager.customSendKeys(codAnalista, true);
    }

    public static void inputCodEquipo(String codEquipo) {
        ActionsManager.customSendKeys(500, 2, codEquipo,true);
    }

    public static void inputMatriz(String matriz) {
        ActionsManager.customSendKeys(500, 3, matriz, true);
    }

    public static void clickButonSubir() {
        ActionManager.click(CargarArchivoRMNConstants.BUTON_SUBIR_ARCHIVO);
    }

    public static void importarArchivo(String direccion, String nombreArchivo, String extencion) {
        String file = System.getProperty("user.dir").concat(direccion).concat(nombreArchivo).concat(extencion);
        if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
            WebElement inputWindows = DriverManager.getDriverInstance().findElement(By.xpath("//*[contains(@Name, 'Abrir')]/ComboBox[@ClassName='ComboBox'][contains(@Name, 'Nombre:')]/Edit[contains(@ClassName, 'Edit')][contains(@Name, 'Nombre:')]"));
            inputWindows.sendKeys(file);
            ActionsManager.pressKey(KeyEvent.VK_ENTER);
        } else {
            WebElement inputWindows = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Open']/ComboBox[@ClassName='ComboBox'][@Name='File name:']/Edit[@ClassName='Edit'][contains(@Name, 'File name:')]"));
            inputWindows.sendKeys(file);
            ActionsManager.pressKey(KeyEvent.VK_ENTER);
        }
    }

    public static void clickProcesarArchivo() {
        ActionManager.click(CargarArchivoRMNConstants.BUTON_PROCESAR_ARCHIVO);
    }

    public static void botonAceptar(){
        Assert.assertTrue(ActionManager.waitVisibility(CargarArchivoRMNConstants.BUTON_ACEPTAR).isDisplayed());
        ActionManager.click(CargarArchivoRMNConstants.BUTON_ACEPTAR);
    }
}
