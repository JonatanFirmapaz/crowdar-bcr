package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.BalanzaConstants;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.openqa.selenium.Keys.ENTER;
import static org.openqa.selenium.Keys.TAB;


public class BalanzaService {

    public static void inputNroPorcion(){
        ActionsManager.waitForElement("//*[@AutomationId='nroPorcionTextControl']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']", "");
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='nroPorcionTextControl']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']")).click();
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M-P1", true);
    }

    public static void inputCodAnalista(String codAnalista) {
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@ClassName='AnalistaBalanzaEmpleadoCodeSelectionControl']/*[@AutomationId='this']/*[@AutomationId='xamTextControl']/*[@AutomationId='PART_FocusSite']"));
        ActionsManager.customSendKeys(codAnalista, true);
    }

    public static void inputGranosArdidos(String ardidos){
        ActionsManager.customSendKeys(500, 2, ardidos, false);
    }

    public static void inputGranosDanados(String granosDanados){
        ActionsManager.customSendKeys(800, 2, granosDanados, false);
    }

    public static void inputGranosDanadosSec(String granosDanados){
        ActionsManager.customSendKeys(200, 1, granosDanados, false);
    }

    public static void inputMateriaExtranas(String matExtranas){
        ActionsManager.customSendKeys(500, 1, matExtranas, false);
    }

    public static void inputQuebradosYchusos(String quebradosYchusos){
        ActionsManager.customSendKeys(200, 1, quebradosYchusos, false);
    }

    public static void inputGranosPanzaBlanca(String panzaBlanca){
        ActionsManager.customSendKeys(500, 1, panzaBlanca, false);
    }

    public static void inputGranosPicados(String picados){
        ActionsManager.customSendKeys(200, 1, picados, false);
    }

    public static void inputOtroTipo(String otroTipo) {
        ActionsManager.customSendKeys(200, 1, otroTipo, false);
    }

    public static void inputPureza(String pureza){
        ActionsManager.customSendKeys(200, 1, pureza, false);
    }
    public static void inputContTarrito(String contTarrito){
        ActionsManager.customSendKeys(200, 1, contTarrito, false);
    }

    public static void inputPesoEnsayo(String pesoEnsayo){
        ActionsManager.customSendKeys(200, 1, pesoEnsayo, false);
    }

    public static void botonAceptar(){
        ActionManager.click(BalanzaConstants.BOTON_ACEPTAR);
    }

    public static void botonCancelar(){
        ActionManager.click(BalanzaConstants.BOTON_CANCELAR);
    }

    public static void inputPeseDelEnsayo(String pesoEnsayo){
        ActionManager.click(BalanzaConstants.INPUT_PESO_ENSAYO);
        ActionsManager.customSendKeys(pesoEnsayo, false);
    }

    public static void inputChamico(String chamico){
        ActionsManager.customSendKeys(200, 1, chamico, false);
    }

    public static void inputTarrito(String tarrito){
        ActionsManager.customSendKeys(200, 1, tarrito, false);
    }
}
