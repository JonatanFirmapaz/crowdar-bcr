package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.GenericConstants;
import com.crowdar.examples.constants.IngresoResultadosEmaConstans;
import com.crowdar.examples.constants.PreIngresoSolicitudConstants;

import com.crowdar.examples.steps.StepsInCommon;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.QueriesManager;
import com.crowdar.examples.utils.UserManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.HashMap;

import static com.crowdar.examples.utils.ActionsManager.*;

public class PreIngresoSolicitudService {
    public static String muestra;
    public static void clickOn(String button) {
        try {
            HashMap<String, String> buttons = new HashMap<>();

            buttons.put("GENERAL", PreIngresoSolicitudConstants.GENERAL_BUTTON);

            ActionManager.click(buttons.get(button.toUpperCase()));
            ActionManager.click(PreIngresoSolicitudConstants.INICIAR_SOLICITUD_BUTTON);
        } catch (IllegalArgumentException e) {
            Logger.getLogger(AdministrarSolicitudesService.class).info(String.format("Option %s isn't implemented", button));
        }
    }

    /* PRE INGRESO SOLICITUD - GENERAL */
    public static void completeFieldsWith(String solicitante,String matriz, String grupo, String peso) {
        UserManager.getInstance().setMuestra(ActionsManager.cambiarMuestra("\\src\\test\\resources\\solici\\FlujosBasicos\\FB6\\", "numeroMuestra"));
        UserManager.getInstance().setSolicitante(solicitante);
        if (DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Fecha Ingreso: ']")).isDisplayed()) {
            pressKey(KeyEvent.VK_DOWN, 1);
        }
        customSendKeys(300, 1, UserManager.getInstance().getMuestra(), false);
        customSendKeys(500, 3, UserManager.getInstance().getSolicitante(), true);
        customSendKeys(500, 4, matriz, true);
        customSendKeys(300, 3, grupo, true);
        customSendKeys(1000, 8, peso, false);
    }



}













