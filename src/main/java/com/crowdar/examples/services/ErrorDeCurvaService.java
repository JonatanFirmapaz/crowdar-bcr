package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.ErrorDeCurvaConstatnts;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class ErrorDeCurvaService {
    public static void inputNroMuestra(){
        Assert.assertTrue(ActionManager.waitVisibility(ErrorDeCurvaConstatnts.INPUT_NRO_MUESTRA).isDisplayed());
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M", false);
    }

    public static void clickFiltrar(){
        DriverManager.getDriverInstance().findElement( By.xpath("//*[@ClassName='Button'][@Name='Filtrar']")).click();

    }

    public static void selectNroMuestra (){
        Assert.assertTrue(ActionManager.waitVisibility("//*[@ClassName='Cell'][contains(@Name,'%s')]", UserManager.getInstance().getNumeroSolicitud()).isDisplayed());
        //Custom[@ClassName=\"Cell\"][@Name=\"00015746\"]/Edit[@ClassName=\"XamTextEditor\"]/Text[@AutomationId=\"TextBlock\"]"
        //HeaderItem[@ClassName=\"RecordSelector\"]/CheckBox[@ClassName=\"CheckBox\"]"
        List<WebElement> elements = DriverManager.getDriverInstance().findElements( By.xpath( String.format( "//*[@ClassName='Cell'][contains(@Name,'%s')]/../*[1]", UserManager.getInstance().getNumeroSolicitud() ) ) );
        elements.forEach( WebElement::click );

    }

    public static void popupValidacion(){
        Assert.assertTrue(ActionManager.waitVisibility("//Text[@ClassName='TextBlock'][@Name='Información']").isDisplayed());
        DriverManager.getDriverInstance().findElement(By.xpath("/Button[@Name='Aceptar'][@AutomationId='OKButton']/Text[@ClassName='TextBlock'][@Name='Acep_tar']")).click();

    }

}
