package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.ImportacionArchivoConstants;
import com.crowdar.examples.constants.IngresarMuestraSoliLacradoConstants;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import org.checkerframework.checker.units.qual.K;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.awt.event.KeyEvent;

import static org.openqa.selenium.Keys.ENTER;

public class IngresarMuestraSoliLacradoService {
    public static void seleccionaFirmante (String codFirmante){
//        ActionManager.setInput(IngresarMuestraSoliLacradoConstants.INGRESAR_COD_FIRMANTE, codFirmante, false, false);
//        ServiceInCommon.pressKey(ENTER, ActionManager.getElement(IngresarMuestraSoliLacradoConstants.INGRESAR_COD_FIRMANTE));
//        Assert.assertTrue(ActionManager.waitVisibility(IngresarMuestraSoliLacradoConstants.INGRESAR_COD_FIRMANTE).isDisplayed());
        ActionsManager.customSendKeysWithTimeout(200, codFirmante, false);
        ActionsManager.pressKey(KeyEvent.VK_ENTER);
    }

    public static void clickGuardar (){
//        DriverManager.getDriverInstance().findElement(By.xpath(IngresarMuestraSoliLacradoConstants.BUTTON_GUARDAR)).click();
        Assert.assertTrue(ActionManager.waitVisibility(IngresarMuestraSoliLacradoConstants.BUTTON_GUARDAR).isDisplayed());
//        ActionManager.waitPresence(IngresarMuestraSoliLacradoConstants.BUTTON_GUARDAR);
//        ActionManager.waitVisibility(IngresarMuestraSoliLacradoConstants.BUTTON_GUARDAR);
        ActionsManager.pressKey(KeyEvent.VK_TAB);
        ActionsManager.pressKey(KeyEvent.VK_TAB);
        ActionsManager.pressKey(KeyEvent.VK_ENTER);
    }

    public static void popUpInfo () {
        Assert.assertTrue(ActionManager.waitVisibility(IngresarMuestraSoliLacradoConstants.BUTTON_POPUP_INFO).isDisplayed());
        ActionsManager.clickWithoutWait(IngresarMuestraSoliLacradoConstants.BUTTON_POPUP_INFO);
    }

    public static void ingresaNroMuestra(){
        Assert.assertTrue(ActionManager.waitVisibility(IngresarMuestraSoliLacradoConstants.INPUT_CUARTEAR_MUESTRA).isDisplayed());
        ActionsManager.customSendKeys(UserManager.getInstance().getMuestra(), true);
    }

}
