package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.examples.constants.AdministrarSolicitudes.AdministrarSolicitudesConstants;
import com.crowdar.examples.constants.ImportacionArchivoConstants;
import com.crowdar.examples.utils.ActionsManager;
import org.apache.log4j.Logger;

import java.util.HashMap;

public class AdministrarSolicitudesService {
    public static void clickOn(String button) {
        try {
            HashMap<String, String> buttons = new HashMap<>();

            buttons.put("PRE INGRESAR SOLICITUD", AdministrarSolicitudesConstants.BOTON_PRE_SOLICITUD);
            buttons.put("INGRESAR SOLICITUD AUTOMATICA", AdministrarSolicitudesConstants.BOTON_SOLICITUD_AUTOMATICA);
            buttons.put("INGRESAR SOLICITUD", AdministrarSolicitudesConstants.BOTON_INGRESAR_SOLICITUD);
            buttons.put("VERIFICAR", AdministrarSolicitudesConstants.BOTON_VERIFICAR);

            if (ActionManager.isPresent(ImportacionArchivoConstants.PAGE_UP)) // si esta el page_up realiza accion, de lo contrario NO
                ActionManager.click(ImportacionArchivoConstants.PAGE_UP);
            ActionsManager.clickWithoutWait(buttons.get(button.toUpperCase()));
        } catch (IllegalArgumentException e) {
            Logger.getLogger(AdministrarSolicitudesService.class).info(String.format("Option %s isn't implemented", button));
        }
    }

    public static void filtrarPor(String option) {
        try {
            HashMap<String, String> options = new HashMap<>();

            options.put("BÚSQUEDA AVANZADA", AdministrarSolicitudesConstants.BOTON_BUSQUEDA_AVANZADA);

            ActionManager.click(options.get(option.toUpperCase()));
        } catch (IllegalArgumentException e) {
            Logger.getLogger(AdministrarSolicitudesService.class).info(String.format("Option %s isn't implemented", option));
        }
    }
}
