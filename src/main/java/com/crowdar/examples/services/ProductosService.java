package com.crowdar.examples.services;

import com.crowdar.driver.DriverManager;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;

import java.awt.event.KeyEvent;

public class ProductosService {

    public static void inputNroPorcion(){
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Agregar'][@AutomationId='btnAgregarMuestra']/Text[@ClassName='TextBlock'][@Name='Agregar']")).click();
        ActionsManager.waitForElement("//*[@ClassName='GroupBox'][@Name='Ingreso de Muestras/Porciones Individualment']/Edit[@ClassName='TextBlock']", "");
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M-P1", false);
        ActionsManager.pressKey(KeyEvent.VK_TAB);
        ActionsManager.pressKey(KeyEvent.VK_ENTER);
    }



}
