package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.examples.constants.AboutConstants;

public class IndexService {
    public static void about(){
    	ActionManager.setInput(AboutConstants.IP,"127.0.0.1");
        ActionManager.click(AboutConstants.ABOUT);
    }
}
