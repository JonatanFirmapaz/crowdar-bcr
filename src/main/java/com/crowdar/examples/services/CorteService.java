package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.CorteConstants;
import com.crowdar.examples.constants.GenericConstants;
import com.crowdar.examples.constants.IngresoMuestraConstans;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.openqa.selenium.Keys.ENTER;

public class CorteService {

    public static void ingresarMuestraCorte() {
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M", false);
        try {
            Thread.sleep( 5000 );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        ActionsManager.clickWithoutWait("//Button[@Name=\"Aceptar\"][@AutomationId=\"OKButton\"]", "");
    }

    public static void clickConfirmarPorciones(){
        ActionManager.click(CorteConstants.BOTON_CONFIRMAR_PORCIONES);
    }

    public static void abrirContenedor(){
        ActionManager.waitPresence(IngresoMuestraConstans.CLICK_BOTON_ACEPTAR_CONTENEDOR);
    }

    public static void validarPorcion(){
        WebElement verificacion = DriverManager.getDriverInstance().findElement(By.xpath("//*[@ClassName='Window']/Text[@ClassName='TextBlock'][@Name='Información']"));
        verificacion.isDisplayed();
        WebElement botonAceptar = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Aceptar'][@AutomationId='OKButton']"));
        botonAceptar.click();
    }

    public static void clickCancelar(){
        ActionManager.click(CorteConstants.BOTON_CANCELAR);
    }
}
