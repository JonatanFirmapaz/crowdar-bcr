package com.crowdar.examples.services;

import com.crowdar.examples.database.util.Database;
import com.crowdar.examples.database.util.SqlFileReader;
import com.crowdar.examples.report.CucumberReporter;
import com.crowdar.util.FileUtils;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class FechaYestadoSolicutudService {

    public static String fechaingresoSolicitud(String direccion, String nombreArchivo) throws IOException, SQLException {
        String query = SqlFileReader.getQueryString("generales/obtenerFechaIngresoSolicitud.sql");
        Map<Integer, Object> parameters = new HashMap<>();
        parameters.put(1, ImportacionArchivoService.selectNumeroSolicitud(direccion, nombreArchivo));
        ResultSet resultSet = (ResultSet) Database.executeQueryWithParameters(query, parameters);
        String respuesta = "";
        while (resultSet.next()) {
            respuesta = resultSet.getString("fecha");
        }
        return respuesta;
    }

    public static String estadoSolicitud(String direccion, String nombreArchivo) throws IOException, SQLException {
        String query = SqlFileReader.getQueryString("generales/estaResultadoOk.sql");
        Map<Integer, Object> parameters = new HashMap<>();
        parameters.put(1, ImportacionArchivoService.selectNumeroSolicitud(direccion, nombreArchivo));
        ResultSet resultSet = (ResultSet) Database.executeQueryWithParameters(query, parameters);
        String respuesta = "";
        while (resultSet.next()) {
            respuesta = resultSet.getString("ID_EstadoSolicitud");
            if (respuesta == null){
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e){
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                };
            }
        }
        return respuesta;
    }

    public static void fechaSolicitud(String direccion, String nombreArchivo) throws SQLException, IOException {
        String fecha = fechaingresoSolicitud(direccion, nombreArchivo).replace("/", "");
        String estado = estadoSolicitud(direccion, nombreArchivo);
        System.out.println("La fecha de la solicitud es: " + fecha);
        System.out.println("El estado de la solicitud es: " + estado);
        System.out.println("El numero de Solicitud es: " + ImportacionArchivoService.selectNumeroSolicitud(direccion, nombreArchivo));

        CucumberReporter.addExtraInfo("Numero de Solicitud", ImportacionArchivoService.selectNumeroSolicitud(direccion, nombreArchivo));
    }

    public static void crearTxt(String direccion, String nombreArchivo) throws IOException, SQLException {
        String fecha = fechaingresoSolicitud(direccion, nombreArchivo).replace("/", "");
        String pathFile = (System.getProperty("user.dir").concat(File.separator).concat("src").concat(File.separator).concat("test").concat(File.separator).concat("resources").concat(File.separator).concat("SolicitudesAVerificar").concat(File.separator).concat("Datos").concat(".txt"));
        FileWriter fileWriter = new FileWriter(pathFile, true);
        try {
            fileWriter.write(ImportacionArchivoService.selectNumeroSolicitud(direccion, nombreArchivo));
            fileWriter.write(", ");
            fileWriter.write(fecha);
            fileWriter.write("\r\n");

            fileWriter.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }


}
