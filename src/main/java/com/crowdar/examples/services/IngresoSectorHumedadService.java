package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.IngresoResultadosEmaConstans;
import com.crowdar.examples.constants.IngresoSectorHumedadConstans;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.QueriesManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.awt.event.KeyEvent;

public class IngresoSectorHumedadService {

    public static void verificarPantalla() {
        Assert.assertTrue(ActionManager.waitVisibility(IngresoSectorHumedadConstans.PANTALLA_SEC_HUMEDAD).isDisplayed());
    }

    public static void ingresarCodAnalista(String codAnalista){
        Assert.assertTrue(ActionManager.waitVisibility(IngresoSectorHumedadConstans.CODIGO_ANALISTA_INPUT).isDisplayed());
        ActionsManager.customSendKeys(codAnalista, true);
        ActionsManager.clickWithoutWait(IngresoSectorHumedadConstans.BUTON_CARGA_ANALISTA);
    }

    public static void ingresaFecha(){
        ActionManager.click(IngresoSectorHumedadConstans.FECHA_INPUT);
        ActionsManager.pressKey(KeyEvent.VK_DOWN);
        ActionsManager.clickWithoutWait(IngresoSectorHumedadConstans.BUTON_CARGA_FECHA);
    }

    public static void ingresarValorTC1(String TC1){
        ActionManager.click(IngresoSectorHumedadConstans.INPUT_TC1);
        ActionsManager.customSendKeys(TC1, false);
    }

    public static void ingresarValorTC2(String TC2){
        ActionsManager.customSendKeys(500, 1, TC2, false);
    }

    public static void ingresarValorPM1(String PM1){
        ActionsManager.customSendKeys(500, 1, PM1, false);
    }

    public static void ingresarValorPM2(String PM2){
        ActionsManager.customSendKeys(500, 1, PM2, false);
    }

    public static void ingresarValorPCMS1(String PCMS1){
        ActionManager.click(IngresoSectorHumedadConstans.INPUT_PCMS1);
        ActionsManager.customSendKeys(PCMS1, false);
    }

    public static void ingresarValorPCMS2(String PCMS2){
        ActionsManager.customSendKeys(500, 1, PCMS2, false);
    }

    public static void validarResultados(String resultado){
        WebElement estado = DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='contentText']/Text[@ClassName='TextBlock'][starts-with(@Name,'"+getCheckResultados(resultado)+"')]"));
        //[@AutomationId='contentText']/Text[@ClassName='TextBlock'][starts-with(@Name,'"+getCheckResultados(resultado)+"')]
        estado.isDisplayed();
        WebElement botonAceptar = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Aceptar'][@AutomationId='OKButton']"));
        botonAceptar.click();
    }

    public static String getCheckResultados(String resultado){
        return resultado;
    }

    public static void ingresarMuestraPorcion() {
        String numeroSolicitud = QueriesManager.getInstance().consultarNumeroSolicitud(UserManager.getInstance().getMuestra());
        UserManager.getInstance().setNumeroSolicitud(numeroSolicitud);
        Assert.assertTrue(ActionManager.waitVisibility(IngresoResultadosEmaConstans.INGRESAR_MUESTRA_INPUT).isDisplayed());
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M-P1", true);
    }
}
