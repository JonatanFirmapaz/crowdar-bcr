package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.GenericConstants;
import com.crowdar.examples.constants.IngresoResultadosEmaConstans;
import com.crowdar.examples.utils.ActionsManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.awt.event.KeyEvent;

public class ServiceInCommon {

    public static void pressKey(Keys key, WebElement elemento){
           elemento.sendKeys(key);
    }

    public static void cerrarContenedor() {
        ActionManager.click(IngresoResultadosEmaConstans.BOTON_NO_ABRIR_CONTENEDOR);
        ActionsManager.pressKey(KeyEvent.VK_ENTER, 1, 4000); // keyEvent, cantidad, e intervalo de espera en milisegundos

    }

    public static void guardarDatos() {
        ActionManager.waitVisibility( GenericConstants.BOTON_GUARDAR ).click();
    }

    public static void clickButon(String name){
        DriverManager.getDriverInstance().findElement( By.xpath( String.format( "//*[@Name='%s'][@ClassName='Button']", name ) ) ).click();


    }
}
