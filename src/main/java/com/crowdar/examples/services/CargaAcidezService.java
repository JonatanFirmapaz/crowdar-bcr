package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.examples.constants.CargaAcidezConstans;
import com.crowdar.examples.constants.IngresoSectorHumedadConstans;
import com.crowdar.examples.utils.ActionsManager;
import org.testng.Assert;

public class CargaAcidezService {

    public static void inputTM(String TM){
        Assert.assertTrue(ActionManager.waitVisibility(CargaAcidezConstans.INPUT_TM).isDisplayed());
        ActionManager.click(CargaAcidezConstans.INPUT_TM);
        ActionsManager.customSendKeys(TM, true);
    }

    public static void inputPMA(String PMA){
        ActionsManager.customSendKeys(500, 1, PMA, false);
    }

    public static void input_V(String V){
        ActionsManager.customSendKeys(500, 1, V, false);
    }
}
