package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.IngresarContenedorOMuestraPorcionAUnSector.IngresarContenedorOMuestraPorcionAUnSectorConstants;
import com.crowdar.examples.constants.IngresoResultadosEmaConstans;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.QueriesManager;
import com.crowdar.examples.utils.UserManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.awt.event.KeyEvent;

import static com.crowdar.examples.steps.StepsInCommon.isComponentVisible;
import static org.openqa.selenium.Keys.ENTER;

public class IngresoResultadosEmaService {

    public static void ingresarMuestra() {
        String numeroSolicitud = QueriesManager.getInstance().consultarNumeroSolicitud(UserManager.getInstance().getMuestra());
        UserManager.getInstance().setNumeroSolicitud(numeroSolicitud);
        Assert.assertTrue(ActionManager.waitVisibility(IngresoResultadosEmaConstans.INGRESAR_MUESTRA_INPUT).isDisplayed());
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M", true);
    }

    public static void verificarEma(){
        ActionManager.waitVisibility(IngresoResultadosEmaConstans.EMA_PANTALLA);
    }

    public static void inputCodigoAnalista(String codAnalista){
        Assert.assertTrue(ActionManager.waitVisibility(IngresoResultadosEmaConstans.CODIGO_ANALISTA_INPUT).isDisplayed());
        ActionsManager.customSendKeys(codAnalista, true);
    }

    public static void inputPH(String PH){
        ActionsManager.customSendKeys(500, 3, PH, false);
        //ActionManager.setInput(IngresoResultadosEmaConstans.PH_INPUT, PH, false);
    }

    public static void inputHEA(String HEA){
        ActionsManager.customSendKeys(500, 1, HEA, false);
  }

    public static void inputCodigoEquipo(String codEquipo) {
        ActionsManager.customSendKeys(500, 1, codEquipo, true);
    }

    public static void inputAnalistaCP(String codAnalista){
        //Se realiza de esta forma porque el Locator contiene un caracter especial no soportado por teclado ingles.
        WebElement analistaCP = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Contenido proteico']/parent::*/*[@ClassName='EtapaControl']/*[@ClassName='GroupBox']/*[@ClassName='AnalistaEtapaControl']/*[@ClassName='GroupBox']/*[@AutomationId='analistaEmpleadoCodeSelectionControl']/*[@AutomationId='this']/*[@AutomationId='xamTextControl']/*[@AutomationId='PART_FocusSite']"));
        analistaCP.click();
        analistaCP.sendKeys(codAnalista);
        analistaCP.sendKeys(ENTER);
    }

    public static void inputCP(String CP){
        ActionManager.setInput(IngresoResultadosEmaConstans.CP_INPUT, CP, true);
        ServiceInCommon.pressKey(ENTER, ActionManager.getElement(IngresoResultadosEmaConstans.CP_INPUT));

    }

    public static void inputCodigoEquipoCP(String codEquipoCP){
        WebElement equipoCP = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Contenido proteico']/parent::*/*[@ClassName='EtapaControl']/*[@ClassName='GroupBox']/*[@ClassName='AnalistaEtapaControl']/*[@ClassName='GroupBox']/*[@AutomationId='this']/*[@AutomationId='controlComponente']/*[@AutomationId='this']/*[@AutomationId='xamTextControl']/*[@AutomationId='PART_FocusSite']"));
        equipoCP.click();
        equipoCP.sendKeys(codEquipoCP);
        equipoCP.sendKeys(ENTER);
    }

    public static void botonAceptar(){
        if ( isComponentVisible( "//*[@AutomationId='mainCommandButtonControl']/Text[@ClassName='TextBlock'][@Name='Aceptar Componente']" ) ) {
            ActionManager.click(IngresoResultadosEmaConstans.BOTON_ACEPTAR);
        }
    }

    public static void botonNoAbrirContenedor(){
        if ( isComponentVisible( "//*[@AutomationId='noRealizarRadioButtonControl2']/*[@Name='No abrir contenedor']" ) ) {
        //if ( isComponentVisible( IngresoResultadosEmaConstans.BOTON_NO_ABRIR_CONTENEDOR ) ) {
            ActionManager.click(IngresoResultadosEmaConstans.BOTON_NO_ABRIR_CONTENEDOR);
            ActionsManager.pressKey( KeyEvent.VK_ENTER );
        }
    }

    public static void validarResultados(String resultado){
        WebElement estado = DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='contentText']/Text[@ClassName='TextBlock'][@Name='"+getCheckResultados(resultado)+"']"));
        estado.isDisplayed();
        WebElement botonAceptar = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Aceptar'][@AutomationId='OKButton']"));
        botonAceptar.click();
    }

    public static String getCheckResultados(String resultado){
        return resultado;
    }

    public static void botonCancelar(){
        ActionManager.click(IngresoResultadosEmaConstans.BOTON_CANCELAR);
    }
}