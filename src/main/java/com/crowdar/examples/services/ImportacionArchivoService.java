package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.*;
import com.crowdar.examples.database.util.Database;
import com.crowdar.examples.database.util.SqlFileReader;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.awt.event.KeyEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.openqa.selenium.Keys.ENTER;
import static org.openqa.selenium.Keys.TAB;


public class ImportacionArchivoService {

    public static void buscarInputMenu(String palabraClave){
        ActionManager.click(ImportacionArchivoConstants.INPUT_SEARCH);
        ActionManager.waitVisibility(ImportacionArchivoConstants.PALABRA_CLAVE);
        ActionManager.click(ImportacionArchivoConstants.PALABRA_CLAVE);
        switch(palabraClave) {
            case "Administrar Solicitudes":
                ActionsManager.customSendKeys("ADMINISTRAR SOLICI", false);
                break;
            case "Ingresar Muestras de Solicitudes Automática":
                ActionsManager.customSendKeys("INGRESAR MUESTRAS DE SOLICITUDES AUTO", false);
                break;
            case "Cerrar Contenedor":
                ActionsManager.customSendKeys("CERRAR CONTENED", false);
                break;
            case "Ingresar Contenedor o Muestra/Porción a un sector":
                ActionsManager.customSendKeys("INGRESAR CONTENEDOR O MUESTRA", false);
                break;
            case "Ingresar Resultados EMA":
                ActionsManager.customSendKeys("INGRESAR RESULTADOS E", false);
                break;
            case "Leer Muestra/Porción para Corte":
                ActionsManager.customSendKeys("LEER MUESTRA", false);
                break;
            case "Leer Muestra/Porción para Ingresar Componentes":
                ActionManager.setInput( ImportacionArchivoConstants.PALABRA_CLAVE, "Leer Muestra/Porción para Ingresar Com" );
                break;
            case "Ingresar Muestra o Contenedor a Archivo":
                ActionsManager.customSendKeys("INGRESAR MUESTRA O CONTENEDOR A ARCH", false);
                break;
            case "Administrar Archivo":
                ActionsManager.customSendKeys("ADMINISTRAR ARCHIV", false);
                break;
            case "Asignar Analista A Muestras Porciones Dentro De Un Contenedor":
                ActionsManager.customSendKeys("ASIGNAR ANALISTA A MUESTRAS PORCIONES DENTRO DE UN CONT", false);
                break;
            case "Ingresar Componentes Ensayos Sector Balanza-Físico":
                ActionsManager.customSendKeys("INGRESAR COMPONENTES ENSAYOS SECTOR BALANZA", false);
                break;
            case "Vaciar Contenedor":
                ActionsManager.customSendKeys("VACIAR CONTENED", false);
                break;
            case "Consultar Resultados de Ensayos de una Solicitud":
                ActionsManager.customSendKeys("CONSULTAR RESULTADOS DE ENSAYOS", false);
                break;
            case "Administrar Facturas de Contado":
                ActionsManager.customSendKeys( "ADMINISTRAR FACTURAS DE C", false );
                break;
            case "Re-analizar Ensayo con Resultado Correcto":
                ActionsManager.customSendKeys( "RE-ANALIZAR ENSAYO CON RESULTADO", false );
                break;
            case "Listar Muestras-Porciones con Error de Tope":
                ActionsManager.customSendKeys( "LISTAR MUESTRAS-PORCIONES CON ERROR DE TO", false );
                break;
            case "Pedido de Extracción":
                ActionsManager.customSendKeys( "PEDIDO DE EXTRA", false );
                break;
            case "Listar Solicitudes Pendientes de Decisión de Resultado":
                ActionsManager.customSendKeys( "LISTAR SOLICITUDES PENDIENTES DE D", false );
                break;
            case "Consultar Procesos de Archivos RMN ensayo Materia Grasa":
                ActionsManager.customSendKeys("CONSULTAR PROCESOS DE ARCHIVOS RMN ENSAYO MATERIA GRA", false);
                break;
            case "Ingresar Muestras Solicitud de Lacrado":
                ActionsManager.customSendKeys( "INGRESAR MUESTRA SOL", false );
                break;
            case "Seleccionar Empleado Firmante de Lacrado":
                ActionsManager.customSendKeys( "SELEC", false );
                break;
            case "Cuartear Muestra de Lacrado":
                ActionsManager.customSendKeys( "CU", false );
                break;
            case "Listar Solicitudes con Error Procesadas por RMN":
                ActionsManager.customSendKeys("LISTAR S", false);
                break;
            case "Listar Muestras-Porciones con Error de Criterio de Aceptación":
                ActionsManager.customSendKeys("LISTAR MUESTRAS-PORCIONES CON ERROR DE CRITERIO DE ACEPTA", false);
                break;
            case "Modificar Componentes":
                ActionsManager.customSendKeys("MODIFICAR COMPONENTE", false);
                break;
        }

        ActionsManager.pressKey(KeyEvent.VK_ENTER);
    }

    public static void clickBotonIngresarSolicitud() {
        ActionManager.waitVisibility(ImportacionArchivoConstants.ADMINISTRAR_SOLICITUDES_SECTION);
        if (ActionManager.isPresent(ImportacionArchivoConstants.PAGE_UP)) // si esta el page_up realiza accion, de lo contrario NO
            ActionManager.click(ImportacionArchivoConstants.PAGE_UP);
        WebElement botonIngresoMuestra= DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Ingresar Solicitud Automática']"));
        botonIngresoMuestra.isDisplayed();
        botonIngresoMuestra.click();
    }

    public static void clickBotonSubirArchivo() {
        try {
            Assert.assertTrue(ActionManager.waitVisibility(DetalleConstants.INGRESO_SOLICITUD_AUTOMATICA_TITLE).isDisplayed());
            ActionsManager.pressKey(KeyEvent.VK_TAB);
            ActionsManager.pressKey(KeyEvent.VK_ENTER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void importarArchivo(String direccion, String nombreArchivo) throws IOException {
        modificarMuestra(direccion, nombreArchivo);
        String file = System.getProperty("user.dir").concat(direccion).concat(nombreArchivo).concat(".txt");
        if (Locale.getDefault().getLanguage().equalsIgnoreCase("es")) {
            WebElement inputWindows = DriverManager.getDriverInstance().findElement(By.xpath("//*[contains(@Name, 'Abrir')]/ComboBox[@ClassName='ComboBox'][contains(@Name, 'Nombre:')]/Edit[contains(@ClassName, 'Edit')][contains(@Name, 'Nombre:')]"));
            inputWindows.sendKeys(file);
            ActionsManager.pressKey(KeyEvent.VK_ENTER);
        } else {
            WebElement inputWindows = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Open']/ComboBox[@ClassName='ComboBox'][@Name='File name:']/Edit[@ClassName='Edit'][contains(@Name, 'File name:')]"));
            inputWindows.sendKeys(file);
            ActionsManager.pressKey(KeyEvent.VK_ENTER);
        }
    }

    public static void modificarMuestra(String direccion, String nombreArchivo) throws IOException {
        String path = System.getProperty("user.dir").concat(direccion).concat(nombreArchivo).concat(".txt");
        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<Object> listadoLineasSolici;
        try {
            listadoLineasSolici = Arrays.asList(bufferedReader.lines().toArray());
            StringBuilder nuevasLineas = new StringBuilder();
                for (Object texto : listadoLineasSolici) {
                    String numeroDeMuestra = texto.toString().substring(0, 15);
                    String numeroNuevoMuestra = numeroDeMuestra.substring(0, 2);
                    String fechaMuestra = numeroDeMuestra.substring(2, 8);
                    String fechaDelDia = new SimpleDateFormat("ddMMyy").format(new java.util.Date());
                    String flujo = numeroDeMuestra.substring(8, 12);
                    Integer intento = Integer.valueOf(numeroDeMuestra.substring(12, 14));
                    if (fechaMuestra.equals(fechaDelDia)) {
                        intento++;
                        numeroNuevoMuestra = numeroNuevoMuestra + fechaMuestra + flujo + String.format("%02d", intento);
                    } else {
                        numeroNuevoMuestra = numeroNuevoMuestra + fechaDelDia + flujo + "00";
                    }
                    texto = texto.toString().replace(numeroDeMuestra, numeroNuevoMuestra + " ");
                    nuevasLineas.append(texto).append("\r\n");
                }

            FileWriter fileWriter = new FileWriter(path, false);
            fileWriter.write(String.valueOf(nuevasLineas));

            fileWriter.close();
            fileReader.close();
            bufferedReader.close();
        }catch(Exception e){
            System.out.println("Falló " + e);
        }
    }

    public static void clickBotonProcesar() {
        ActionManager.click(ImportacionArchivoConstants.BOTON_PROCESAR_ARCHIVO);
    }

    public static void ingresaCodigoPuerto(String codigoPuerto) {
        ActionManager.setInput(ImportacionArchivoConstants.INPUT_CODIGO_PUERTO, codigoPuerto, true, false);
        ServiceInCommon.pressKey(ENTER, ActionManager.getElement(ImportacionArchivoConstants.INPUT_CODIGO_PUERTO));

    }

    public static void validaIngresoMuestras() {
        ActionManager.getElement(ImportacionArchivoConstants.TABLA_DATOS_ARCHIVOS_SOLICI);
        String muestrasProcesadas = ActionManager.getText(ImportacionArchivoConstants.VALOR_MUESTRAS_PROCESADAS);
        String muestrasCorrectas = ActionManager.getText(ImportacionArchivoConstants.VALOR_MUESTRAS_CORRECTAS);
        System.out.println(muestrasProcesadas);
        System.out.println(muestrasCorrectas);
    }

    public static void close(){
        ActionManager.click(ImportacionArchivoConstants.CLOSE);
    }

    public static String getTxtFromFile(String direccion, String nombreArchivo) throws IOException {
        String path = System.getProperty("user.dir").concat(direccion).concat(nombreArchivo).concat(".txt");
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    public static String selectNumeroSolicitud(String direccion, String nombreArchivo) throws IOException, SQLException {
        String query = SqlFileReader.getQueryString("generales/obtenerNumeroSolicitud.sql");
        Map<Integer, Object> parameters = new HashMap<>();
        parameters.put(1, UserManager.getInstance().getMuestra());
        ResultSet resultSet = (ResultSet) Database.executeQueryWithParameters(query, parameters);
        String respuesta = "";
        while (resultSet.next()) {
            respuesta = resultSet.getString("Numero_Solicitud");
        }
        return respuesta;
    }

    public static void numeroSolicitud(String direccion, String nombreArchivo) throws SQLException, IOException {
        UserManager.getInstance().setNumeroSolicitud( selectNumeroSolicitud(direccion, nombreArchivo) );
    }
}