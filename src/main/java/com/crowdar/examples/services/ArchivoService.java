package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.ArchivoConstants;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class ArchivoService {

   public static void clickAgregar(){
        ActionManager.click(ArchivoConstants.BOTON_AGREGAR);
   }

   public static void ingresarMuestra(){
       ActionsManager.clickWithoutWait(ArchivoConstants.INPUT_INGRESAR_MUESTRA);
       ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M", false);
   }

   public static void clickBotonAceptar(){
        WebElement botonAceptar = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Aceptar'][@AutomationId='mainCommandButtonControl']"));
        botonAceptar.click();
   }

   public static void validarEstadoMuestra(){
       String estadoValidacion = ActionManager.getText(ArchivoConstants.VALIDAR_ESTADO);
       Assert.assertEquals(estadoValidacion, "Correcta.");
       ActionManager.click(ArchivoConstants.BOTON_ACEPTAR);
   }

   public static void clickBotonIngresar(){
       ActionManager.click(ArchivoConstants.BOTON_INGRESAR);
   }

   public static void clickBotonAgregar(){
       ActionManager.click(ArchivoConstants.BOTON_AGREGAR_MUESTRA);
       ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M", false);
   }

   public static void inputContDestino(String contDestino) {
       ActionsManager.customSendKeys(200, 4, contDestino, false);
   }

   public static void botonAceptar(){
       ActionManager.click(ArchivoConstants.BOTON_ACEPTAR_MUESTRA);
   }

   public static void validarIngreso(){
       String validacion = ActionManager.getText(ArchivoConstants.VALIDAR_ESTADO);
       Assert.assertEquals(validacion, "Correcta.");
       ActionManager.click(ArchivoConstants.ACEPTAR_VALIDATION);
   }
}
