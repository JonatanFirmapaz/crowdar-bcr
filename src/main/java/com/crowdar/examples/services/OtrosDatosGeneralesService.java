package com.crowdar.examples.services;

import com.crowdar.driver.DriverManager;
import com.crowdar.examples.utils.ActionsManager;
import org.openqa.selenium.By;

public class OtrosDatosGeneralesService {

    public static void completeFieldsWith(String pagador, String formaPago) {
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Otros Datos Generales']")).click();

        ActionsManager.customSendKeys(200, 7, pagador, true);
        ActionsManager.customSendKeys(500, 4, formaPago, true);
    }

}
