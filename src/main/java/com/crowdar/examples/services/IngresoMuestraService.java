package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.core.actions.WebActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.ArchivoConstants;
import com.crowdar.examples.constants.GenericConstants;
import com.crowdar.examples.constants.IngresarContenedorOMuestraPorcionAUnSector.IngresarContenedorOMuestraPorcionAUnSectorConstants;
import com.crowdar.examples.constants.IngresoMuestraConstans;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.QueriesManager;
import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.openqa.selenium.Keys.ARROW_DOWN;
import static org.openqa.selenium.Keys.ENTER;


public class IngresoMuestraService {

    public static void ingresarFechaDelDia() {
        Assert.assertTrue(ActionManager.waitVisibility(IngresoMuestraConstans.INPUT_FECHA).isDisplayed());
        ActionsManager.pressKey(KeyEvent.VK_DOWN);
    }

    public static void ingresarcodigoPuerto(String codigoPuerto) {
        ActionsManager.customSendKeys(500, 1, codigoPuerto, true);
    }

    public static void ingresarNumeroDeMuestra(String direccion, String nombreArchivo) throws IOException {
        String textoArchivo = getTxtFromFile(direccion, nombreArchivo);
        UserManager.getInstance().setMuestra( textoArchivo.substring( 0, 14 ) );
        ActionManager.setInput(IngresoMuestraConstans.INPUT_NUMERO_MUESTRA, UserManager.getInstance().getMuestra(), true, false);
        ActionsManager.pressKey(KeyEvent.VK_ENTER, 500);
    }
//lacrado validar muestra
    public static void validarSistMensaje(String mensaje) {
//        Assert.assertTrue(ActionManager.isPresent());
        WebElement estado = DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='contentText']/Text[@ClassName='TextBlock'][@Name='"+getCheckMessage(mensaje)+"']"));
        estado.isDisplayed();
    }
    public static String getCheckMessage(String mensaje){
        return mensaje;
    }

    public static String getTxtFromFile(String direccion, String nombreArchivo) throws IOException {
        String path = new String(System.getProperty("user.dir").concat(direccion).concat(nombreArchivo).concat(".txt"));
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    public static void clickEnElBotonRegistrarSolicitud() {
        ActionManager.click(IngresoMuestraConstans.CLICK_BOTON_REGISTRAR_SOLICITUD);
    }

    public static void validarMensajeSist(String mensajeSistema){
        String validacion = ActionManager.getText(ArchivoConstants.VALIDAR_ESTADO);
        Assert.assertEquals(validacion, "Correcta.");
    }

    public static void ingresoNombreContZorra(String codigoContenedor) {
//        DriverManager.getDriverInstance().findElement(By.xpath(IngresoMuestraConstans.INPUT_CODIGO_CONT_ZORRA)).click();
        Assert.assertTrue(ActionManager.waitVisibility(IngresoMuestraConstans.INPUT_CODIGO_CONT_ZORRA).isDisplayed());
        ActionsManager.customSendKeys(codigoContenedor, true);
    }

    public static void clickEnElBotonAbrirNuevoContenedor() {
        Assert.assertTrue(ActionManager.waitVisibility(IngresoMuestraConstans.BOTON_NUEVO_CONTEDOR).isDisplayed());
        ActionsManager.pressKey(KeyEvent.VK_ENTER);
    }

    public static void clickEnElBotonAbrirContenedor() {
        ActionManager.click(IngresoMuestraConstans.CLICK_BOTON_ABRIR_CONTENEDOR);

        //ActionsManager.waitVisibilityFromTargetDynamicTimeout(IngresoMuestraConstans.CLICK_BOTON_ACEPTAR_MODAL_INFO, 120);
        //ActionsManager.pressKey(KeyEvent.VK_ENTER, 2, 1000);
        //ServiceInCommon.pressKey(ENTER, ActionManager.getElement(IngresoMuestraConstans.CLICK_BOTON_ACEPTAR_MODAL_INFO));
    }

    public static void getModalContenedor() {
        WebElement element = ActionManager.getElement(IngresoMuestraConstans.MODAL_CONTENEDOR);
        element.isDisplayed();
    }

    public static void ingresoNombreContenedor(String codigoContenedor) {
        Assert.assertTrue(ActionManager.waitVisibility(IngresoMuestraConstans.INPUT_CODIGO_CONTENEDOR).isDisplayed());
        ActionsManager.customSendKeys(codigoContenedor, false);
    }

    public static void ingresoNombreContenedorParaCerrarlo(String codigoContenedor) {
        ActionManager.setInput(IngresoMuestraConstans.INPUT_CERRAR_CONTENEDOR, codigoContenedor, true, false);
    }

    public static void desactivarCheckBox() {
        ActionManager.setCheckbox(IngresoMuestraConstans.CHECKBOX_CONTENEDOR, false);
    }

    public static void getModalCreacionContenedor() {
        ActionsManager.checkAlertInformation("Se han guardado los cambios");
        ActionsManager.pressKey(KeyEvent.VK_ENTER);
        ActionsManager.pressKey(KeyEvent.VK_ENTER, 500);
    }

    public static void clickEnElBotonCerrarContenedor() {
        ActionManager.click(IngresoMuestraConstans.BOTON_CERRAR_CONTENEDOR);
    }

    public static void getModalCierreContenedor() {
        String mensaje = ActionManager.getText(IngresoMuestraConstans.MENSAJE_MODAL_CONTENEDOR_CERRADO);
        Assert.assertEquals(mensaje, "El Contenedor fue cerrado.");
        ActionManager.click(IngresoMuestraConstans.CLICK_BOTON_ACEPTAR_MODAL_INFO);
    }

    public static void cierreAplicacionLaboratorio() {
        // DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name=\"Lista\"][@AutomationId=\"listRegion\"]/Button[@ClassName=\"Button\"]")).click();
        DriverManager.getDriverInstance().findElement(By.xpath("//TitleBar[@AutomationId=\"TitleBar\"]/Button[@Name=\"Cerrar\"][@AutomationId=\"Close\"]")).click();

        // ActionManager.click(IngresoMuestraConstans.BOTON_CERRAR_LABOATORIO);
        ActionManager.click(IngresoMuestraConstans.BOTON_SI_CERRAR_LABORATORIO);
    }

    public static void ingresoNombreContenedorMasEnter(String codigoContenedor) {
        Assert.assertTrue(ActionManager.waitVisibility(IngresoMuestraConstans.INPUT_CODIGO_CONTENEDOR).isDisplayed());
        ActionsManager.customSendKeys(200, 0, codigoContenedor, true);
    }

    public static void ingresoMuestraPorcion() {

    }

    /*public static void cierreVentana() throws InterruptedException {
        Thread.sleep(5000);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }*/

    public static void ingresarCodigoMuestra() {
        ActionsManager.clickWithoutWait(IngresarContenedorOMuestraPorcionAUnSectorConstants.BOTON_AGREGAR);
        String numeroSolicitud = QueriesManager.getInstance().consultarNumeroSolicitud(UserManager.getInstance().getMuestra());
        UserManager.getInstance().setNumeroSolicitud(numeroSolicitud);
        Assert.assertTrue(ActionManager.waitVisibility(IngresarContenedorOMuestraPorcionAUnSectorConstants.INGRESAR_MUESTRA).isDisplayed());
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M", false);
        ActionsManager.clickWithoutWait(GenericConstants.BOTON_ACEPTAR);
    }

    public static void ingresarCodigoMuestraPorcion() {
        ActionsManager.clickWithoutWait(IngresarContenedorOMuestraPorcionAUnSectorConstants.BOTON_AGREGAR);
        String numeroSolicitud = QueriesManager.getInstance().consultarNumeroSolicitud(UserManager.getInstance().getMuestra());
        UserManager.getInstance().setNumeroSolicitud(numeroSolicitud);
        Assert.assertTrue(ActionManager.waitVisibility(IngresarContenedorOMuestraPorcionAUnSectorConstants.INGRESAR_MUESTRA).isDisplayed());
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M-P1", false);
        ActionsManager.clickWithoutWait(GenericConstants.BOTON_ACEPTAR);
    }
}