package com.crowdar.examples.services;

import com.crowdar.examples.database.util.Database;
import com.crowdar.examples.database.util.SqlFileReader;
import com.crowdar.examples.utils.UserManager;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ActualizarPuestoDeTrabajoService {

    public static void actualizarPuesto(String puestoActual, int puestoNuevo) throws IOException, SQLException {
        String query = SqlFileReader.getQueryString("generales/actualizaPuestoDeTrabajo.sql");
        Map<Integer, Object> parameters = new HashMap<>();
        parameters.put(1, puestoNuevo);
        parameters.put(2, UserManager.getInstance().getPuesto());
        Database.executeQueryWithParameters(query, parameters);
    }
}
