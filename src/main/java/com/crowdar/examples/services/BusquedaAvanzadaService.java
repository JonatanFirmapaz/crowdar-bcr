package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.core.actions.MobileActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.BusquedaAvanzadaConstants;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.FlowActions;

import com.crowdar.examples.utils.UserManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.awt.event.KeyEvent;
import java.util.HashMap;

public class BusquedaAvanzadaService {

    public static void completeFieldsForBehaviors(String... behaviors) {
        for (String behavior: behaviors) {
            FlowActions.valueOf(behavior.toUpperCase()).performBehavior();
        }
    }

    public static void setDate() {
        try {
            ActionsManager.pressKey(KeyEvent.VK_DOWN, 1);
            ActionsManager.pressKey(KeyEvent.VK_TAB, 1);
            ActionsManager.pressKey(KeyEvent.VK_DOWN, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clickOn(String component) {
        HashMap<String, String> components = new HashMap<>();

        components.put("FILTRAR", BusquedaAvanzadaConstants.BOTON_FILTRAR);
        components.put("INGRESAR SOLICITUD", BusquedaAvanzadaConstants.BOTON_INGRESAR_SOLICITUD);

        if (MobileActionManager.waitVisibility(components.get(component.toUpperCase())).isDisplayed())
            System.out.println("O");

        ActionManager.click(components.get(component.toUpperCase()));
    }

    public static void selectCell( ) {
        ActionsManager.clickWithoutWait("//*[contains(@Name, '%s')]", UserManager.getInstance().getNumeroSolicitud() );
    }

    public static void selectCheck(){
        ActionsManager.clickWithoutWait("//*[@Name='Records']/DataItem[@ClassName='Record'][@Name='BCR.Lab.Business.Entities.MuestrasPorciones']/HeaderItem[@ClassName='RecordSelector']/CheckBox[@ClassName='CheckBox']");
    }
}
