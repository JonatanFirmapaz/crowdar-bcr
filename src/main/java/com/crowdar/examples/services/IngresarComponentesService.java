package com.crowdar.examples.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import com.crowdar.examples.constants.ModificarComponentesConstans;
import com.crowdar.examples.utils.ActionsManager;
import com.crowdar.examples.utils.UserManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.util.List;

public class IngresarComponentesService {

    public static void ingresarMuestra() {
        ActionsManager.waitForElement("//Custom[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']");
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M", true);
    }
    public static void ingresarNroPorcion() {
        ActionsManager.waitForElement("//Custom[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']");
        ActionsManager.customSendKeys(UserManager.getInstance().getNumeroSolicitud()+"-M-P1", true);
    }

    public static void clickAceptarSolictud(){
        ServiceInCommon.clickButon("Aceptar Solicitud");
    }

    public static void selectCompRegistered(){
        ActionsManager.waitForElement("//Custom[@ClassName='Cell'][@Name='Chamico']/Text[@ClassName='TextBlock'][@Name='Chamico']");
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@ClassName='Cell'][contains(@Name,'Chamico')]/../*[1]")).click();
        ServiceInCommon.clickButon("Aceptar Muestra / Porción");
    }

    public static void inputCodAnalista(String codAnalista){

        DriverManager.getDriverInstance().findElement(By.xpath("//*[@ClassName='EmpleadoCodeSelectionControl']/*[@AutomationId='this']/*[@AutomationId='xamTextControl']/*[@AutomationId='PART_FocusSite']")).click();
        ActionsManager.customSendKeys(codAnalista, true);
        ServiceInCommon.clickButon("Pre-cargar Analista");
    }

    public static void selectDate (){
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='fechaAReplicarDateTimeControl']/Edit[@AutomationId='xamDateTimeControl']")).click();
        ActionsManager.pressKey(KeyEvent.VK_DOWN);
        ServiceInCommon.clickButon("Pre-cargar Fecha");
    }

    public static void inputCantSemilla (String CSch){
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='numericControl_CantidaddeSemillasCSch']")).click();
        ActionsManager.customSendKeys(100, 0, CSch,  true);
    }

    public static void inputPesoSobreZaranda (String PSZ){
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='numericControl_PesoSobreZarandaPSZ']")).click();
        ActionsManager.customSendKeys(100, 0, PSZ, true);
    }

    public static void inputPesoBajoZaranda (String PBZ){
        DriverManager.getDriverInstance().findElement(By.xpath("//*[@AutomationId='numericControl_PesoBajoZarandaPBZ']")).click();
        ActionsManager.customSendKeys(100, 0, PBZ, true);
    }

    public static void clickAceptarComp(){
        ServiceInCommon.clickButon("Aceptar Componente");
    }

    public static void clickOnAceptar(){
        DriverManager.getDriverInstance().findElement(By.xpath("//Button[@Name='Aceptar'][@AutomationId='OKButton']")).click();
    }

    public static void clickOnCancelar(){
        DriverManager.getDriverInstance().findElement(By.xpath("//Button[@Name='Cancelar'][@AutomationId='cancelButton']")).click();
    }

    public static void inputMotivo(String palabra){
        DriverManager.getDriverInstance().findElement(By.xpath("//Custom[@ClassName='MotivoBasicViewDropDownList']/ComboBox[@AutomationId='xamComboControl']")).click();
        if ("Error analitico".equals(palabra)) {
            ActionsManager.customSendKeys("ERROR ANA", true);
        }
    }

    public static void inputObservacion(String observacion){
        DriverManager.getDriverInstance().findElement(By.xpath("//Custom[@AutomationId='this']/Edit[@AutomationId='xamTextControl']")).click();
        ActionsManager.customSendKeys(observacion.toUpperCase(), false);
    }

    public static void selectComponeteAmodificar(){
        ActionManager.click(ModificarComponentesConstans.CLICK);
        List<WebElement> elements = DriverManager.getDriverInstance().findElements( By.xpath(  "//*[@ClassName='Cell'][contains(@Name, 'Chamico')]/../*[1]" ) );
        elements.forEach( WebElement::click );
    }

    public static void buttonAceptar(){
        WebElement element = DriverManager.getDriverInstance().findElement(By.xpath("//*[@Name='Aceptar Muestra / Porción'][@AutomationId='OKButton']"));
        element.click();
    }

    public static void inputPSZ_modificado(String psz){
        ActionManager.setInput(ModificarComponentesConstans.PSZ_MODOFICADO, psz, true);
    }

    public static void inputPBZ_modificado(String pbz){
        ActionManager.setInput(ModificarComponentesConstans.PBZ_MODIFICADO, pbz, true);
    }

}