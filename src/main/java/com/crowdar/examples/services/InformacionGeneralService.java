package com.crowdar.examples.services;

import com.crowdar.examples.utils.ActionsManager;

import org.testng.Assert;


public class InformacionGeneralService {
    public static void completeFieldsWith(String vendedor, String solicitante, String muestraDeclarada) {
        Assert.assertTrue(ActionsManager.waitForElement("//*[contains(@Name, 'Información General')]"));

        ActionsManager.customSendKeys(200, 7, solicitante, true);
        ActionsManager.customSendKeys(200, 4, vendedor, true);
        ActionsManager.customSendKeys(200, 15, muestraDeclarada, true);
    }
}
