package com.crowdar.examples.constants.ErrorDeTope;

public class ErrorDeTopeConstants {
    public static final String CONTAINER_LOC = "//*[@Name='Records']";
    public static final String CHECKBOX_SECTOR = "(//*[@Name='%s'][@ClassName='Cell']/../*)[1]";
    public static final String CHECK_BOX_TODOS = "//*[@ClassName='TextBlock'][@Name='Todos']";
    public static final String BUTON_FILTRAR = "//Button[@ClassName='Button'][@Name='Filtrar']";
    public static final String BUTON_FECHA = "//Text[@ClassName='TextBlock'][@Name='Fecha']";
    public static final String CHECKBOX_NRO_MUESTRA ="(//Custom[@ClassName='Cell'][contains(@Name,'%s')]/../*)[1]";
    public static final String PAGE_UP = "//*[@AutomationId='PageUp']";
    public static final String BUTON_ACCION = "//Button[@ClassName='Button'][@Name='%s']";
    public static final String MENU_TIPO_MOTIVO = "//Edit[@AutomationId='PART_EditableTextBox']";
    public static final String OBSERVACION = "//*[@AutomationId='observacionesText']/Edit[@AutomationId='xamTextControl']";
    public static final String BUTON_ACEPTAR_OPCION = "//*[@Name='Aceptar Opción'][@AutomationId='mainCommandButtonControl']";
    public static final String BUTON_CONFIRMACION = "//*[@Name='Si'][@AutomationId='OKButton']";

}
