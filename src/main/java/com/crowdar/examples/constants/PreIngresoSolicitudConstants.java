package com.crowdar.examples.constants;

public class PreIngresoSolicitudConstants {
    public static final String GENERAL_BUTTON = "XPATH://*[@Name='General']";
    public static final String FECHA_INGRESO_LABEL = "XPATH://*[contains(@Name, 'Fecha Ingreso')]";
    public static final String INICIAR_SOLICITUD_BUTTON = "XPATH://*[@Name='Iniciar Solicitud']";
    public static final String SOLICITANTE_PEDIDO_INPUT = "XPATH://*[@Name='Código Solicitante del Pedido: ']/*";
}
