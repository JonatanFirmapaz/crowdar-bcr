package com.crowdar.examples.constants;

public class CorteConstants {

    public static final String BOTON_AGREGAR = "XPATH://*[@Name='Agregar'][@AutomationId='btnAgregarMuestra']";
    public static final String INPUT_INGRESAR_MUESTRA = "XPATH://*[@Name='Ingreso de Muestras/Porciones Individualmente']/Edit[@ClassName='TextBox']";
    public static final String BOTON_ACEPTAR = "XPATH://*[@Name='Aceptar'][@AutomationId='mainCommandButtonControl']";
    public static final String INPUT_MUESTRA_CORTE = "XPATH://*[@ClassName='LeerMuestraPorcionParaCorteDDV']/Edit[@AutomationId='solicitudTextControl']";
    public static final String BOTON_CONFIRMAR_PORCIONES = "XPATH://*[@ClassName='Button'][@Name='Confirmar Porciones']/Text[@ClassName='TextBlock'][@Name='Confirmar Porciones']";
    public static final String BOTON_CANCELAR = "XPATH://*[@Name='Cancelar'][@AutomationId='cancelButton']";
    public static final String VERIFICO_POCION = "Cortelocators.cartelPorcion";
}
