package com.crowdar.examples.constants;

public class GenericConstants {
    public static final String BOTON_GUARDAR = "XPATH://*[@AutomationId='check'][last()]";
    public static final String HORIZONTAL_SCROLL_BAR = "XPATH://*[@AutomationId='HorizontalScrollBar']";
    public static final String BOTON_ACEPTAR = "XPATH://*[@Name='Aceptar']";
    public static final String BOTON_AGREGAR = "XPATH://*[@Name='Agregar']";
    public static final String BOTON__AGREGAR = "XPATH://*[@AutomationId='compositionGroupBoxControl']//*[@Name='_Agregar']";
    public static final String OPCION_PESO_HECTOLITRICO = "GenericLocators.OPCION_PESO_HECTOLITRICO";
    public static final String ALERT = "XPATH://*[contains(@Name, '%s')]";
}
