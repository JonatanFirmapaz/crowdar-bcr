package com.crowdar.examples.constants;

public class IngresoMuestraConstans {
    public static final String PALABRA_CLAVE_MUESTRA = "XPATH://*[@AutomationId='PART_EditableTextBox']";
    public static final String INPUT_FECHA = "XPATH://*[@AutomationId='fechaTextControl']/*[@AutomationId='xamDateTimeControl']";
    public static final String INPUT_NUMERO_MUESTRA = "XPATH://*[@AutomationId='codigoMuestraText']";
    public static final String INPUT_CODIGO_PUERTO = "XPATH://*[@ClassName='ClienteCodeSelectionControl']/*[@AutomationId='this']/*[@AutomationId='xamTextControl']/*[@AutomationId='PART_FocusSite']";
    public static final String CLICK_BOTON_REGISTRAR_SOLICITUD = "XPATH://*[@Name='Registrar Solicitud']";
    public static final String CLICK_BOTON_NUEVO_CONTENEDOR = "XPATH://*[@AutomationId=/nuevoRadioButtonControl/]/RadioButton[@Name=/Abrir nuevo contenedor/][@AutomationId=/radioButton/]/Text[@ClassName=/TextBlock/][@Name=/Abrir nuevo contenedor/]";
    public static final String CLICK_BOTON_ACEPTAR_CONTENEDOR = "XPATH://*[@AutomationId='OKButton']/Image[@AutomationId='check']";
    public static final String INPUT_CODIGO_CONTENEDOR = "XPATH://*[@AutomationId='codContenedorTextControl']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";
    public static final String INPUT_CODIGO_CONT_ZORRA = "XPATH://*[@ClassName='MuestraSolicitudLacradoDataView']/Edit[@AutomationId='codigoContenedorText']";
    public static final String CHECKBOX_CONTENEDOR = "XPATH://*[@AutomationId='imprimeCheckControl']/Button[@Name='Imprime datos de apertura'][@AutomationId='checkBoxToolControl']";
    public static final String CLICK_BOTON_ABRIR_CONTENEDOR = "XPATH://*[@Name='Abrir Contenedor'][@AutomationId='mainCommandButtonControl']/Text[@ClassName='TextBlock'][@Name='Abrir Contenedor']";
    public static final String MODAL_CONTENEDOR = "XPATH://*[@ClassName='Window'][@Name='Contenedores']";
    public static final String CLICK_BOTON_ACEPTAR_MODAL_INFO = "XPATH://*[@Name='Aceptar']";
    public static final String INPUT_CERRAR_CONTENEDOR = "XPATH://*[@AutomationId='codContenedorTextControl']";
    ///Group[@ClassName=\"GroupBox\"][@Name=\"Cerrar Contenedor\"]/Edit[@AutomationId=\"codContenedorTextControl\"]"
    public static final String PALABRA_CLAVE_CERRAR_CONTENEDOR = "XPATH://*[@Name='Cerrar Contenedor']/Text[@ClassName='TextBlock'][@Name='Cerrar Contenedor']";
    public static final String BOTON_CERRAR_CONTENEDOR = "XPATH://*[@AutomationId='mainCommandButtonControl']/Text[@ClassName='TextBlock'][@Name='Cerrar Contenedor']";
    public static final String MENSAJE_MODAL_CONTENEDOR_CERRADO = "XPATH://*[@ClassName='MessageView']/Text[@ClassName='TextBlock'][@Name='El Contenedor fue cerrado.']";

    public static final String BOTON_CERRAR_LABOATORIO = "XPATH://*[@Name=\"Cerrar\"][@AutomationId=\"Close\"]";
    public static final String BOTON_SI_CERRAR_LABORATORIO = "XPATH://*[@Name='Si'][@AutomationId='OKButton']";

    public static final String BOTON_NUEVO_CONTEDOR = "XPATH://*[@AutomationId='nuevoRadioButtonControl']/RadioButton[@Name='Abrir nuevo contenedor'][@AutomationId='radioButton']";
}
