package com.crowdar.examples.constants;

public class LabFisicoConstants {
    public static final String ASIGNAR_ANALISTA_INPUT = "XPATH://*[@ClassName='TextBlock'][@Name='Asignar Analista A Muestras Porciones Dentro De Un Contenedor']";
    public static final String CLICK_ANALISTA = "XPATH://*[@AutomationId='Analista']/Custom[@AutomationId='this']/Edit[@AutomationId='TextCodeControl_Analista']";
    public static final String ASIGNAR_ANALISTA = "XPATH://*[@AutomationId='Analista']/Custom[@AutomationId='this']/Edit[@AutomationId='TextCodeControl_Analista']/Edit[@AutomationId='PART_FocusSite']";
    public static final String CLICK_MESADA = "XPATH://*[@AutomationId='Mesada']/Custom[@AutomationId='this']/Edit[@AutomationId='TextCodeControl_Mesada']";
    public static final String ASIGNAR_MESADA = "XPATH://*[@AutomationId='Mesada']/Custom[@AutomationId='this']/Edit[@AutomationId='TextCodeControl_Mesada']/Edit[@AutomationId='PART_FocusSite']";
}
