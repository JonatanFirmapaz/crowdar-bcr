package com.crowdar.examples.constants;

public class ImportacionArchivoConstants {
    public static final String INPUT_SEARCH = "XPATH://*[@AutomationId='searchBox']";
    public static final String PALABRA_CLAVE = "XPATH://*[@AutomationId='PART_EditableTextBox']";
    public static final String OPCION_MENU_SOLICITUDES = "XPATH://*[@Name='Administrar Solicitudes']/Text[@ClassName='TextBlock'][@Name='Administrar Solicitudes']";
    public static final String BOTON_SUBIR_ARCHIVO = "XPATH://*[@AutomationId='solici01FileManagerControl']/Button[@Name='Subir'][@AutomationId='buttonControl']o";
    public static final String BOTON_PROCESAR_ARCHIVO = "XPATH://*[@AutomationId='procesarButtonControl']/Text[@ClassName='TextBlock'][@Name='Procesar']";
    public static final String INPUT_CODIGO_PUERTO = "XPATH://*[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";
    public static final String TABLA_DATOS_ARCHIVOS_SOLICI = "XPATH://*[@ClassName='ScrollViewer']/Custom[@ClassName='SolicitudAutomaticaDetailDataView']/Group[@ClassName='GroupBox'][@Name='Resultados']";
    public static final String CLOSE = "ImportacionArchivoLocators.close";
    public static final String BOTON_ABRIR_EXPLORADOR_WIN =  "XPATH://*[@Name='Abrir']/Button[@ClassName='Button'][@Name='Abrir']";
    public static final String BOTON_ABRIR_EXPLORADOR_WIN_INGLES =  "XPATH://*[@Name='Open']/Button[@ClassName='Button'][@Name='Open']";
    public static final String VALOR_MUESTRAS_PROCESADAS = "XPATH://*[@AutomationId='cantidadMuestrasProcesadasNumericControl']/Edit[@AutomationId='xamNumericControl']";
    public static final String VALOR_MUESTRAS_CORRECTAS = "XPATH://*[@AutomationId='cantidadMuestrasCorrectasNumericControl']/Edit[@AutomationId='xamNumericControl']/Text[@AutomationId='TextBlock']";

    public static final String LABORATORIO_LAUNCHER = "XPATH://*[@ClassName='Window'][@Name='BCR - Reingeniería']";

    public static final String PAGE_UP = "XPATH://*[@AutomationId='PageUp']";
    public static final String ADMINISTRAR_SOLICITUDES_SECTION = "XPATH://*[@Name='Administrar Solicitudes']";
}
