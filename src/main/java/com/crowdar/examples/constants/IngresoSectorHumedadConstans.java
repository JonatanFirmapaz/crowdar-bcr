package com.crowdar.examples.constants;


public class IngresoSectorHumedadConstans {
    public static final String PANTALLA_SEC_HUMEDAD = "XPATH://*[@ClassName='GroupBox']/Custom[@ClassName='AnalistaEtapaControl']/Group[@ClassName='GroupBox']";
    public static final String CODIGO_ANALISTA_INPUT = "XPATH://*[@ClassName='EmpleadoCodeSelectionControl']/Custom[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";
    public static final String BUTON_CARGA_ANALISTA = "XPATH://*[@ClassName='Button'][@Name='Pre-cargar Analista']/Text[@ClassName='TextBlock'][@Name='Pre-cargar Analista']";
    public static final String FECHA_INPUT = "XPATH://*[@AutomationId='fechaAReplicarDateTimeControl']/Edit[@AutomationId='xamDateTimeControl']";
    public static final String BUTON_CARGA_FECHA = "XPATH://*[@ClassName='IngresarComponenteDetailDataView']/Button[@ClassName='Button'][@Name='Pre-cargar Fecha']/Text[@ClassName='TextBlock'][@Name='Pre-cargar Fecha']";
    public static final String INPUT_TC1 = "XPATH://*[@AutomationId='controlComponente']/Edit[@AutomationId='numericControl_TaraCpsulaTC']";
    public static final String INPUT_PCMS1 = "XPATH://*[@AutomationId='controlComponente']/Edit[@AutomationId='numericControl_PesodeCpsulamsMuestraSecaPCMS']";
}
