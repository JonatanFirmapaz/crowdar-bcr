package com.crowdar.examples.constants;

public class IngresarMuestraSoliLacradoConstants {
    public static final String VERIFY_SISTEM_MESSAGE = "XPATH://*[@ClassName='MuestraSolicitudLacradoDataView']/Custom[@ClassName='MuestraSolicitudLacradoDataView']/Edit[@AutomationId='xamTextControl']/Text[@AutomationId='TextBlock']";
    public static final String INGRESAR_COD_FIRMANTE = "XPATH://*[@ClassName='EmpleadoFirmanteLacradoCodeSelectionControl']/Custom[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";
    public static final String BUTTON_GUARDAR = "XPATH://Button[@Name='Guardar']";
    public static final String BUTTON_POPUP_INFO = "XPATH://Button[@Name='Aceptar'][@AutomationId='OKButton']/Text[@ClassName='TextBlock'][@Name='Acep_tar']";
                                                          //Button[@Name='Aceptar'][@AutomationId='OKButton']/Text[@ClassName='TextBlock'][@Name='Acep_tar']"
    public static final String INPUT_CUARTEAR_MUESTRA ="XPATH: //Custom[@ClassName='CuartearMuestraLacradoDataView']/Edit[@AutomationId='codigoMuestraText']";
}
