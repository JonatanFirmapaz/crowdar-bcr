package com.crowdar.examples.constants;

public class IngresoResultadosEmaConstans {
    public static final String RESLTADOS_EMA_INPUT = "XPATH://*[@Name='Ingresar Resultados EMA']/Text[@ClassName='TextBlock'][@Name='Ingresar Resultados EMA']";
    public static final String INGRESAR_MUESTRA_INPUT = "XPATH://*[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";

    public static final String EMA_PANTALLA = "XPATH://*[@ClassName='ScrollViewer']/Custom[@ClassName='PantallaParticular1Fisico']";
    public static final String CODIGO_ANALISTA_INPUT = "XPATH://*[@ClassName='AnalistaEtapaControl']/Group[@ClassName='GroupBox']/Custom[@AutomationId='analistaEmpleadoCodeSelectionControl']/Custom[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";
    public static final String PH_INPUT = "XPATH://*[@ClassName='ComponenteNumericoControl']/Custom[@AutomationId='controlComponente']/Edit[starts-with(@AutomationId,'numericControl_LFCValorPesoHectolitricoVPH')]";
    public static final String HEA_INPUT = "XPATH://*[@ClassName='ComponenteNumericoControl']/Custom[@AutomationId='controlComponente']/Edit[starts-with(@AutomationId,'numericControl_LFCHumedadEquiposAutomticosHEA')]";
    public static final String CODIGO_EQUIPO = "XPATH://*[@ClassName='GroupBox']/Custom[@AutomationId='this']/Custom[@AutomationId='controlComponente']/Custom[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";
    public static final String CP_INPUT = "XPATH://*[@ClassName='ComponenteNumericoControl']/Custom[@AutomationId='controlComponente']/Edit[starts-with(@AutomationId,'numericControl_LFCValorContenidoProteicoVCP')]";

    public static final String BOTON_ACEPTAR = "XPATH://*[@AutomationId='mainCommandButtonControl']/Text[@ClassName='TextBlock'][@Name='Aceptar Componente']";
    public static final String BOTON_NO_ABRIR_CONTENEDOR = "XPATH://*[@AutomationId='noRealizarRadioButtonControl2']/*[@Name='No abrir contenedor']";

    public static final String VALIDAR_PH = "XPATH://*[@ClassName='TextBlock'][@Name='Los resultados se encuentran correctos.']";
    public static final String VALIDAR_CP = "XPATH://*[@ClassName='TextBlock'][@Name='Los resultados se encuentran correctos.']";

    public static final String VALIDAR_PH_FLUJO2 = "IngresoResultadosEmaLocators.validarResultadosPHFlujo2";

    public static final String BOTON_CANCELAR = "XPATH://*[@AutomationId='cancelButton']";
}