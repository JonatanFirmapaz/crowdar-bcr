package com.crowdar.examples.constants;

public class VaciarContenedoresConstants {
    public static final String INPUT_VACIAR_CONTENEDORES = "XPATH://*[@ClassName='ListBoxItem'][@Name='Vaciar Contenedor']/Text[@ClassName='TextBlock'][@Name='Vaciar Contenedor']";
    public static final String INPUT_CODIGO_CONTENEDOR = "XPATH://*[@ClassName='VaciarContenedorView']/Group[@ClassName='GroupBox'][@Name='Vaciar Contenedor']/Edit[@AutomationId='codContenedorTextControl']";
    public static final String BOTON_ACEPTAR = "XPATH://*[@Name='Aceptar'][@AutomationId='OKButton']";
}