package com.crowdar.examples.constants.AdministrarSolicitudes;

public class AdministrarSolicitudesConstants {
    public static final String NUMERO_SOLICITUD_INPUT = "XPATH://*[@AutomationId='numeroSolicitudMaskControl']/Edit[@AutomationId='xamMaskedControl']";
    public static final String PAGE_UP = "XPATH://*[@AutomationId='PageUp']";
    public static final String BOTON_PRE_SOLICITUD = "XPATH://*[@Name='Pre - Ingreso Solicitud']";
    public static final String BOTON_SOLICITUD_AUTOMATICA = "XPATH://*[@Name='Ingresar Solicitud Automática']/Text[@ClassName='TextBlock'][@Name='Ingresar Solicitud Automática']";
    public static final String BOTON_BUSQUEDA_AVANZADA = " XPATH://*[@Name='Búsqueda Avanzada']";
    public static final String BOTON_INGRESAR_SOLICITUD = "XPATH://*[@Name='Ingreso Solicitud']";
    public static final String BOTON_VERIFICAR = "XPATH://*[@Name='Verificar']";

    public static final String INPUT_NUMERO_SOLICITUD = "XPATH://*[@ClassName='ResultadoEnsayoSolicitudDDV']/Edit[@AutomationId='numeroSolicitudText']";

    public static final String VERIFICACION_PAGE = "XPATH://*[@ClassName='Label'][@Name='Técnica']/Text[@ClassName='TextBlock'][@Name='Técnica']";

    public static final String BUTTON_ACEPTAR = "XPATH://*[@ClassName='Window']/Button[@Name='Aceptar'][@AutomationId='OKButton']/Text[@ClassName='TextBlock'][@Name='Acep_tar']";
}
