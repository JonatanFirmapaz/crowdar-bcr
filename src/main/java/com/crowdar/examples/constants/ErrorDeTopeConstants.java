package com.crowdar.examples.constants;

public class ErrorDeTopeConstants {
    public static final String CHECKLIST_SECTOR = "//*[@AutomationId='chkListSector']";
    public static final String CONTAINER_LOC = "//*[@Name='Records']";
    public static final String CHECKBOX_SECTOR = "(//*[@Name='%s'][@ClassName='Cell']/../*)[1]";
    public static final String ADMINISTRACION = "//*[@Name='Administración'][@ClassName='Cell']";
    public static final String LAB_FISICO_EMA= "//*[@Name='Lab. físico - EMA'][@ClassName='Cell']";
    public static final String CHECK_BOX_LAB_FISICO_EMA = "(//*[@Name='Lab. físico - EMA'][@ClassName='Cell']/../*)[1]";

    public static final String CHECK_BOX_TODOS = "//*[@ClassName='TextBlock'][@Name='Todos']";
    //public static final String CHECK_BOX_TODOS = "//Custom[@AutomationId=chkTodosEnsayos]";
    //Custom[@AutomationId=\"chkTodosEnsayos\"]/Button[@Name=\"Todos\"][@AutomationId=\"checkBoxToolControl\"]"
    public static final String BUTON_FILTRAR = "//Button[@ClassName='Button'][@Name='Filtrar']";
    public static final String BUTON_FECHA = "//Text[@ClassName='TextBlock'][@Name='Fecha']";
    public static final String CONTAINER_LOC_NO_MSTRA = "//*[@ClassName='Record'][@Name='BCR.Lab.Business.Entities.MuestrasPorciones']/";
    public static final String NRO_MUESTRA = "//Custom[@ClassName='Cell'][@Name='%s']";
//    public static final String NRO_MUESTRA ="(//Text[@AutomationId='TextBlock'])[1]";
    public static final String CHECKBOX_NRO_MUESTRA ="(//Custom[@ClassName='Cell'][contains(@Name,'%s')]/../*)[1]";
    public static final String CHECKBOX_ENSAYO ="(//Custom[@ClassName='Cell'][contains(@Name,'%s')]/../*)[1]";
//    public static final String CHECKBOX_NRO_MUESTRA ="(//HeaderItem[@ClassName='RecordSelector']/CheckBox[@ClassName='CheckBox'])[1]";

    public static final String PAGE_UP = "//*[@AutomationId='PageUp']";
    public static final String CONTAINER_LOC_RECORDS = "//*[@ClassName='MuestraPorcionErrorTopeDDV']";
    public static final String CONTAINER_LOC_PAGEUP = "//*[@ClassName='MuestraPorcionErrorTopeDDV']";
    public static final String BUTON_ACCION = "//Button[@ClassName='Button'][@Name='%s']";
    public static final String BUTON_REANALIZAR_MISMO_METODO = "//*[@Name='Re-Analizar por Mismo Método']";
    public static final String BUTON_VER_DETALLE = "//Button[@ClassName='Button'][@Name='Ver Detalle Problema']";
    public static final String BUTON_CONSULTAR_COMPONENTES = "//Button[@ClassName='Button'][@Name='Consultar Componentes']";
    public static final String BUTON_IMPRIMIR = "//Button[@ClassName='Button'][@Name='Imprimir']";
    public static final String BUTON_ACEPTAR_COMPONENTES = "//Button[@ClassName='Button'][@Name='Aceptar Componentes']";
    public static final String BUTON_REANALIZAR_METODO_PATRON = "//Button[@ClassName='Button'][@Name='Re-Analizar por Método Patrón']";

    public static final String MENU_TIPO_MOTIVO = "//Edit[@AutomationId='PART_EditableTextBox']";

    public static final String OBSERVACION = "//*[@AutomationId='observacionesText']/Edit[@AutomationId='xamTextControl']";

    public static final String BUTON_ACEPTAR_OPCION = "//*[@Name='Aceptar Opción'][@AutomationId='mainCommandButtonControl']";
    public static final String BUTON_CONFIRMACION = "//*[@Name='Si'][@AutomationId='OKButton']";
}
