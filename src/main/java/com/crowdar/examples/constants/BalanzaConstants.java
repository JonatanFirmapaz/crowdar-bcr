package com.crowdar.examples.constants;

public class BalanzaConstants {

    public static final String INPUT_PORCION = "XPATH://*[@ClassName='TextBlock'][@Name='Ingresar Componentes']";
    public static final String BALANZA_VERIFICACION = "XPATH://*[@ClassName='IngresarComponentesEnsayosSectorBalanzaDetailDataView']";
    public static final String INPUT_ANALISTA = "XPATH://*[@ClassName='AnalistaBalanzaEmpleadoCodeSelectionControl']/Custom[@AutomationId='this']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";
    public static final String INPUT_ARDIDOS = "XPATH://*[@ClassName='ComponenteNumericoControl']/Custom[@AutomationId='controlComponente']/Edit[@AutomationId='numericControl_PesodelEnsayoenSectorBalanza']";
    public static final String BOTON_ACEPTAR = "XPATH://*[@Name='Aceptar Componente'][@AutomationId='mainCommandButtonControl']/Text[@ClassName='TextBlock'][@Name='Aceptar Componente']";
    public static final String BOTON_CANCELAR = "XPATH://*[@Name='Cancelar'][@AutomationId='cancelButton']";

    public static final String INPUT_PESO_ENSAYO = "XPATH://*[@AutomationId='controlComponente']/Edit[@AutomationId='numericControl_PesodelEnsayoenSectorBalanza']";

}
