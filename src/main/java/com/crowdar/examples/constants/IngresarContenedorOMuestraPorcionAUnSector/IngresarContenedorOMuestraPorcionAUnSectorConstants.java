package com.crowdar.examples.constants.IngresarContenedorOMuestraPorcionAUnSector;

public class IngresarContenedorOMuestraPorcionAUnSectorConstants {
    public static final String BOTON_AGREGAR = "XPATH://*[@Name='Agregar']";
    public static final String INGRESAR_MUESTRA = "XPATH://*[@Name='Ingreso de Muestras/Porciones Individualmente']/Edit[@ClassName='TextBox']";
}
