package com.crowdar.examples.constants;

public class BusquedaAvanzadaConstants {
    public static final String BOTON_FILTRAR = "XPATH://*[@Name='Filtrar']";
    public static final String BOTON_INGRESAR_SOLICITUD = "XPATH://*[@Name='Ingreso Solicitud']";
}
