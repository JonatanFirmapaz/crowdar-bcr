package com.crowdar.examples.constants;

public class ModificarComponentesConstans {

    public static final String PSZ_MODOFICADO = "XPATH://*[@AutomationId='numericControl_PesoSobreZarandaPSZ']";
    public static final String PBZ_MODIFICADO = "XPATH://*[@AutomationId='numericControl_PesoBajoZarandaPBZ']";
    public static final String CLICK = "XPATH://*[@AutomationId='ExpanderBarButton']";
}
