package com.crowdar.examples.constants;

public class LoginConstants {
    public static final String USERNAME_INPUT = "XPATH://*[@AutomationId='usernametexbox']";
    public static final String PASSWORD_INPUT = "XPATH://*[@AutomationId='passwordControl']";
    public static final String LOGIN_BUTTON = "XPATH://*[@AutomationId='LoginButton']";
    public static final String LABORATORIO_BUTTON = "XPATH://*[@AutomationId='Imageicon']";

    public static final String USUARIO_INCORRECTO = "XPATH://*[@Name='El usuario o la clave ingresada es incorrecta.']/Text[@ClassName='TextBlock'][@Name='El usuario o la clave ingresada es incorrecta.']";
    public static final String USUARIO_OBLIGATORIO = "XPATH://*[@Name='El campo Usuario es obligatorio.']/Text[@ClassName='TextBlock'][@Name='El campo Usuario es obligatorio.']";
    public static final String PASS_OBLIGATORIA = "XPATH://*[@Name='El campo Password es obligatorio.']/Text[@ClassName='TextBlock'][@Name='El campo Password es obligatorio.']";
}
