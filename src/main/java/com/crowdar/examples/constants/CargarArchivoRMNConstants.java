package com.crowdar.examples.constants;

public class CargarArchivoRMNConstants {
    public static final String BUTTON_PROCESAR_ARCHIVO = "XPATH://*[@AutomationId='this']/Pane[@ClassName='ScrollViewer']/Button[@Name='Procesar Archivo'][@AutomationId='addButton']";
    public static final String PAGE_VERIFICATION = "XPATH://*[@AutomationId='this']/Pane[@ClassName='ScrollViewer']/Custom[@ClassName='ProcesoArchivoRMNFilterDataView']/Custom[@AutomationId='numeroProcesoRadioButtom']";
    public static final String BUTON_SUBIR_ARCHIVO = "XPATH://*[@AutomationId='fileManager']/Button[@Name='Subir'][@AutomationId='buttonControl']";
    public static final String BUTON_PROCESAR_ARCHIVO = "XPATH://*[@AutomationId='this']/Button[@Name='Procesar Archivo'][@AutomationId='saveCopyButton']/Text[@ClassName='TextBlock'][@Name='Procesar Archivo']";
    public static final String BUTON_ACEPTAR = "XPATH://*[@Name='Aceptar'][@AutomationId='OKButton']/Text[@ClassName='TextBlock'][@Name='Acep_tar']";
}
