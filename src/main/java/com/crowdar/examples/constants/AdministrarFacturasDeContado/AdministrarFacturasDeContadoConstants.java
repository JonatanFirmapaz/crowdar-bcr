package com.crowdar.examples.constants.AdministrarFacturasDeContado;

public class AdministrarFacturasDeContadoConstants {

    public static final String FORMA_PAGO_INPUT = "XPATH://Custom[@AutomationId=\"FormaPagoDDL\"]/ComboBox[@AutomationId=\"xamComboControl\"]";
    public static final String CONCEPTO_INPUT = "XPATH://Custom[@AutomationId=\"detalles\"]/ComboBox[@AutomationId=\"xamComboControl\"]/ComboBox[@AutomationId=\"PART_FocusSite\"]/Edit[@AutomationId=\"PART_EditableTextBox\"]";
    public static final String CHECKBOX_GENERAL = "XPATH:(//*[@Name='%s']/..//*)[1]";

}