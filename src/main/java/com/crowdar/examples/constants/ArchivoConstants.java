package com.crowdar.examples.constants;

public class ArchivoConstants {

    public static final String INPUT_MENU_ARCHIVO = "XPATH://*[@ClassName='TextBlock'][@Name='Ingresar Muestra o Contenedor a Archivo']";
    public static final String BOTON_AGREGAR = "XPATH://*[@Name='Agregar'][@AutomationId='btnAgregarMuestra']";
    public static final String INPUT_INGRESAR_MUESTRA = "XPATH://*[@Name='Ingreso de Muestras/Porciones Individualmente']/Edit[@ClassName='TextBox']";
    public static final String VALIDAR_ESTADO = "XPATH://*[@ClassName='Cell'][@Name='Correcta.']/Edit[@ClassName='XamTextEditor']/Text[@AutomationId='TextBlock']";
    public static final String INPUT_ADMINISTRAR_ARCHIVO = "XPATH://*[@ClassName='TextBlock'][@Name='Administrar Archivo']";
    public static final String BOTON_ACEPTAR = "XPATH://*[@Name='Aceptar'][@AutomationId='OKButton']";
    public static final String BOTON_INGRESAR = "XPATH://*[@ClassName='Button'][@Name='Ingresar']";
    public static final String BOTON_AGREGAR_MUESTRA = "XPATH://*[@Name='Agregar'][@AutomationId='btnAgregarMuestra']";
    public static final String INPUT_MUESTRA = "XPATH://*[@ClassName='GroupBox'][@Name='Muestras']/Edit[@ClassName='TextBox']";
    public static final String CONTENEDOR_DESTINO = "XPATH://*[@AutomationId='codContenedorDestinoTextControl']/Edit[@AutomationId='xamTextControl']";
    public static final String INPUT_CONTENEDOR_DESTINO = "XPATH://*[@AutomationId='codContenedorDestinoTextControl']/Edit[@AutomationId='xamTextControl']/Edit[@AutomationId='PART_FocusSite']";
    public static final String ACEPTAR_VALIDATION = "XPATH://*[@Name='Aceptar'][@AutomationId='OKButton']";
    public static final String BOTON_ACEPTAR_MUESTRA = "XPATH://*[@Name='Aceptar'][@AutomationId='mainCommandButtonControl']";
}
