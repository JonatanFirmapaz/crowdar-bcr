package com.crowdar.examples.drivers;

import java.util.HashMap;
import java.util.Map;

import com.crowdar.core.PropertyManager;
import com.crowdar.driver.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class LauncherDriver extends GenericDriver{
	
	public static void open() {
		try {
			Map <String, String> capabilities = new HashMap <>();
			capabilities.put( "app", System.getProperty( "user.dir" ).concat( "\\app\\BCR.UI.Launcher.application" ) );
			DriverManager.initialize( capabilities );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

}

