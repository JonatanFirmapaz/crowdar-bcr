package com.crowdar.examples.drivers;

import com.crowdar.core.Injector;
import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;

import java.io.IOException;

public class GenericDriver {

	public static void close() {
		DriverManager.dismissCurrentDriver();
		Injector.cleanThreadCache();
		ActionManager.clean();
	}
}

