package com.crowdar.examples.drivers;

import com.crowdar.driver.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DesktopDriver extends GenericDriver{
	private static WebDriver driver;
	private static DesiredCapabilities cap;

	public static void open() {
		try {
			Map<String, String> capabilitiesDes = new HashMap<String, String>();
			capabilitiesDes.put("app", "Root");
			DriverManager.initialize(capabilitiesDes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getLaboratorioWindow() {
		WebElement MAWebElement = DriverManager.getDriverInstance().findElement(By.name("BCR - Reingeniería"));
		String MAWinHandleStr = MAWebElement.getAttribute("NativeWindowHandle");
		int MAWinHandleInt = Integer.parseInt(MAWinHandleStr);
		String MAWinHandleHex = Integer.toHexString(MAWinHandleInt);
		return MAWinHandleHex;
	}
}

