package com.crowdar.examples.drivers;

import com.crowdar.driver.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.Map;

public class LaboratorioDriver extends GenericDriver{

	public static void open(String appTopLevelWindowHandle) {
		try {
			Map<String, String> capabilities2 = new HashMap<String, String>();
			capabilities2.put("deviceName", "WindowsPC");
			capabilities2.put("platformName", "Windows");
			capabilities2.put("appTopLevelWindow", appTopLevelWindowHandle);
			DriverManager.initialize(capabilities2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

